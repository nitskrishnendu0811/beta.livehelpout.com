angular
.module("authFront", ["ngCookies"])
.factory("myAuth", function ($http, $cookies, $cookieStore) {
  var factobj = {};
  factobj.alerts = [
    { type: 'danger', msg: 'Invalid Email/Password!', altclass: 'text-center alert alert-danger' },
    { type: 'success', msg: 'You have successfully logged in.', altclass: 'text-center alert alert-success' },
    { type: 'warning', msg: 'You have successfully logged in.', altclass: 'text-center alert alert-warning' }
  ];

  factobj.addAlert = function(type,message) {
   var msg={};
    angular.forEach(factobj.alerts,function(alert,key){
       if(alert.type==type)
       {
         alert.msg=message;
         msg=alert;
       }
    });
    return msg;
  };
  
    //factobj.baseurl = "http://localhost/livehelpout/";
    //factobj.baseurl = "http://45.79.160.248/";
     factobj.baseurl = "http://livehelpout.com/";
    
    var years = [];
    for (var i = 2014; i <= 2050; i++) {
        years.push(i);
    }
    factobj.years = years;
    
    var days = [];
    for (var i = 1; i <= 31; i++) {
        days.push(i);
    }
    factobj.days = days;
    
    /*********************************Admin Authorisation ***********************************/
    factobj.adminuserinfo = { loginstatus: false, id: "",username:"" };
    factobj.updateAdminUserinfo = function (obj) {
    if(obj)
	{
           factobj.adminuserinfo = { loginstatus: obj.isloggedin, id: obj.id,username:obj.username };
           return true;
	}
    };
    factobj.resetAdminUserinfo = function () {
        factobj.adminuserinfo = { loginstatus: false, id: "", username:""};
    };
    
    factobj.getAdminAuthorisation = function () {      
       var obj=$cookieStore.get('admins');
       if(obj)
       return obj;
       else
       return null;
    };
    
    factobj.getAdminNavlinks = function () {
        var adminlogin = factobj.adminuserinfo.loginstatus,
            adminemail = (typeof factobj.adminuserinfo.id == "undefined" || factobj.adminuserinfo.id == "") ? "Unknown" : factobj.adminuserinfo.id;
        if (!adminlogin) {
           return false;
        } else {
	    return factobj.adminuserinfo;
        }
    };
    
    factobj.isAdminLoggedIn = function () {
        var adminlogin = factobj.adminuserinfo.loginstatus;
        if (!adminlogin) {
            return false;
        } else {
	    return true;
        }
    };
    
    /*********************************Admin Authorisation End***********************************/
    
    /*********************************User Authorisation ***********************************/
    factobj.userinfo = { loginstatus: false, id: "", email: "",name:"",image:"",username:"",role:"",current_status:"",price:"",fbid:"",lnid:"",gid:"",imgtype:"",credit_tokens:0 };
    factobj.updateUserinfo = function (obj) {
    if(obj)
	{
           if(obj.imgtype==1){
           	if(obj.image=='')
           	{
           		obj.image = factobj.baseurl+'assets/upload/user_images/noimage.png';
           	}else{
           		obj.image = factobj.baseurl+'assets/upload/user_images'+obj.image;
           	}
           }
           factobj.userinfo = { loginstatus: obj.isloggedin, id: obj.id, email: obj.email,name:obj.name,username:obj.username,image:obj.image,role:obj.role,current_status:obj.login_status,price:obj.price,fbid:obj.fbid,lnid:obj.lnid,gid:obj.gid,imgtype:obj.imgtype,credit_tokens:obj.credit_tokens};
           return true;
	}
    };
    factobj.resetUserinfo = function () {
        factobj.userinfo = { loginstatus: false, id: "", email: "",name:"",image:"",username:"",role:"",current_status:"",price:"",fbid:"",lnid:"",gid:"",imgtype:"",credit_tokens:0 };
    };
    
    factobj.getUserAuthorisation = function () {      
       var obj=$cookieStore.get('users');
       if(obj)
       return obj;
       else
       return null;
    };
    
    factobj.getUserNavlinks = function () {
        var userlogin = factobj.userinfo.loginstatus,
            useremail = (typeof factobj.userinfo.id == "undefined" || factobj.userinfo.id == "") ? "Unknown" : factobj.userinfo.id;
        if (!userlogin) {
           return false;
        } else {
	    return factobj.userinfo;
        }
    };
    
    factobj.isUserLoggedIn = function () {
        var userlogin = factobj.userinfo.loginstatus;
        if (!userlogin) {
            return false;
        } else {
	    return true;
        }
    };
    
    /*********************************User Authorisation End***********************************/
    
    return factobj;
	
})
