'use strict';

/**
 * Config constant
 */
app.constant('APP_MEDIAQUERY', {
    'desktopXL': 1200,
    'desktop': 992,
    'tablet': 768,
    'mobile': 480
});
app.constant('JS_REQUIRES', {
    //*** Scripts
    scripts: {
        //****** admin css*****//
        'admin':['assets/css/styles.css','assets/css/plugins.css','assets/css/components-md.css'],
        
        //******frontend style******//
        'frontend':['assets/frontend/css/style.css','assets/frontend/css/animate.css','assets/frontend/js/wow.min.js','assets/frontend/js/move-top.js','assets/frontend/js/easing.js','assets/frontend/js/bootstrap.touchspin.js','assets/frontend/js/jquery-ui.min.js'],
        
        //*** Javascript Plugins
        'modernizr': ['vendor/modernizr/modernizr.js'],
        'moment': ['vendor/moment/moment.min.js'],
        'spin': 'vendor/ladda/spin.min.js',
        'jstz': 'assets/frontend/js/jstz-1.0.4.min.js',

        //*** jQuery Plugins
        'perfect-scrollbar-plugin': ['vendor/perfect-scrollbar/perfect-scrollbar.min.js', 'vendor/perfect-scrollbar/perfect-scrollbar.min.css'],
        'ladda': ['vendor/ladda/spin.min.js', 'vendor/ladda/ladda.min.js', 'vendor/ladda/ladda-themeless.min.css'],
        'sweet-alert': ['vendor/sweet-alert/sweet-alert.min.js', 'vendor/sweet-alert/sweet-alert.css'],
        'chartjs': 'vendor/chartjs/Chart.min.js',
        'jquery-sparkline': 'vendor/sparkline/jquery.sparkline.min.js',
        'ckeditor-plugin': 'vendor/ckeditor/ckeditor.js',
        'jquery-nestable-plugin': ['vendor/ng-nestable/jquery.nestable.js', 'vendor/ng-nestable/jquery.nestable.css'],
        'touchspin-plugin': 'vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js',
        'ngTagsInput': ['assets/frontend/js/ng-tags-input.min.js',  'assets/frontend/css/ng-tags-input.min.css'],
        //'stripejs' : 'https://js.stripe.com/v2/',
        //'ngCalendar': ['assets/js/bower_components/angular-ui-calendar/src/calendar.js',  'assets/js/bower_components/fullcalendar/dist/fullcalendar.min.js', 'assets/js/bower_components/fullcalendar/dist/fullcalendar.css'],
	
        //*** Controllers
        'dashboardCtrl': 'assets/js/controllers/dashboardCtrl.js',
        'categoryCtrl': 'assets/js/controllers/categoryCtrl.js',
	'subcategoryCtrl': 'assets/js/controllers/subcategoryCtrl.js',
	'editsubcategoryCtrl': 'assets/js/controllers/editsubcategoryCtrl.js',
	'addsubcategoryCtrl': 'assets/js/controllers/addsubcategoryCtrl.js',
	'addcategoryCtrl': 'assets/js/controllers/addcategoryCtrl.js',
	'editcategoryCtrl': 'assets/js/controllers/editcategoryCtrl.js',
        'helperCtrl': 'assets/js/controllers/helperCtrl.js',
        'helpeeCtrl': 'assets/js/controllers/helpeeCtrl.js',
        'iconsCtrl': 'assets/js/controllers/iconsCtrl.js',
        'vAccordionCtrl': 'assets/js/controllers/vAccordionCtrl.js',
        'ckeditorCtrl': 'assets/js/controllers/ckeditorCtrl.js',
        'laddaCtrl': 'assets/js/controllers/laddaCtrl.js',
        'ngTableCtrl': 'assets/js/controllers/ngTableCtrl.js',
        'cropCtrl': 'assets/js/controllers/cropCtrl.js',
        'asideCtrl': 'assets/js/controllers/asideCtrl.js',
        'toasterCtrl': 'assets/js/controllers/toasterCtrl.js',
        'sweetAlertCtrl': 'assets/js/controllers/sweetAlertCtrl.js',
        'mapsCtrl': 'assets/js/controllers/mapsCtrl.js',
        'chartsCtrl': 'assets/js/controllers/chartsCtrl.js',
        'calendarCtrl': 'assets/js/controllers/calendarCtrl.js',
        'nestableCtrl': 'assets/js/controllers/nestableCtrl.js',
        'validationCtrl': ['assets/js/controllers/validationCtrl.js'],
        'userCtrl': ['assets/js/controllers/userCtrl.js'],
        'selectCtrl': 'assets/js/controllers/selectCtrl.js',
        'wizardCtrl': 'assets/js/controllers/wizardCtrl.js',
        'uploadCtrl': 'assets/js/controllers/uploadCtrl.js',
        'treeCtrl': 'assets/js/controllers/treeCtrl.js',
        'inboxCtrl': 'assets/js/controllers/inboxCtrl.js',
        'xeditableCtrl': 'assets/js/controllers/xeditableCtrl.js',
        'chatCtrl': 'assets/js/controllers/chatCtrl.js',
       	'adminloginCtrl':'assets/js/controllers/adminloginCtrl.js',
	'adminheaderCtrl':'assets/js/controllers/adminheaderCtrl.js',
        
         //******** Filters
         'htmlToPlaintext': 'assets/js/filters/htmlToPlaintext.js',
       
        //*********front end controllers
        'availabilityCtrl': 'assets/js/controllers/frontend/availabilityCtrl.js',
        'usersettingCtrl': 'assets/js/controllers/frontend/usersettingCtrl.js',
        'mentorprofileCtrl': 'assets/js/controllers/frontend/mentorprofileCtrl.js',
        'taxinfoCtrl': 'assets/js/controllers/frontend/taxinfoCtrl.js',
        'expertCtrl': 'assets/js/controllers/frontend/expertCtrl.js',
        'accountsettingCtrl': 'assets/js/controllers/frontend/accountsettingCtrl.js',
        'indexCtrl': 'assets/js/controllers/frontend/indexCtrl.js',
        'headerCtrl': 'assets/js/controllers/frontend/headerCtrl.js',
        'userdashboardCtrl': 'assets/js/controllers/frontend/userdashboardCtrl.js',
        'useractivationCtrl': 'assets/js/controllers/frontend/useractivationCtrl.js',
        'resetpasswordCtrl': 'assets/js/controllers/frontend/resetpasswordCtrl.js',
        'mentorsignupCtrl': 'assets/js/controllers/frontend/mentorsignupCtrl.js',
        'mentordashboardCtrl': 'assets/js/controllers/frontend/mentordashboardCtrl.js',
        'mentorsignupstep1Ctrl': 'assets/js/controllers/frontend/mentorsignupstep1Ctrl.js',
        'openrequestCtrl': 'assets/js/controllers/frontend/openrequestCtrl.js',
        'mentorleftpanelCtrl': 'assets/js/controllers/frontend/mentorleftpanelCtrl.js',
        'settingleftpanelCtrl': 'assets/js/controllers/frontend/settingleftpanelCtrl.js',
        'userprofileCtrl': 'assets/js/controllers/frontend/userprofileCtrl.js',
        'livefeedCtrl': 'assets/js/controllers/frontend/livefeedCtrl.js',
        'searchResultCtrl': 'assets/js/controllers/frontend/searchResultCtrl.js',
        'buycreditCtrl' : 'assets/js/controllers/frontend/buycreditCtrl.js',        
        'paymentreturnCtrl' : 'assets/js/controllers/frontend/paymentreturnCtrl.js',
        'paymentsuccessCtrl' : 'assets/js/controllers/frontend/paymentsuccessCtrl.js',
        'paymentcancelCtrl' : 'assets/js/controllers/frontend/paymentcancelCtrl.js'
    },

    //*** angularJS Modules
    modules: [{
        name: 'angularMoment',
        files: ['vendor/moment/angular-moment.min.js']
    },{
        name: 'perfect_scrollbar',
        files: ['vendor/perfect-scrollbar/angular-perfect-scrollbar.js']
    }, {
        name: 'toaster',
        files: ['vendor/toaster/toaster.js', 'vendor/toaster/toaster.css']
    }, {
        name: 'angularBootstrapNavTree',
        files: ['vendor/angular-bootstrap-nav-tree/abn_tree_directive.js', 'vendor/angular-bootstrap-nav-tree/abn_tree.css']
    }, {
        name: 'angular-ladda',
        files: ['vendor/ladda/angular-ladda.min.js']
    }, {
        name: 'ngTable',
        files: ['vendor/ng-table/ng-table.min.js', 'vendor/ng-table/ng-table.min.css']
    }, {
        name: 'ui.select',
        files: ['vendor/ui-select/select.min.js', 'vendor/ui-select/select.min.css', 'vendor/ui-select/select2.css', 'vendor/ui-select/select2-bootstrap.css', 'vendor/ui-select/selectize.bootstrap3.css']
    }, {
        name: 'ui.mask',
        files: ['vendor/ui-utils/mask/mask.js']
    }, {
        name: 'angular-bootstrap-touchspin',
        files: ['vendor/bootstrap-touchspin/angular.bootstrap-touchspin.js', 'vendor/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css']
    }, {
        name: 'ngImgCrop',
        files: ['vendor/ngImgCrop/ng-img-crop.js', 'vendor/ngImgCrop/ng-img-crop.css']
    }, {
        name: 'angularFileUpload',
        files: ['vendor/angular-file-upload/angular-file-upload.min.js', 'vendor/angular-file-upload/directives.js']
    }, {
        name: 'ngAside',
        files: ['vendor/angular-aside/angular-aside.min.js', 'vendor/angular-aside/angular-aside.min.css']
    }, {
        name: 'truncate',
        files: ['vendor/angular-truncate/truncate.js']
    }, {
        name: 'oitozero.ngSweetAlert',
        files: ['vendor/sweet-alert/ngSweetAlert.min.js']
    }, {
        name: 'monospaced.elastic',
        files: ['vendor/angular-elastic/elastic.js']
    }, {
        name: 'ngMap',
        files: ['vendor/angular-google-maps/ng-map.min.js']
    }, {
        name: 'tc.chartjs',
        files: ['vendor/chartjs/tc-angular-chartjs.min.js']
    }, {
        name: 'sparkline',
        files: ['vendor/sparkline/angular-sparkline.js']
    }, {
        name: 'flow',
        files: ['vendor/ng-flow/ng-flow-standalone.min.js']
    }, {
        name: 'uiSwitch',
        files: ['vendor/angular-ui-switch/angular-ui-switch.min.js', 'vendor/angular-ui-switch/angular-ui-switch.min.css']
    }, {
        name: 'ckeditor',
        files: ['vendor/ckeditor/angular-ckeditor.min.js']
    }, {
        name: 'mwl.calendar',
        files: ['vendor/angular-bootstrap-calendar/angular-bootstrap-calendar.js', 'vendor/angular-bootstrap-calendar/angular-bootstrap-calendar-tpls.js', 'vendor/angular-bootstrap-calendar/angular-bootstrap-calendar.min.css']
    }, {
        name: 'ng-nestable',
        files: ['vendor/ng-nestable/angular-nestable.js']
    }, {
        name: 'vAccordion',
        files: ['vendor/v-accordion/v-accordion.min.js', 'vendor/v-accordion/v-accordion.min.css']
    }, {
        name: 'xeditable',
        files: ['vendor/angular-xeditable/xeditable.min.js', 'vendor/angular-xeditable/xeditable.css']
    }, {
        name: 'config-xeditable',
        files: ['vendor/angular-xeditable/config-xeditable.js']
    }, 
    {
        name: 'checklist-model',
        files: ['vendor/checklist-model/checklist-model.js']
    },{
        name: 'angularPayments',
        files: ['vendor/angular-payments/lib/angular-payments.js']
    }]
});
