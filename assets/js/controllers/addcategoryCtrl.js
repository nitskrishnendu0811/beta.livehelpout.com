'use strict';
/** 
  * controllers used for the dashboard
*/
app.controller('addcategoryCtrl', function ($rootScope, $scope,$location, ngTableParams, $filter, $http, $timeout, $state, SweetAlert,myAuth) {
    $scope.serviceurl=$rootScope.serviceurl;
   $scope.siteurl = myAuth.baseurl;
     $scope.$on('fileuploaddone', function(e, data){
       $('#cat_file').val(data.result.files[0].name);
       
   }); 
   $scope.$on("deleterequestfile", function(event, message){
     
     $('#cat_file').val('');
    });
         
     $scope.saveit=function (){
         
         var image=$('#cat_file').val();
         if($('#checkboxid').is(':checked')){
             var is_active=1;
         }else{
              var is_active=0; 
         }
         
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/editsubcategory",
            data: $.param({'id':'','is_active': is_active,'name': $scope.name,'image':image,'parent_id':'0'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
             if(data.error==1){
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
             }else{
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $location.path("/admin/category" );
                    
                    
                    
             }
           
        });
         
         
     }
     
    $scope.back=function(id){
        $location.path("/admin/category" );
        
    }
     
   
});

