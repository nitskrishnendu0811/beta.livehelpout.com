'use strict';
/** 
  * controllers used for the dashboard
*/
app.controller('addsubcategoryCtrl', function ($rootScope, $scope,$location, ngTableParams, $filter, $http, $timeout, $state, SweetAlert,myAuth) {
    $scope.serviceurl=$rootScope.serviceurl;
   $scope.siteurl = myAuth.baseurl;
    
         
     $scope.saveit=function (){
         
         
         if($('#checkboxid').is(':checked')){
             var is_active=1;
         }else{
              var is_active=0; 
         }
         
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/editsubcategory",
            data: $.param({'id':'','is_active': is_active,'name': $scope.name,'parent_id': $scope.parent_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
             if(data.error==1){
                
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
             }else{
                 
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                   
                      $location.path("/admin/sub-category/"+$scope.parent_id );
                    
                    
                    
             }
           
        });
         
         
     }
     
    $scope.back=function(id){
        $location.path("/admin/sub-category/"+id );
        
    }
     
   
});

