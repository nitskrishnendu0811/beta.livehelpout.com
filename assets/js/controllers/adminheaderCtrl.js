'use strict';
/** 
  * controller for angular-aside
  * Off canvas side menu to use with ui-bootstrap. Extends ui-bootstrap's $modal provider.
*/
app.controller('adminheaderCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth) {
    myAuth.updateAdminUserinfo(myAuth.getAdminAuthorisation());
    $scope.adminloggedindetails = myAuth.getAdminNavlinks();
    $rootScope.$on("admin_update_parent_controller", function(event, message) {
        $scope.adminloggedindetails = message;
       
    });
    
    //console.log(myAuth.isAdminLoggedIn())
    $scope.isAdminLoggedIn = myAuth.isAdminLoggedIn();
    if ($scope.isAdminLoggedIn)
    {

    }
    else
    {
        $location.path("/adminlogin/signin");
   }
    
    $scope.logout = function() {
       
        $scope.loginalertmessage = false;
        $cookieStore.put('admins', null);
        $scope.loggedindetails = '';
    
        $location.path("/adminlogin/signin");
    }

});