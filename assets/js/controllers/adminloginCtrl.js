'use strict';
/** 
  * controller for angular-aside
  * Off canvas side menu to use with ui-bootstrap. Extends ui-bootstrap's $modal provider.
*/
app.controller('adminloginCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth) {
   $scope.login = function() {
        if ($scope.user_username == '' || $scope.user_username === undefined || $scope.user_password == '' || $scope.user_password === undefined)
        {
            $scope.loginalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please provide your login details.');
        }
        else
        {
            $scope.loggedindetails = '';
            $scope.loading = true;
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/loginadmin",
                data: $.param({
                    'username': $scope.user_username,
                    'password': $scope.user_password
                }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                if (data.msg_type == '0')
                {
                    $scope.loginalertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                    $scope.user_password = '';
                } else {
                   
                    $cookieStore.put('admins', data.userdetails);
                    $scope.loginalertmessage = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.user_username = '';
                    $scope.user_password = '';
                    myAuth.updateAdminUserinfo(myAuth.getAdminAuthorisation());
                    $scope.adminloggedindetails = myAuth.getAdminNavlinks();
                    
                    $rootScope.$emit('admin_update_parent_controller', $scope.adminloggedindetails);
                   
                   
                    $location.path("admin/dashboard");
                }
            });
        }
    }

});