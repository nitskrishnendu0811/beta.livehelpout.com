'use strict';
/** 
  * controllers used for the dashboard
*/
app.controller('categoryCtrl', function ($rootScope, $scope,$location, ngTableParams, $filter,myAuth, $http, $timeout, $state, SweetAlert) {
    $scope.serviceurl=$rootScope.serviceurl;
    $scope.siteurl = myAuth.baseurl;
     $scope.loadlisting=function(){
        
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategoriesall",
            data: $.param({'type': 'parent'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allcategories = data.allcategories;
           
           
        });
         
     }
    
    $scope.getclass=function (index){
       
        if(index%2==0){
             return "odd";
        }else{
             return "even";
        }
        
    }
    $scope.sort = function(keyname){
        
		$scope.sortKey = keyname;   //set the sortKey to the param passed
		$scope.reverse = !$scope.reverse; //if true make it false and vice versa
	}
        
    $scope.redaddcat=function(){
        $location.path("/admin/add-category" );
    }
        
     $scope.redirecttosubcat=function(id){
         $location.path("/admin/sub-category/"+id );
         
     }  
        
     $scope.editcategory = function(id){
        
		 $location.path("/admin/edit-category/"+id );
	} 
        
         $scope.deletethis=function(id){
            
            
            var r = confirm("Are you sure to delete this category?");
    if (r == true) {
           
            $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/deletecat",
            data: $.param({'id': id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
               
                   if(data.error==1){
                
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
             }else{
                 
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                     $http({
                        method: "POST",
                        url: $scope.siteurl + "php/deletefiles_category.php",
                        data: $.param({'filename': data.image}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).success(function(data) {

                        });
                    $scope.loadlisting();
                    
                   
                    
                    
             }
            
           
                });
            } 
        }
        
        
    $scope.loadlisting();
   
});

