'use strict';
/** 
  * controllers used for the dashboard
*/
app.controller('editcategoryCtrl', function ($rootScope, $scope,$location, ngTableParams, $filter, $http, $timeout, $state, SweetAlert,myAuth) {
    $scope.serviceurl=$rootScope.serviceurl;
   $scope.siteurl = myAuth.baseurl;
     $scope.$on('fileuploaddone', function(e, data){
       $('#cat_file').val(data.result.files[0].name);
       
   }); 
   
      $scope.$on("deleterequestfile", function(event, message){
     var previmg=$('#prev_cat_file').val();
     $('#cat_file').val(previmg);
    });
         
      $scope.loadlisting=function(){
        
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/catdetails",
            data: $.param({'id': $scope.catid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
             $scope.name = data.name;
             $("#hidden_name").val($scope.name);
             $scope.is_active=data.is_active;
             $scope.parent_id=data.parent_id;
             $scope.image=data.image;
            if($scope.is_active==1){
                $("#checkboxid").attr('checked','checked');
            }
           
        });
         
     }    
         
         
     $scope.saveit=function (){
         
         var image=$('#cat_file').val();
         if($('#checkboxid').is(':checked')){
             var is_active=1;
         }else{
              var is_active=0; 
         }
         
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/editsubcategory",
            data: $.param({'id':$scope.catid,'is_active': is_active,'name': $scope.name,'image':image,'parent_id':'0'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
             if(data.error==1){
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
             }else{
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    //$scope.loadlisting();
                    $location.path("/admin/category" );
                    
                    
                    
             }
           
        });
         
         
     }
     
    $scope.back=function(id){
        $location.path("/admin/category" );
        
    }
     
     $scope.loadlisting();
   
});

