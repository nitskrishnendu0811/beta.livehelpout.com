'use strict';
/** 
  * controllers used for the dashboard
*/
app.controller('editsubcategoryCtrl', function ($rootScope, $scope,$location, ngTableParams, $filter, $http, $timeout, $state, SweetAlert,myAuth) {
    $scope.serviceurl=$rootScope.serviceurl;
   $scope.siteurl = myAuth.baseurl;
    
     $scope.loadlisting=function(){
        
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/catdetails",
            data: $.param({'id': $scope.subcatid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
             $scope.name = data.name;
             $("#hidden_name").val($scope.name);
             $scope.is_active=data.is_active;
             $scope.parent_id=data.parent_id;
            if($scope.is_active==1){
                $("#checkboxid").attr('checked','checked');
            }
           
        });
         
     }
     
     $scope.saveit=function (){
         
         
         if($('#checkboxid').is(':checked')){
             var is_active=1;
         }else{
              var is_active=0; 
         }
         
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/editsubcategory",
            data: $.param({'id': $scope.subcatid,'is_active': is_active,'name': $scope.name,'parent_id': $scope.parent_id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
             if(data.error==1){
                 var pastname=$("#hidden_name").val();
                
                     $("#real_name").val(pastname);
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
             }else{
                 
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
             }
           
        });
         
         
     }
     
    $scope.back=function(id){
        $location.path("/admin/sub-category/"+id );
        
    }
        
        
    $scope.loadlisting();
   
});

