'use strict';
/** 
  * controllers used for the user dashboard
*/
app.controller('accountsettingCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, $compile,socket) {
     var tagc=0;
     $scope.siteurl = myAuth.baseurl;
     $scope.youtubevideo='';
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     } 
        console.log($scope.loggedindetails.id);   
        $scope.getuserdetails = function() {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getuserdetails",
                data: $.param({'userid': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.userdata=data.userdetails;
                $scope.usertimezone=data.userdetails.timezone;
                $scope.alltimezones = data.userdetails.alltimezones;
                
            });
        }
        $scope.getuserdetails();
    
    $scope.update = function(){
        $scope.loading = true;
        var FormData = {
            'id' : $scope.loggedindetails.id,
            'username' : $scope.userdata.username,
            'email' : $scope.userdata.email,
            'password' : $scope.password
        };
        
        if ($scope.userdata.username == '' || $scope.userdata.username === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter your username.');
        }
        else if ($scope.userdata.email == '' || $scope.userdata.email === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter email.');
        }
        else if ($scope.password != '' && ($scope.password!=$scope.confpassword) ) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Password and confirm password not matched.');
        }
        else {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/updateAccountSetting",
                data: $.param(FormData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                $scope.alertmessage = true;
                if (data.msg_type == 1) {
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.userdata=data.userdetails;
                    $scope.usertimezone=data.userdetails.timezone;
                    $cookieStore.put('users', data.userdetails);
                    $cookieStore.put('isactivation', 1);
                    setTimeout(function()
                    {
                        $scope.$apply(function() {
                            myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                            $scope.loggedindetails = myAuth.getUserNavlinks();
                            $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                            
                        });
                    }, 2000);
                } else {
                    $scope.alertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                }
            });
        }
    }
    
    
    $scope.$on('fileuploaddone', function(e, data){
       var beforefiles=$('#hiddenfiles').val();
       if(beforefiles==1){
           var filejson=[data.result.files[0].name];
       }
       else
       {
           var filejson;
           var filearray=$.parseJSON(beforefiles);
           var arr = $.map(filearray, function(el) { return el; });
           arr.push(data.result.files[0].name);
           filejson=arr;
      }
      $('#hiddenfiles').val(JSON.stringify(filejson));
   }); 
   
   $scope.$on("deleterequestfile", function(event, message){
      var filejson=[];
      var beforefiles=$.parseJSON($('#hiddenfiles').val());
      for (var i = 0; i < beforefiles.length; i++) {
          if (beforefiles[i] != message) {
              filejson.push(beforefiles[i]);
          }
      }
      if(filejson.length==0)
      {
        $('#hiddenfiles').val('1');
      }
      else
      {
        $('#hiddenfiles').val(JSON.stringify(filejson));
      }
   });
   
   $scope.switchToMentor= function(){
        $('.dropdown-1').hide();
        $('.dropdown-menu1').hide();
        $('.dropdown-menu2').hide();
        $scope.loginalertmessage = false;
        $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getuserdetails",
                data: $.param({'userid': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
               $http({
                 method: "POST",
                 url: $rootScope.serviceurl + "users/changerole",
                 data: $.param({'userid': $scope.loggedindetails.id,'role':'mentor'}),
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
               }).success(function(datauser) {
               });
                if(data.msg_type==1 && data.is_profile_completed==1)
                {
                  data.userdetails.role=2;
                  $cookieStore.put('users', data.userdetails);
                  myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                  $scope.loggedindetails = myAuth.getUserNavlinks();
                  $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                  $location.path("/frontend/mentor-dashboard");
                }
                else if(data.msg_type==1 && data.is_profile_completed==0)
                {
                  $http({
                        method: "POST",
                        url: $rootScope.serviceurl + "users/logoutuser",
                        data: $.param({'userid': $scope.loggedindetails.id}),
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                  }).success(function(logoutdata) {
                        $cookieStore.put('users', null);
                        $scope.loggedindetails = '';
                        $scope.loggedin = false;
                        $scope.onlinestat=false;
                        $scope.notloggedin = true;
                        $location.path("/frontend/mentor-signup-step1/"+data.encoded_id); 
                  });
                }
        });
    }
    
    /*$scope.dashboardload = function(){
        var isactivation = $cookieStore.get('isactivation');
        if (isactivation == 1) {
            $scope.getallcategoriesstep1();
            $scope.whytolike();
        }
       $('#dropdown-menu').hide();
       $cookieStore.put('isactivation', 0);
    }*/
    
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(cheboxkey) {
        var idx = $scope.selection.indexOf(cheboxkey);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(cheboxkey);
        }

        $("#whylike").val($scope.selection.join());
        $scope.selectedwhy = $scope.selection.join();
    };

    
   // $scope.dashboardload();
});


