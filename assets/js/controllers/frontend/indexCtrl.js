'use strict';
/** 
  * controllers used for the home page
*/
app.controller('indexCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile) {
    
 $scope.cakeurl=  $rootScope.serviceurl;  
 $scope.siteurl=myAuth.baseurl;
 $scope.loggedindetails = myAuth.getUserNavlinks();

  $scope.loadthis=function (){
   setTimeout(function()
   {
     $('.mentorheaderclass').hide();
     $('.mainheaderclass').show();
   }, 1000);
  }     

  $scope.search=function(){
    $location.path("/frontend/search-result/");
  }  
 
  $scope.getallcategories = function() {
     $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type':'parent'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
     }).success(function(data) {
         if (data.is_categories_exist == 0)
         {
             $scope.showcategories = false;
         }
         else
         {
             $scope.showcategories = true;
             $scope.totalcategories=data.allcategories.length;
         }
         $scope.allcategories = data.allcategories;
     });
   }

   $scope.getcategorieswithsubcats = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategorieswithsubcats",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.is_categories_exist == 0)
            {
                $scope.showcategorieswithsub = false;
            }
            else
            {
                $scope.showcategorieswithsub = true;
		$scope.totalcategoriessub=data.allcategorieswithsub.length;
            }
            $scope.allcategoriessub = data.allcategorieswithsub;
        });
   }
   
   $scope.getallmentors = function() {
       $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
       if ($scope.isUserLoggedIn)
       {
         var userid=$scope.loggedindetails.id;
       }
       else
       {
         var userid='';
       }
       $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/getallmentors",
            data: $.param({'userid': userid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.is_experts_available==0)
            {
              $scope.nomentoravailable=true;
            }
            else
            {
              $scope.nomentoravailable=false;
            }
            $scope.allrelatedmentors=data.experts;
       });
   }
    
   $scope.showhidden=function(){
     $('#buttonareashowhide').html($compile('<a href="javascript:void(0);" ng-click="showless();">Less Categories</a>')($scope));
     $('.hiddenpart').css('visibility','visible');
     $('.hiddenpart').fadeIn();
   }

   $scope.sendtosearch=function(name){
     var newval=name;
     if(newval.indexOf(" ")>-1)
     {                 
       newval=newval.replace(/\ /g, "+");                
     }
     if(newval.indexOf("/")>-1){                 
       newval=newval.replace(/\//g, "+");                
     }
     $location.path("/frontend/search-result/"+newval);
   }

   $scope.showless=function(){
     $('#buttonareashowhide').html($compile('<a href="javascript:void(0);" ng-click="showhidden();">More Categories</a>')($scope));
     $('.hiddenpart').fadeOut();
   }
   
   $scope.getallcategories();    
   $scope.getcategorieswithsubcats();
   $scope.loadthis();  
   $scope.getallmentors();
});

