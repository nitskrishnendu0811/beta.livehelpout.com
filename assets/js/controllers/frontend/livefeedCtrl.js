'use strict';
/** 
  * controllers used for the user live feed
*/
app.controller('livefeedCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, $compile,socket){
    $rootScope.$on("updatelivefeedonpage", function(event, message){
     var hid=$('#hidden_activetab').val();
     if(hid==1){
        $scope.getalllivefeeds();
     }else{
        $scope.filterresult();
     }
    });
    
    $rootScope.$on("updaterequestlivefeedonpage", function(event, message){
     var hid=$('#hidden_activetab').val();
     if(hid==1){
        $scope.getalllivefeeds();
     }else{
        $scope.filterresult();
     }
    });
    
    $('.dropdown-menu2').hide();
    $scope.siteurl = myAuth.baseurl;
    $scope.youtubevideo='';
    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
    $scope.loggedindetails = myAuth.getUserNavlinks();
    $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
    if ($scope.isUserLoggedIn)
    {

    }
    else
    {
       $location.path("/frontend/home");
    }    
    $scope.tags = [];
    $scope.getUserDetails = function(){
          $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/getdetails",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) 
                {
                    if(data.userdetails.tags)
                    {
                        $scope.tags = data.userdetails.tags;
                    }
                });
    }
    $scope.getUserDetails();
     
    $scope.getalllivefeeds = function() {
          $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "helps/getopenrequests",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) 
                {
                   $("#loader").hide();
                   if(data.related_request_exist==1)
                   {
                        $scope.relatedrequestsdiv=false;
                        $scope.relatedrequests=data.relatedrequests;
                        $('.tab-pane').removeClass('in');
                        $('.tab-pane').removeClass('active');
                        $('#related_request').addClass('in');
                        $('#related_request').addClass('active');
                   }else{
                        $('.tab-pane').removeClass('in');
                        $('.tab-pane').removeClass('active');
                        $('#related_request').addClass('in');
                        $('#related_request').addClass('active');
                        $scope.relatedrequestsdiv=true;
                  }
          });
   }
   
   $scope.getalllivefeedsother = function() {
        $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "helps/allotherrequests",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
             }).success(function(data) {
                  $("#loader").hide();
                  if(data.related_request_exist==1){
                    $scope.allotherdiv=false;
                    $('.tab-pane').removeClass('in');
                    $('.tab-pane').removeClass('active');
                    $('#allother').addClass('in');
                    $('#allother').addClass('active');
                    $scope.allotherrequests=data.relatedrequests;
                 }else{
                    $('.tab-pane').removeClass('in');
                    $('.tab-pane').removeClass('active');
                    $('#allother').addClass('in');
                    $('#allother').addClass('active');
                    $scope.allotherdiv=true;
                 }
         });
    }
    
  $scope.openrequesthistorytab=function(divid){
   $("#loader").show();
    if(divid=='allother'){
        $scope.allotherdiv=true;
        $scope.relatedrequestsdiv=true;
        $scope.filterresult();
        $('#hidden_activetab').val(2);
    }
    if(divid=='related_request'){
        $scope.relatedrequestsdiv=true;
        $scope.getalllivefeeds();
        $('#hidden_activetab').val(1);
    }
     $('.requesthistoryli').removeClass('in');
     $('.requesthistoryli').removeClass('active');
     $(".tab-pane").hide();
     $("#"+divid).fadeIn();

     $('.tab-pane').removeClass('in');
     $('.tab-pane').removeClass('active');

     $('#'+divid+"-tab").addClass('in');
     $('#'+divid+"-tab").addClass('active');
   }
   $scope.getalllivefeeds();
   
   $scope.openfilter=function(){
     var filtertagstring=$('#filtertagstring').val();
     if(filtertagstring != '')
     {
      $('#filter_tag').slideToggle('slow');
     }
      $('#filter-cat').slideToggle('slow');
   }
   
   $scope.getallcategories = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'parent'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allparentcategories = data.allcategories;
        });
    }
    $scope.getallcategories();
    
    $scope.getsubcats = function(parentid,catname) {
       $('#filtertagstring').val('1');
       $('#filter_tag').show();
       var flag=false;
       $( ".selectedmaincat" ).each(function( index ) {
         if($(this).attr('id') == 'selectedmaincat_'+parentid)
         {
           flag=true;
         }
       });
       if(!flag)
       {
        $('#selectedtags').prepend($compile('<li id="selectedmaincat_'+parentid+'" class="selectedmaincat"><a href="javascript:void(0);">'+catname+' <span class="glyphicon glyphicon-remove" aria-hidden="true" ng-click="removemaincat(\''+catname+'\',\''+parentid+'\')"></span></a><input type="hidden" value="'+parentid+'" class="hidden_main_cat"></li>')($scope));
       
        $('#maincat'+parentid).addClass('act');
        $('#maincat'+parentid).children('a').append('&nbsp;&nbsp;<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        $scope.filterresult();
        $scope.getsubcategories(parentid);
      }
    }
    
    $scope.filterresult=function(){
       var filtermaincatstring='';
       var filtersubcatstring='';
       $( ".hidden_main_cat" ).each(function( index ) {
           filtermaincatstring+=$(this).val()+',';
       });
       $( ".hidden_sub_cat" ).each(function( index ) {
           filtersubcatstring+=$(this).val()+',';
       });
       if(filtermaincatstring!='')
       {
         $("#loaderfilter").show();
         $('.filterresdiv').hide();
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getfilterresults",
            data: $.param({'main_cats': filtermaincatstring,'sub_cats':filtersubcatstring,'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
         }).success(function(data) {
            $("#loaderfilter").hide();
            $("#loader").hide();
            $('.filterresdiv').show();
            if(data.filter_result_exist==1){
                $scope.allotherdiv=false;
                $('.tab-pane').removeClass('in');
                $('.tab-pane').removeClass('active');
                $('#allother').addClass('in');
                $('#allother').addClass('active');
                $scope.allotherrequests=data.relatedrequests;
            }else{
                 $('.tab-pane').removeClass('in');
                 $('.tab-pane').removeClass('active');
                 $('#allother').addClass('in');
                 $('#allother').addClass('active');
                 $scope.allotherdiv=true;
            }
         });
       }
       else
       {
         $scope.getalllivefeedsother();
       } 
    }
    
    $scope.gridOptions = {
     urlKey      :     "original_url",
     sortKey     :     "nth",
     onClicked   :     function(image) {
                        $rootScope.$emit('showbigimageslidemain', image);                        
                      },
     onBuilded   :     function() {                       
                        $scope.$apply()
                      },
     margin      :     2
   }
   
   $scope.likerequest = function(requestid){
       $rootScope.$emit('likerequestemit', requestid);
   }
    
    $scope.getsubcategories=function(parentid){
       $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'child','parent_id':parentid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
           if(data.is_categories_exist==1)
           {
              data.allcategories.forEach(function(categorydata,index){ 
               $('#subcategories').prepend($compile('<li class="parentid_'+parentid+'" id="sub_'+categorydata.Category.id+'"><a href="javascript:void(0);" ng-click="selectsubcattags(\''+categorydata.Category.id+'\',\''+categorydata.Category.name+'\',\''+parentid+'\');">'+categorydata.Category.name+'</a></li>')($scope));
             });
           }
        });
    }
    
    $scope.selectsubcattags=function(id,name,parentid){
       var flag=false;
       $( ".selectedmaincat" ).each(function( index ) {
         if($(this).attr('id') == 'selectedmaincat_'+id)
         {
           flag=true;
         }
       });
       if(!flag)
       {
        $('#selectedtags').prepend($compile('<li id="selectedmaincat_'+id+'" class="selectedmaincat selectedsubcat_'+parentid+'"><a href="javascript:void(0);">'+name+' <span class="glyphicon glyphicon-remove" aria-hidden="true" ng-click="removesubcat(\''+name+'\',\''+id+'\')"></span></a><input type="hidden" value="'+id+'" class="hidden_sub_cat"></li></li>')($scope));
      
        $('#sub_'+id).addClass('act');
        $('#sub_'+id).children('a').append('&nbsp;&nbsp;<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span>');
        $scope.filterresult();
      }
    }
    
    $scope.removemaincat=function(name,parentid){
       $('#selectedmaincat_'+parentid).remove();
       $('.selectedsubcat_'+parentid).remove();
       $('#maincat'+parentid).removeClass('act');
       $('#maincat'+parentid).children('a').children('span').remove();
       $('.parentid_'+parentid).remove();
       $scope.filterresult();
       if($('.selectedmaincat').length==0)
       {
        $('#filtertagstring').val('');
        $('#filter_tag').hide();
       }
    }
   
    $scope.removesubcat=function(name,id){
       $('#selectedmaincat_'+id).remove();
       $('#sub_'+id).removeClass('act');
       $('#sub_'+id).children('a').children('span').remove();
       $scope.filterresult();
    }
   
});


