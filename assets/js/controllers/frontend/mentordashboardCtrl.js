'use strict';
/** 
  * controllers used for the frontend header
*/
app.controller('mentordashboardCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth) {
     $scope.siteurl = myAuth.baseurl;
     $scope.youtubevideo='';
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     $scope.previewer = [];
     $scope.badge = [];
     $scope.button = [];
     $scope.badge.text = 'contact';
     $scope.previewer.tab = 'github';
     $scope.button.text = 'meLight';
     
     $scope.meLight = '<a href="'+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'"><img src="'+$scope.siteurl+'assets/frontend/images/b-1.png" alt="Contact me on LiveHelpOut" style="max-width:100%"></a>';
     $scope.meDark = '<a href="'+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'"><img src="'+$scope.siteurl+'assets/frontend/images/b-2.png" alt="Contact me on LiveHelpOut" style="max-width:100%"></a>';
     $scope.light = '<a href="'+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'"><img src="'+$scope.siteurl+'assets/frontend/images/b-4.png" alt="Contact me on LiveHelpOut" style="max-width:100%"></a>';
     $scope.dark = '<a href="'+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'"><img src="'+$scope.siteurl+'assets/frontend/images/b-3.png" alt="Contact me on LiveHelpOut" style="max-width:100%"></a>';
     
     $scope.liveBadge='<script data-livehelpout="'+$scope.loggedindetails.username+'" data-style="badge" id="badge" data-theme="light" src="'+$scope.siteurl+'assets/livementor.js"></script>';
     $('#badgeOfMe').html('<script data-livehelpout="'+$scope.loggedindetails.username+'" data-style="badge" id="badge" data-theme="light" src="'+$scope.siteurl+'assets/livementor.js"></script>');
     $scope.embeded_code = '<iframe src="'+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'"></iframe>';
     $scope.contact = '<a href="'+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'"><img src="'+$scope.siteurl+'assets/frontend/images/btn-1.png" alt="Contact me on LiveHelpOut" style="max-width:100%"></a>';
     $scope.session = '<a href="'+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'"><img src="'+$scope.siteurl+'assets/frontend/images/btn-2.png" alt="Contact me on LiveHelpOut" style="max-width:100%"></a>';
     $scope.help = '<a href="'+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'"><img src="'+$scope.siteurl+'assets/frontend/images/btn-3.png" alt="Contact me on LiveHelpOut" style="max-width:100%"></a>';
     $scope.profile_link = ''+$scope.siteurl+'frontend/mentor/'+$scope.loggedindetails.username+'';
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     
     $scope.afterLoad = function(){
     	$http({
		       method: "POST",
		       //url: $rootScope.serviceurl + "users/getdetails",
		       //data: $.param({'userid': $scope.loggedindetails.id}),
		       url: $rootScope.serviceurl + "users/getdetailsById",
		       data: $.param({'username': $scope.loggedindetails.id}),
		       headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		   }).success(function(data) {
		       console.log(data);
		       $scope.userdata=data.userdetails;
		       $scope.expertise=data.expertise;
		       $scope.tips=data.tips;
		       //console.log(data.userdetails.why_to_like);
		       console.log($scope.userdata.id);
		       $scope.usertimezone=data.userdetails.timezone;
		       
		       $scope.imgtype = data.userdetails.imgtype;
		       $scope.alltimezones = data.userdetails.alltimezones;
		       $scope.tags=data.userdetails.tags;
		       console.log(data.userdetails.tags);
		       $scope.whytolikelists=data.userdetails.whytolike.whytolike;
		       console.log($scope.whytolikelists);
		       $scope.shareName = data.userdetails.name + ' | LiveHelpOut';
		       $scope.define = '';
		       angular.forEach($scope.whytolikelists, function(value, key) {
				  console.log(key + ': ' + value);
				  $scope.define += ', '+value;
				});
		       
		   });
     }
     $scope.afterLoad();
     
     $scope.popWindow = function(url,winName,w,h) {
        if (window.open) {
		   if (poppedWindow) { poppedWindow = ''; }
		  var windowW = w;
		   var windowH = h;
		   var windowX = (screen.width/2)-(windowW/2);
		   var windowY = (screen.height/2)-(windowH/2);
		   var myExtra = "status=no,menubar=no,resizable=yes,toolbar=no,scrollbars=yes,addressbar=no";
		   var poppedWindow = window.open(url,winName,'width='+w+',height='+h+',top='+windowY+',left=' + windowX + ',' + myExtra + '');
	    }
	    else {
		   alert('Your security settings are not allowing our popup windows to function. Please make sure your security software allows popup windows to be opened by this web application.');
	    }
	    return false;
        
        $(".steps").removeClass('act');
        if (divId == 'showstep1') {
            $("#headerstep1").addClass('act');
        } else if (divId == 'showstep2') {
            $("#headerstep2").addClass('act');
        }

        $scope.step1alertmesage = false;
        $("#" + presentdivId).slideUp(1000, function() {
            $("#" + divId).slideDown(1000);
        });
    }
     
     /*function popWindow(url,winName,w,h) {
	    
	} */   
});

