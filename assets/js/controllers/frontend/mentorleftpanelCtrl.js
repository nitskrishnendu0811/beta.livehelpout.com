'use strict';
/** 
  * controllers used for the frontend header
*/
app.controller('mentorleftpanelCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, socket) {
    
    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
    $scope.loggedindetails = myAuth.getUserNavlinks();
console.log($scope.loggedindetails.fbid);
    $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
    
     if($scope.isUserLoggedIn){
     	socket.emit('joinuser', $scope.loggedindetails.id);
     	$scope.imgtype = $scope.loggedindetails.imgtype;
     	if($scope.loggedindetails.fbid!="")
            {
              $scope.headersiteimage = false;
            }else{
            	$scope.headersiteimage = true;
            }
     }
     
    $scope.redirecttopage=function(redirectto){
        $location.path("/frontend/"+redirectto);
    }
    
    $scope.relatedrequestcount = function() {
        $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "helps/getopenrequests",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
             }).success(function(data) {
                   if(data.related_request_exist==1){
                       $scope.openrequestcount=data.total;
                   }
                   else
                   {
                     $scope.openrequestcount=0;
                   }
       });
    }
    $scope.relatedrequestcount();
    
    
    $scope.switchToHelpee= function(){
        $('.dropdown-1').hide();
        $('.dropdown-menu1').hide();
        $('.dropdown-menu2').hide();
        $scope.loginalertmessage = false;
        $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/getuserdetails",
                data: $.param({'userid': $scope.loggedindetails.id}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
                $http({
                 method: "POST",
                 url: $rootScope.serviceurl + "users/changerole",
                 data: $.param({'userid': $scope.loggedindetails.id,'role':'helpee'}),
                 headers: {'Content-Type': 'application/x-www-form-urlencoded'},
               }).success(function(datauser) {
               });
                if(data.msg_type==1)
                {
                    data.userdetails.role=1;
                    $cookieStore.put('users', data.userdetails);
                    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                    $scope.loggedindetails = myAuth.getUserNavlinks();
                    $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                    $location.path("/frontend/dashboard");
                }
                
        });
    }
    
});

