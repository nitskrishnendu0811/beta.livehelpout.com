'use strict';
/** 
  * controllers used for the user dashboard
*/
app.controller('mentorprofileCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, $compile,$stateParams) {
    $scope.siteurl = myAuth.baseurl;
    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
    $scope.loggedindetails = myAuth.getUserNavlinks();
    $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
    var username = $stateParams.username;//uname[cnt-1];
        console.log(username);
    $scope.expertleveleditbtn=true;
    $scope.timezoneeditbtn=true;
    $scope.requiredhelpeditbtn=true;
    $scope.expertaboutmebtn=true;
    $scope.expert_level_options = [{ name: "Select Expert Level", value: 0},{ name: "Beginner", value: 1 }, { name: "Intermediate", value:2 }, { name: "Advanced", value:3 }];
    $scope.socialmeta = true;
    /*if ($scope.isUserLoggedIn)
    {

    }
    else
    {
       $location.path("/frontend/home");
    }*/
    $scope.getuserdetails = function() {
        var url =  $location.url();
        var uname = url.split('/');
        var cnt = uname.length;
        
        var username = $stateParams.username;//uname[cnt-1];
        console.log(username);
        $http({
            method: "POST",
            //url: $rootScope.serviceurl + "users/getdetails",
            //data: $.param({'userid': $scope.loggedindetails.id}),
            url: $rootScope.serviceurl + "users/getdetailsByUserName",
            data: $.param({'username': username}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            console.log(data);
            $scope.userdata=data.userdetails;
            $scope.expertise=data.expertise;
            $scope.tips=data.tips;
            //console.log(data.userdetails.why_to_like);
            console.log($scope.userdata.id);
            $scope.usertimezone=data.userdetails.timezone;
            if(data.userdetails.expert_level=='Beginner')
            {
             $scope.user_expert_level=$scope.expert_level_options[1];
            }
            else if(data.userdetails.expert_level=='Intermediate')
            {
             $scope.user_expert_level=$scope.expert_level_options[2];
            }
            else if(data.userdetails.expert_level=='Advanced')
            {
             $scope.user_expert_level=$scope.expert_level_options[3];
            }
            else
            {
             $scope.user_expert_level=$scope.expert_level_options[0];
            }
            if(data.userdetails.fbid)
            {
              $scope.siteimage = false;
            }else{
            	$scope.siteimage = true;
            }
             $scope.imgtype = data.userdetails.imgtype;
            $scope.alltimezones = data.userdetails.alltimezones;
            $scope.tags=data.userdetails.tags;
            console.log(data.userdetails.tags);
            $scope.whytolikelists=data.userdetails.whytolike.whytolike;
            $scope.shareName = data.userdetails.name + ' | LiveHelpOut';
		       $scope.define = '';
		       angular.forEach($scope.whytolikelists, function(value, key) {
				  console.log(key + ': ' + value);
				  $scope.define += ', '+value;
				});
			//$('#shareogTitle').val($scope.shareName);	
			//$('#shareogContent").val($scope.define);
			$("meta[property='og\\:title']").attr("content", $scope.shareName);
			$("meta[property='og\\:description']").attr("content", $scope.shareName);
            if($scope.isUserLoggedIn && $scope.userdata.id==$scope.loggedindetails.id)
            {
            	$scope.timezoneeditbtn=true;$scope.meTrue = true;$scope.requiredhelpeditbtn=true;$scope.expertleveleditbtn=true;$scope.expertaboutmebtn=true;
            }else{
            	$scope.timezoneeditbtn=false;$scope.meTrue = false;$scope.requiredhelpeditbtn=false;$scope.expertleveleditbtn=false;$scope.expertaboutmebtn=false;
            }
        });
    }
    $scope.getuserdetails();
    
    $scope.loadTags = function(query) {
        return $http.get($rootScope.serviceurl + "categories/getsubcategories/" + query);
    }
    
    $scope.getallpreviousrequests = function() {
        $scope.loggedindetails = myAuth.getUserNavlinks();
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/getallpreviousrequests",
            data: $.param({'userid': $scope.loggedindetails.id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if(data.is_previous_request_exist==0)
            {
              $scope.norequestexist=false;
            }
            else
            {
              $scope.norequestexist=true;
            }
            $scope.allpreviousrequests=data.allrequests;
        });
    }
    $scope.getallpreviousrequests();
    
    $scope.submit_edit_expart_level= function() {
        if($scope.user_expert_level.value!=0)
        {
          $scope.loading = true;
          $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/updateexpertlevel",
            data: $.param({'userid': $scope.loggedindetails.id,'expertlevel':$scope.user_expert_level.name}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function(data) {
             $scope.getuserdetails();
             $scope.loading = false;
             $scope.expertleveleditbtn=true;
          });
        }
    }
    
    $scope.submit_edit_timezone= function() {
      
        if($scope.usertimezone!='' && $scope.usertimezone!=null)
        {
          $scope.loadingtimezone = true;
          $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/updatetimezone",
            data: $.param({'userid': $scope.loggedindetails.id,'timezone':$scope.usertimezone}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function(data) {
             $scope.getuserdetails();
             $scope.loadingtimezone = false;
             $scope.timezoneeditbtn=true;
          });
        }
    }
    
    $scope.submit_edit_requiredhelp= function() {
        if($scope.tags.length!=0)
        {
          $scope.loadingtopics = true;
          $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/updaterequirement",
            data: $.param({'userid': $scope.loggedindetails.id,'chosencats': $scope.tags}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function(data) {
             $scope.getuserdetails();
             $scope.loadingtopics = false;
             $scope.requiredhelpeditbtn=true;
          });
        }
    }
    
    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(cheboxkey) {
        var idx = $scope.selection.indexOf(cheboxkey);
        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.selection.push(cheboxkey);
        }

        $("#whylike").val($scope.selection.join());
        $scope.selectedwhy = $scope.selection.join();
    };
    
    $scope.submit_edit_aboutme= function() {
        if ($scope.selectedwhy == '' || $scope.selectedwhy === undefined) {
        }else{
               $scope.loadingaboutme = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/updatewhytolike",
                    data: $.param({'userid': $scope.loggedindetails.id, 'updatewhytolike': $scope.selectedwhy }),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.getuserdetails();
                        $scope.loadingaboutme = false;
                        $scope.expertaboutmebtn=true;
                    }
                });
        }
    }
    
    $scope.add_tip = function(){
    	console.log($scope);
    		$scope.loadingaboutme = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/addTip",
                    data: $.param({'userid': $scope.userdata.id, 'tipid': $scope.tip.id,'title' : $scope.tip.title,'description': $scope.tip.description, 'tags': $scope.tip.tags }),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.getuserdetails();
                        $scope.loadingaboutme = false;
                        $scope.expertaboutmebtn=true;
                        $('.modal').modal('hide');
                    }
                });
    }
    
    $scope.editTip = function(id){
    	console.log($scope);
    		$scope.loadingaboutme = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/getTip",
                    data: $.param({'id': id }),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    if (data.msg_type == 1)
                    {
                        $scope.getuserdetails();
                        $scope.loadingaboutme = false;
                        $scope.expertaboutmebtn=true;
                        $('.modal').modal('hide');
                    }
                });
    }
    
    $scope.openModalTip = function(divId) {
        $('.modal').modal('hide');
        setTimeout(function()
        {
            $('#' + divId).modal('show');
        }, 500);
    }
    
    $scope.open_edit_expart_level = function() {
      $scope.expertleveleditbtn=false;
    }
    $scope.close_edit_expart_level = function() {
      $scope.expertleveleditbtn=true;
    }
    $scope.open_edit_timezone = function() {
      $scope.timezoneeditbtn=false;
    }
    $scope.close_edit_timezone = function() {
      $scope.timezoneeditbtn=true;
    }
    $scope.open_edit_requiredhelp = function() {
      $scope.requiredhelpeditbtn=false;
    }
    $scope.close_edit_requiredhelp = function() {
      $scope.requiredhelpeditbtn=true;
    }
    $scope.open_edit_aboutme = function() {
      $scope.expertaboutmebtn=false;
    }
    $scope.close_edit_aboutme = function() {
      $scope.expertaboutmebtn=true;
    }
});

