'use strict';
/** 
  * controllers used for the frontend header
*/
app.controller('mentorsignupCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, socket, $facebook, $compile, GooglePlus, $linkedIn) {
    $scope.siteurl=myAuth.baseurl; 
    myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     $rootScope.$on("update_parent_controller", function(event, message)
    {
        $scope.loggedindetails = message;
        if ($scope.loggedindetails)
        {
            $scope.loggedin = true;
            $scope.notloggedin = false;
            $scope.onlinestatnoti=true;
            if ($scope.loggedindetails.role == 1) {
                $scope.onlinestat = false;
            } else {
                $scope.onlinestat = true;
            }
        }
        else
        {
            $scope.loggedin = false;
            $scope.notloggedin = true;
            $scope.onlinestat = false;
            $scope.onlinestatnoti=false;
        }
    });
     if ($scope.isUserLoggedIn)
     {
         if($scope.loggedindetails.role==1){
            $location.path("/frontend/dashboard");
        }else if($scope.loggedindetails.role==2){
            $location.path("/frontend/mentor-dashboard");
        }
        
     }
     
     socket.on('getmentorloginstatus', function(data) {
     console.log('hi');
        $rootScope.$broadcast('mentorFblogin');
        $scope.$digest();
        
    });
    
    /*************************FaceBook Sign Up*************************************/   
    $scope.fbreguser = function (idmsg) {
     $scope.idmsg = idmsg;
	 if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = false;
						}else{
							$scope.regalertmessage = false;
						}
	 $facebook.login().then(function() {
      refreshment();
     });
    }
    
    function refreshment() {
     $facebook.api("/me").then( 
      function(response) {
        console.log(response);
		console.log($scope.idmsg);
        	$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/fbsignupuser",
                    data: $.param({'email': response.email,'name':response.name,'fbid':response.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = true;
						}else{
							$scope.regalertmessage = true;
						}
						//$scope.alertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '2')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               //$scope.regalertmessage = true;
					   if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = true;
						}else{
							$scope.regalertmessage = true;
						}
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    } else{
                    	$('.modal').modal('hide');
                    	$('#fbmodalmentor').modal('show');
                    	$scope.user_name = response.name;
                    	$scope.user_email = response.email;
                    	$scope.user_fbid = response.id
                    }
                });
      },
      function(err) {
            console.log(err);
            //$scope.alertmessage = true;
            //$scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
      });
    }
    
    $scope.fbusersignupmentor = function(){
    		console.log('email:'+ $scope.user_email+' name:'+$scope.user_name+' fbid'+$scope.user_fbid+' username:'+$scope.user_username);
    		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/fbsignupusernamementor",
                    data: $.param({'email': $scope.user_email,'name':$scope.user_name,'fbid':$scope.user_fbid,'username':$scope.user_username}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.fbsignupMessagementor = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else{
                    	$cookieStore.put('users', data.userdetails);
		               $scope.fbsignupMessagementor = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-signup-step1/"+data.userid); 
		               }
                    }
                });
    }


/********************Signup Gplus mentor*********************************/     
$scope.gpreguser = function (idmsg) {
	    $scope.idmsg = idmsg;
		if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = false;
						}else{
							$scope.regalertmessage = false;
						}
        GooglePlus.login().then(function (authResult) {
            console.log(authResult);

            GooglePlus.getUser().then(gregfresh);
            /*GooglePlus.getUser().then(function (user) {
                gfresh();
            });*/
        }, function (err) {
            console.log(err);
        });
    };
    
    function gregfresh(user) {
        console.log(user);
        console.log('GooGle SignUp:----'+user.name+'|'+user.id+'|'+user.picture+'|'+user.email);
        	$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/gpsignupuser",
                    data: $.param({'email': user.email,'name':user.name,'gpid':user.id,'image':user.picture}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    console.log(data);
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = true;
						}else{
							$scope.regalertmessage = true;
						}
						//$scope.signupalertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '2')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = true;
						}else{
							$scope.regalertmessage = true;
						}
					   //$scope.signupalertmessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    } else{
                    	$('.modal').modal('hide');
                    	$('#gpmodalmentor').modal('show');
                    	$scope.user_name = user.name;
                    	$scope.user_email = user.email;
                    	$scope.user_gpid = user.id;
                    	$scope.user_picture = user.picture;
                    }
                });
      
      
    }
    
    $scope.gpusersignupmentor = function(){
    		console.log('email:'+ $scope.user_email+' name:'+$scope.user_name+' gpid'+$scope.user_gpid+' username:'+$scope.user_username);
    		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/gpsignupusernamementor",
                    data: $.param({'email': $scope.user_email,'name':$scope.user_name,'gpid':$scope.user_gpid,'username':$scope.user_username,'image':$scope.user_picture}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.gpsignupMessagementor = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else{
                    	$cookieStore.put('users', data.userdetails);
		               $scope.gpsignupMessagementor = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.gpid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-signup-step1/"+data.userid); 
		               }
                    }
                });
    }
    
/*************LinkedIn Login**********************/    
     $scope.lnreguser = function(idmsg) {
	 	//IN.Event.on(IN, "auth", onLinkedInAuth);
		$scope.idmsg = idmsg;
		if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = false;
						}else{
							$scope.regalertmessage = false;
						}
	 	$linkedIn.authorize();
	 	IN.Event.on(IN, "auth", onLinkedInAuthment);
	}
	
	function onLinkedInAuthment(){
		IN.API.Profile("me")
          .fields('id','first-name','last-name','location','industry','headline','picture-urls::(original)','email-address')
          .result(linkrefreshment)
          .error(function(err){
              console.log(err);
          });
          IN.User.logout();
     } 
	
	function linkrefreshment(profile){
	
		console.log(profile);
		var member = profile.values[0];
		console.log(member.pictureUrls.length);
		if(member.pictureUrls.values.length>0)
		{
			console.log(member.pictureUrls.values[0]+' | lllllllllll');
			var image = member.pictureUrls.values[0];
		}else{
			var image = '';
		}
		
		console.log('email'+ member.emailAddress +' name'+member.firstName+' '+member.lastName +' lnid'+member.id + ' image'+image+' location'+member.location.name);
		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/lnsignupuser",
                    data: $.param({'email': member.emailAddress,'name':member.firstName+' '+member.lastName,'lnid':member.id,'image':image,'location':member.location.name}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = true;
						}else{
							$scope.regalertmessage = true;
						}
						//$scope.alertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else if(data.msg_type == '2')
                    {
                    	$cookieStore.put('users', data.userdetails);
		               if($scope.idmsg == 'login')
						{
							$scope.regalertmessage = true;
						}else{
							$scope.regalertmessage = true;
						}
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.fbid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    } else{
                    	$('.modal').modal('hide');
                    	$('#lnmodalmentor').modal('show');
                    	$scope.user_name = member.firstName+' '+member.lastName;
                    	$scope.user_email = member.emailAddress;
                    	$scope.user_picture = image;
                    	$scope.user_location = member.location.name
                    	$scope.user_lnid = member.id
                    	
                    }
                });
	}
	
	$scope.lnusersignupmentor = function(){
    		console.log('email:'+ $scope.user_email+' name:'+$scope.user_name+' lnid'+$scope.user_lnid+' username:'+$scope.user_username);
    		$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/lnsignupusernamementor",
                    data: $.param({'email': $scope.user_email,'name':$scope.user_name,'lnid':$scope.user_lnid,'username':$scope.user_username,'image':$scope.user_picture,'location':$scope.user_location}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.lnsignupMessagementor = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    }
                    else{
                    	$cookieStore.put('users', data.userdetails);
		               $scope.lnsignupMessagementor = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.imgtype = $scope.loggedindetails.imgtype;
		               if($scope.loggedindetails.lnid!="")
					  {
					    $scope.headersiteimage = false;
					  }else{
					  	$scope.headersiteimage = true;
					  }
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		                   socket.emit("mentor_login", jsonstatus);
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                  $location.path("/frontend/mentor-signup-step1/"+data.userid); 
		               }
                    }
                });
    }
        
/*********************************************/     
    $scope.fblogin = function () {
     $facebook.login().then(function() {
      refresh();
     });
    }
    
    function refresh() {
     $facebook.api("/me").then( 
      function(response) {
        console.log(response);
        	$scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/fbsignupmentor",
                    data: $.param({'email': response.email,'name':response.name,'fbid':response.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.alertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    } else if (data.msg_type == '1'){
                        $scope.alertmessage = true;
                        $scope.alert = myAuth.addAlert('success', data.msg);
                        $scope.email = '';
                        $scope.name = '';
                        
                        $location.path("/frontend/mentor-signup-step1/"+data.userid); 
                    } else{
                    	$cookieStore.put('users', data.userdetails);
		               $scope.loginalertmessage = true;
		               $scope.alert = myAuth.addAlert('success', data.msg);
		               $scope.user_username = '';
		               $scope.user_password = '';
		               myAuth.updateUserinfo(myAuth.getUserAuthorisation());
		               $scope.loggedindetails = myAuth.getUserNavlinks();
		               $scope.loggedin = true;
		               $scope.notloggedin = false;
		               $scope.onlinestatnoti=true;
		               
		               if (data.userdetails.role == 1) {
		                   $scope.onlinestat = false;
		               } else {
		                   var jsonstatus = {'userid': $scope.loggedindetails.id};
		               socket.emit("mentor_login", jsonstatus);
		                   
		                   $scope.onlinestat = true;
		                   if (data.userdetails.login_status == 1) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Instant Session';
		                   } else if (data.userdetails.login_status == 2) {
		                       var htmls = '<img src="assets/frontend/images/online.png" alt="" style="margin-right:5px; margin-top:-2px;">Online';
		                   }
		                   else if (data.userdetails.login_status == 3) {
		                       var htmls = '<img src="assets/frontend/images/away.png" alt="" style="margin-right:5px; margin-top:-2px;">Away';
		                   }
		                   else if (data.userdetails.login_status == 4) {
		                       var htmls = '<img src="assets/frontend/images/offline.png" alt="" style="margin-right:5px; margin-top:-2px;">Offline';
		                   }
		                   $("#headstatus").html(htmls + '<span class="caret"></span>');
		               }
		               
		               $('.modal').modal('hide');
		               if (data.userdetails.role == 1)
		               {
		                   $location.path("/frontend/dashboard");
		               }
		               else
		               {
		                   $location.path("/frontend/mentor-dashboard");
		               }
                    }
                });
      },
      function(err) {
            console.log(err);
            //$scope.alertmessage = true;
            //$scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
      });
    }

    $scope.mentorsignup=function(){
      if ($scope.mentorsignupForm.$valid) {
          if($scope.name == '' || $scope.name === undefined){
               $scope.regalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please provide your name.');
          }
        else if ($scope.email == '' || $scope.email === undefined)
        {
            $scope.regalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please provide your email.');
        }
        else
        {
            var emailfilter = /(([a-zA-Z0-9\-?\.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
            if (!(emailfilter.test($scope.email)))
            {
                $scope.regalertmessage = true;
                $scope.alert = myAuth.addAlert('danger', 'Please provide valid email address.');
            }
            else
            {
                $scope.loading = true;
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/signupmentor",
                    data: $.param({'email': $scope.email,'name':$scope.name}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $scope.loading = false;
                    if (data.msg_type == '0')
                    {
                        $scope.regalertmessage = true;
                        $scope.alert = myAuth.addAlert('danger', data.msg);
                    } else {
                        $scope.regalertmessage = true;
                        $scope.alert = myAuth.addAlert('success', data.msg);
                        $scope.email = '';
                        $scope.name = '';
                        $location.path("/frontend/mentor-signup-step1/"+data.userid); 
                    }
                });
            }
        }
      }
    }
});

