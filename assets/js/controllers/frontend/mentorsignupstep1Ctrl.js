'use strict';
/** 
 * controllers used for the frontend header
 */
app.controller('mentorsignupstep1Ctrl', function($rootScope, $scope, $http, $compile, $location, $sce, $cookies, $cookieStore, myAuth ) {

    $scope.loadTags = function(query) {

        return $http.get($rootScope.serviceurl + "categories/getsubcategories/" + query);
    }
    $scope.getallcategoriesstep1 = function() {
        setTimeout(function()
        {

            $('.mainheaderclass').hide();
            $('.mentorheaderclass').show();

        }, 500);
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/getcategories",
            data: $.param({'type': 'child'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.allcategories = data.allcategories;

        });


        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/getalltimezones",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.timezones = data;
            var tz = jstz.determine(); // Determines the time zone of the browser client
            $scope.timezone = tz.name();

        });

    }



    $scope.selection = [];
    $scope.toggleSelection = function toggleSelection(cheboxkey) {

        var idx = $scope.selection.indexOf(cheboxkey);

        // is currently selected
        if (idx > -1) {
            $scope.selection.splice(idx, 1);
        }

        // is newly selected
        else {
            $scope.selection.push(cheboxkey);
        }

        $("#categories_selected").val($scope.selection.join());
        $scope.categories_selected = $scope.selection.join();
    };


    $scope.step1submit = function() {
        $scope.loading = true;

        if ($scope.headline == '' || $scope.headline === undefined) {
            $scope.loading = false;
            $scope.step1alertmesage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please add your profile headline.');

        }
        else if ($scope.about == '' || $scope.about === undefined) {
            $scope.loading = false;
            $scope.step1alertmesage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please tell something about yourself.');


        } else if ($scope.price == '' || $scope.price === undefined) {
            $scope.loading = false;
            $scope.step1alertmesage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter price.');
        } else if ($scope.categories_selected == '' || $scope.categories_selected === undefined) {
            $scope.loading = false;
            $scope.step1alertmesage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please choose your expertise.');
        } else {


            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/mentorsignupstep1",
                data: $.param({'userid': $scope.userid, 'headline': $scope.headline, 'about': $scope.about, 'price': $scope.price, 'selectedcats': $scope.categories_selected, 'chosencats': $scope.tags, 'twitter': $scope.twitter, 'stackoverflow': $scope.stackoverflow}),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                if (data.msg_type == 1)
                {
                    $scope.step1alertmesage = true;
                    $scope.loading = false;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    setTimeout(function()
                    {

                        $scope.step1alertmesage = false;
                        $("#showstep1").slideUp(1000, function() {
                            $("#showstep2").slideDown(1000);
                        });


                    }, 1000);
                }
            });
        }
        $(".steps").removeClass('act');
        $("#headerstep2").addClass('act');
    }


    $scope.step2submit = function() {
        $scope.loading = true;

        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/mentorsignupstep2",
            data: $.param({
                'userid': $scope.userid,
                'availablity': $scope.availablity,
                'timezone': $scope.timezone,
                'company': $scope.company,
                'job_title': $scope.job_title,
                'why_mentor': $scope.why_mentor,
                'experience_details': $scope.experience_details,
                'video_url': $scope.video_url,
                'website_url': $scope.website_url

            }),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.msg_type == 1)
            {
                $scope.step1alertmesage = true;
                $scope.loading = false;
                $scope.alert = myAuth.addAlert('success', data.msg);
                setTimeout(function()
                {

                    $scope.laststep();



                }, 1000);
            }
        });

    }


    $scope.laststep = function() {
        $scope.step1alertmesage = false;
        $scope.fetchexpertise();
        $("#showstep2").slideUp(1000, function() {
            $("#showstep3").slideDown(1000);

        });
        $(".steps").removeClass('act');
        $("#headerstep3").addClass('act');
        $scope.expertise_related_tags = [];
        $scope.expertise_experience = '';
        $scope.expertise_moredetails = '';
        $scope.expertise_rating = '';
    }


    $scope.fetchexpertise = function() {
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/getexpertisedetails",
            data: $.param({'userid': $scope.userid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {

            $scope.expertisetabs = data;

        });

    }

    $scope.values = function(text, id) {

        var previdx = $('.active').children('a').attr('id');
        var tprevid = previdx.split('__');
        var previd = tprevid[tprevid.length - 1];
        $scope.formattedjson =
                {
                    "expertise_experience": $scope.expertise_experience,
                    "expertise_moredetails": $scope.expertise_moredetails,
                    "expertise_rating": $scope.expertise_rating,
                    "tagname": previd
                    , "expertise_related_tags": $scope.expertise_related_tags};

        $('#hid_' + previd + '_hidden').val(JSON.stringify($scope.formattedjson));


        var autopopulate = $("#hid_" + text + "_hidden").val();

        if (autopopulate != '') {
            var parsed = jQuery.parseJSON(autopopulate);

            $scope.expertise_related_tags = parsed.expertise_related_tags;
            $scope.expertise_experience = parsed.expertise_experience;
            $scope.expertise_moredetails = parsed.expertise_moredetails;
            $scope.expertise_rating = parsed.expertise_rating;
        } else {
            $scope.expertise_related_tags = [];
            $scope.expertise_experience = '';
            $scope.expertise_moredetails = '';
            $scope.expertise_rating = '';
        }




        $("#h2tag").html(text);
        $("#spantag").html(text);
        $(".tabs").removeClass('active');
        $("#maintag__" + id).parent('li').addClass('active');

    }

    $scope.prev = function(divId, presentdivId) {
        $(".steps").removeClass('act');
        if (divId == 'showstep1') {
            $("#headerstep1").addClass('act');
        } else if (divId == 'showstep2') {
            $("#headerstep2").addClass('act');
        }




        $scope.step1alertmesage = false;
        $("#" + presentdivId).slideUp(1000, function() {
            $("#" + divId).slideDown(1000);
        });




    }

    $scope.addtobodyexpertise = function() {



        $(".foradd").removeClass('active');

        var tags = $scope.expertise_tags;
        $(".tabs").removeClass('active');
        $(".foradd").removeClass('active');
        $("#addexperisemodal").modal('hide');
        var exist = $('.tabs').length;


        tags.forEach(function(locationdata, index) {
            var flg = true;
            $(".tabs").each(function(index) {

                if (locationdata.text.toUpperCase() == $(this).children('a').attr('id').toUpperCase()) {
                    flg = false;
                }
            });

            if (flg) {
                if (index == 0) {
                    var val = '{"tagname":"' + locationdata.text + '","expertise_related_tags":[],"expertise_experience":"","expertise_moredetails":"","expertise_rating":""}';
                    var html = '<li role="presentation" class="tabs active" >';
                    html += ' <a  ng-click="values(\'' + locationdata.text + '\',' + exist + ');" id="maintag__' + locationdata.text + '" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true" class="ng-binding" style="cursor:pointer;">' + locationdata.text + '</a>';
                    html += '<input type="hidden" id="hid_' + locationdata.text + '_hidden" class="hiddens"  >';
                    html += '</li>';
                    $("#h2tag").html(locationdata.text);
                    $("#spantag").html(locationdata.text);
                } else {
                    var val = '{"tagname":"' + locationdata.text + '","expertise_related_tags":[],"expertise_experience":"","expertise_moredetails":"","expertise_rating":""}';
                    var html = '<li role="presentation" class="tabs">';
                    html += ' <a  ng-click="values(\'' + locationdata.text + '\',' + exist + ');" id="maintag__' + locationdata.text + '" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="true" class="ng-binding" style="cursor:pointer;">' + locationdata.text + '</a>';
                    html += '<input type="hidden" id="hid_' + locationdata.text + '_hidden" class="hiddens"  >';
                    html += '</li>';
                }

            } else {
                $(".tabs").each(function(index) {
                    if (index == 0) {
                        $(this).addClass('active');

                        $("#h2tag").html($(this).children('a').attr('id'));
                        $("#spantag").html($(this).children('a').attr('id'));
                    }

                });
            }

            $("#myTab li:last").before($compile(html)($scope));
            exist = exist + 1;
        });
        $scope.expertise_related_tags = [];
        $scope.expertise_experience = '';
        $scope.expertise_moredetails = '';
        $scope.expertise_rating = '';


    }

    $scope.openaddmodal = function() {
        $(".foradd").removeClass('active');

        var previd = $('.active').children('a').attr('id');

        $scope.formattedjson =
                {"tagname": previd
                    , "expertise_related_tags": $scope.expertise_related_tags,
                    "expertise_experience": $scope.expertise_experience,
                    "expertise_moredetails": $scope.expertise_moredetails,
                    "expertise_rating": $scope.expertise_rating};



        $('#hid_' + previd + '_hidden').val(JSON.stringify($scope.formattedjson));



        $scope.expertise_tags = '';
        $("#addexperisemodal").modal('show');



    }

    $scope.closemodal = function() {
        $(".foradd").removeClass('active');
        $("#addexperisemodal").modal('hide');
        $(".tabs").each(function(index) {
            if (index == 0) {
                $(this).addClass('active');

                $("#h2tag").html($(this).children('a').attr('id'));
                $("#spantag").html($(this).children('a').attr('id'));
            }

        });

    }
    $scope.remove = function() {
        $('.active').remove();
        $(".foradd").removeClass('active');
        $scope.expertise_related_tags = [];
        $scope.expertise_experience = '';
        $scope.expertise_moredetails = '';
        $scope.expertise_rating = '';
        var thisid;
        $(".tabs").each(function(index) {
            if (index == 0) {
                $(this).addClass('active');
                thisid = $(this).children('a').attr('id');
                $("#h2tag").html($(this).children('a').attr('id'));
                $("#spantag").html($(this).children('a').attr('id'));
            }

        });




        var autopopulate = $("#hid_" + thisid + "_hidden").val();

        if (autopopulate != '') {
            var parsed = jQuery.parseJSON(autopopulate);

            $scope.expertise_related_tags = parsed.expertise_related_tags;
            $scope.expertise_experience = parsed.expertise_experience;
            $scope.expertise_moredetails = parsed.expertise_moredetails;
            $scope.expertise_rating = parsed.expertise_rating;
        } else {
            $scope.expertise_related_tags = [];
            $scope.expertise_experience = '';
            $scope.expertise_moredetails = '';
            $scope.expertise_rating = '';
        }


    }

    $scope.step3submit = function() {
        $scope.loading = true;
        $(".foradd").removeClass('active');
        var arr = [];
        var previd = $('.active').children('a').attr('id');

        $scope.formattedjson =
                {"tagname": previd,
                    "expertise_related_tags": $scope.expertise_related_tags,
                    "expertise_experience": $scope.expertise_experience,
                    "expertise_moredetails": $scope.expertise_moredetails,
                    "expertise_rating": $scope.expertise_rating
                };

        $('#hid_' + previd + '_hidden').val(JSON.stringify($scope.formattedjson));

        $(".hiddens").each(function(index) {
            arr.push($(this).val());
        });
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/mentorsignupstep3",
            data: $.param({'userid': $scope.userid, 'tagdetails': arr}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.msg_type == 1)
            {
                $scope.loading = false;
                $scope.step1alertmesage = true;
                $scope.loading = false;
                $scope.alert = myAuth.addAlert('success', data.msg);
                $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "users/getswitcheduserdetails",
                    data: $.param({'userid': $scope.userid}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(userdata) {
                    if (userdata.msg_type == 1 && userdata.userdetails.role == 1)
                    {
                        userdata.userdetails.role = 2;
                        $cookieStore.put('users', userdata.userdetails);
                        myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                        $scope.loggedindetails = myAuth.getUserNavlinks();
                        $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                        $('.mainheaderclass').show();
                        $('.mentorheaderclass').hide();
                        $location.path("/frontend/mentor-dashboard");
                    }
                    else if (userdata.msg_type == 1 && userdata.userdetails.role == 2)
                    {
                        $location.path("/frontend/home");
                    }
                });
            } else {
                $scope.loading = false;
                $scope.step1alertmesage = true;
                $scope.loading = false;
                $scope.alert = myAuth.addAlert('danger', data.msg);
            }

        });
    }
    $scope.getallcategoriesstep1();
});

