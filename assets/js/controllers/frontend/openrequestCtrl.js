'use strict';
/** 
  * controllers used for the open request
*/
app.controller('openrequestCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth,$compile,socket) {
     $scope.siteurl = myAuth.baseurl;
     myAuth.updateUserinfo(myAuth.getUserAuthorisation());
     $scope.loggedindetails = myAuth.getUserNavlinks();
     $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
     if ($scope.isUserLoggedIn)
     {

     }
     else
     {
       $location.path("/frontend/home");
     }
     
     $rootScope.$on("updatelivefeedonpage", function(event, message){
      var hid=$('#hidden_activetab').val();
      if(hid==1){
        $scope.relatedrequest();
      }else{
        $scope.getotherrequests();
      }
    });
     
    $rootScope.$on("updaterequestlivefeedonpage", function(event, message){
        var hid=$('#hidden_activetab').val();
        if(hid==1){
            $scope.relatedrequest();
        }else{
            $scope.getotherrequests();
        }
    });
     
    $scope.relatedrequest = function() {
           $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "helps/getopenrequests",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                  $("#loader").hide();
                  $('.relatedrequestsdiv').fadeIn();
                  if(data.related_request_exist==1){
                    $scope.relatedrequestsdiv=false;
                    $scope.relatedrequests=data.relatedrequests;
                    $('.tab-pane').removeClass('in');
                    $('.tab-pane').removeClass('active');
                    $('#related_request').addClass('in');
                    $('#related_request').addClass('active');
                }else{
                    $('.tab-pane').removeClass('in');
                    $('.tab-pane').removeClass('active');
                    $('#related_request').addClass('in');
                    $('#related_request').addClass('active');
                    $scope.relatedrequestsdiv=true;
                }
             });
             $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "interests/skipreason",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
              }).success(function(data) {
                      $scope.skipoptions=data.whyskip;
              });        
  }
  
  $scope.gridOptions = {
     urlKey      :     "original_url",
     sortKey     :     "nth",
     onClicked   :     function(image) {
                        $rootScope.$emit('showbigimageslidemain', image);                        
                      },
     onBuilded   :     function() {                       
                        $scope.$apply()
                      },
     margin      :     2
  }
  
  $scope.likerequest = function(requestid){
     $rootScope.$emit('likerequestemit', requestid);
  }
    
  $scope.getotherrequests = function() {
        $http({
                    method: "POST",
                    url: $rootScope.serviceurl + "helps/allotherrequests",
                    data: $.param({'userid': $scope.loggedindetails.id}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).success(function(data) {
                    $("#loader").hide();
                    $('.allotherdiv').fadeIn();
                  if(data.related_request_exist==1){
                    $scope.allotherdiv=false;
                    $('.tab-pane').removeClass('in');
                    $('.tab-pane').removeClass('active');
                    $('#allother').addClass('in');
                    $('#allother').addClass('active');
                    $scope.allotherrequests=data.relatedrequests;
                }else{
                    $('.tab-pane').removeClass('in');
                    $('.tab-pane').removeClass('active');
                    $('#allother').addClass('in');
                    $('#allother').addClass('active');
                    $scope.allotherdiv=true;
                }
             });
 }

 $scope.open_active_request=function(id){
    $("#activerequestdetails"+id).slideToggle(500)
 }
    
 $scope.openrequesthistorytab=function(divid){
    $("#loader").show();
    if(divid=='allother'){
        $scope.getotherrequests();
        $('.relatedrequestsdiv').hide();
        $('#hidden_activetab').val(2);
    }
    if(divid=='related_request'){
        $scope.relatedrequest();
        $('.allotherdiv').hide();
        $('#hidden_activetab').val(1);
    }
    
   $('.requesthistoryli').removeClass('in');
   $('.requesthistoryli').removeClass('active');
   $(".tab-pane").hide();
   $("#"+divid).fadeIn();

   $('.tab-pane').removeClass('in');
   $('.tab-pane').removeClass('active');

   $('#'+divid+"-tab").addClass('in');
   $('#'+divid+"-tab").addClass('active');
 }
         
 $scope.skipthis=function(divid ,type){
     $('#whyskipthis').modal('show');
     $scope.hiddenskip=divid;
     $scope.hiddenskiptype=type;
     $scope.other_reason='';
 }  
       
 $scope.closemodal=function(divid ){
    $scope.skipalert=false;
     $('#whyskipthis').modal('hide');
     $scope.other_reason='';
 } 
 
 $scope.saveskiprequest=function(){
    $scope.loading=true;
    var reasontextarea=$('#other_reason').val();
     if($scope.reason==''){
             var rsn=reasontextarea;
     }else{
             var rsn=$scope.reason;
     }
     if(rsn=='' || rsn===undefined){
          $scope.loading=false;
          $scope.skipalert=true;
          $scope.alert = myAuth.addAlert('danger', 'Please enter a reason to skip.');
     }else{
         
         $scope.skipalert=false;
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "interests/submitskipreason",
            data: $.param({'userid': $scope.loggedindetails.id,'help_id':$scope.hiddenskip,'is_skipped':1,'skipreason':rsn}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
         }).success(function(data) {
           $scope.loading=false;
           
            $('#whyskipthis').modal('hide');
             if( $scope.hiddenskiptype=='related'){
                $scope.relatedrequest(); 
             }else if( $scope.hiddenskiptype=='other'){
               $scope.getotherrequests();   
             }
         }); 
     }
 } 

 $scope.showinterest=function(divid ,type){
  $scope.loading=true;
  $http({
           method: "POST",
           url: $rootScope.serviceurl + "interests/showinterest",
           data: $.param({'userid': $scope.loggedindetails.id,'help_id':divid}),
           headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
           var intereststatus={'status':'interest','userid':$scope.loggedindetails.id};
           $rootScope.$broadcast('updateliveinterest',intereststatus);
           $scope.loading=false;
           $("#"+type+"_interest_"+divid).hide();
           $("#right_"+divid).show();
           $('#textarea_'+divid).removeAttr(' disabled');
           $('#textarea_'+divid).focus();
        });
 }

 $scope.addcomment=function(divid ,type){
  $scope.loading1=true;
  var comment=$("#textarea_"+divid).val();
  if(comment!=''){
     $http({
            method: "POST",
            url: $rootScope.serviceurl + "comments/addComment",
            data: $.param({'userid': $scope.loggedindetails.id,'help_id':divid,'comment':comment}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          }).success(function(data) {
             $scope.loading1=false;
             var html='<div id="comment_'+data.lastid+'">';
               html+='<div class="col-md-1 col-sm-1 col-xs-3" style="padding-left:15px;">';
               html+='<div class="profile_img-new">';
               html+='<img src="'+$scope.siteurl+'assets/upload/user_images/'+data.image+'">';
               html+='</div> ';
               html+='</div>';
               html+='<div  class="col-md-11 col-sm-11 col-xs-9">';
               html+=' <h2><a class="ng-binding">'+data.name+'</a>';
               html+='<span style="margin-left:10px; font-size: 12px;">'+data.time+'</span>';
               html+='<span style="margin-left:10px; font-size: 14px; cursor: pointer;"  ng-click="deletecomment('+data.lastid+');">X</span>';
               html+='</h2> ';
               html+='<p>'+comment+'</p>';
               html+='</div>';
               html+='</div><hr  style="clear: both;">';
           $('.commentarea').append($compile(html)($scope));
           $("#textarea_"+divid).val('')
           socket.emit("update_request_feed", data.lastid);
        });
    }else{
        $scope.loading1=false;
        $("#textarea_"+divid).focus()
    }
 }

 $scope.deletecomment=function(commentid){
  var txt;
  var r = confirm("are you sure you want to delete this comment?");
  if (r == true) {
    $http({
            method: "POST",
            url: $rootScope.serviceurl + "comments/deletecomment",
            data: $.param({'commentid':commentid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
           $('#'+commentid).remove();
           socket.emit("update_request_feed", data.lastid);
        });
   }        
 }
 $scope.relatedrequest();
});


