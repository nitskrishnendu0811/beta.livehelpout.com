'use strict'; 
/** 
  * controllers used for the open request
*/
app.controller('paymentreturnCtrl', function ($rootScope, $scope, $http, $location,myAuth,$stateParams,$cookieStore) {
   //console.log('Token =========== ', $stateParams.token);
   $scope.token = $stateParams.token;
   $scope.getPaymentDetails = function(){
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "paypals/payment_details",
            data: 'token=' + $scope.token,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function(data){
            if(data.status=='success')
            {
                 $http({
                            method: "POST",
                            url: $rootScope.serviceurl + "users/getuserdetails",
                            data: 'userid=' + data.user_id ,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).success(function(userdata){
                            if(userdata && userdata.msg_type)
                            {
                                myAuth.updateUserinfo(userdata.userdetails);
                                $cookieStore.put('users',userdata.userdetails);
                                $location.search({});
                                $location.path('/frontend/payment_success');
                            }
                        })
            }
            else
            {
                $location.search({});
                $location.path('/frontend/payment_cancel');
            }
        });
    }
    
    if($stateParams.PayerID)
    {
        $scope.getPaymentDetails();
    }
    else
    {
        $location.search({});
        $location.path('/frontend/payment_cancel');
    }
});


