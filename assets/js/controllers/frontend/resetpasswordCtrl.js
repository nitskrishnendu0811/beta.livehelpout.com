'use strict';
/** 
  * controllers used for the reset password
*/


app.controller('resetpasswordCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth) {

    $scope.loaddetailsreset = function() {

        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/getactivationuserdetails",
            data: $.param({'id': $scope.uid, 'type': 'resetpassword'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.msg_type == 1) {
                $scope.resetuseremail = data.email;

            } else {
                $location.path("/");
            }
        });
    }


    $scope.resetpassword = function() {
        $scope.loading = true;
        var FormData = {
            'id': $scope.uid,
            'password': $scope.newpassword,
            'confpassword': $scope.newconfirmpassword,
        };
        if ($scope.newpassword == '' || $scope.newpassword === undefined) {
            $scope.loading = false;
            $scope.resetalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter password.');
        }
        else if ($scope.newpassword != $scope.newconfirmpassword) {
            $scope.loading = false;
            $scope.resetalertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Password does not match.');
        } else {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/resetpassword",
                data: $.param(FormData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                $scope.resetalertmessage = true;
                if (data.msg_type == 1) {
                    $scope.alert = myAuth.addAlert('success', data.msg);
                } else {
                    $scope.resetalertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                }
            });
        }
    }

    $scope.loaddetailsreset();
});


