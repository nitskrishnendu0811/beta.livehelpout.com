'use strict';
/** 
 * controllers used for the search
 */
app.controller('searchResultCtrl', function($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth, $compile, socket) {
    $scope.siteurl = myAuth.baseurl;
    $scope.isUserLoggedIn = myAuth.isUserLoggedIn();
    
     if ($scope.isUserLoggedIn)
     {
         $scope.loggedindetails = myAuth.getUserNavlinks();
         $scope.userid=$scope.loggedindetails.id;
     }else{
          $scope.userid='';
     }
     
    
    $scope.searchresultrequests = function(keyvalue, type) {
        /*if (type == 1) {
            $("#loader").show();
            $("#relatedrequestsdiv").hide();
        }*/
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/searchresultrequests",
            data: $.param({'keyword': keyvalue,'userid':$scope.userid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data)
        {
            $("#loader").hide();
            if (data.related_request_exist == 1)
            {
                $(".relatedrequestsdiv").fadeIn();
                $scope.relatedrequestsdiv = false;
                $scope.relatedrequests = data.relatedrequests;
                $('.tab-pane').removeClass('in');
                $('.tab-pane').removeClass('active');
                $('#related_request').addClass('in');
                $('#related_request').addClass('active');
            } else {
                $(".relatedrequestsdiv").fadeIn();
                $('.tab-pane').removeClass('in');
                $('.tab-pane').removeClass('active');
                $('#related_request').addClass('in');
                $('#related_request').addClass('active');
                $scope.relatedrequestsdiv = true;
            }
        });
    }
    
    $scope.likerequest = function(requestid){
        $rootScope.$emit('likerequestemit', requestid);
    }

    $scope.searchresultmentors = function(keyvalue, type) {
        if (type == 1) {
            $("#loader").show();
            $("#allotherdiv").hide();
        }
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "helps/searchresultmentors",
            data: $.param({'keyword': keyvalue}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $("#loader").hide();
            if (data.ismentorexist == 1) {
                $scope.allotherdiv = false;
                $(".allotherdiv").fadeIn();
                $('.tab-pane').removeClass('in');
                $('.tab-pane').removeClass('active');
                $('#allother').addClass('in');
                $('#allother').addClass('active');
                $scope.allotherrequests = data.mentors;
            } else {
                $(".allotherdiv").fadeIn();
                $('.tab-pane').removeClass('in');
                $('.tab-pane').removeClass('active');
                $('#allother').addClass('in');
                $('#allother').addClass('active');
                $scope.allotherdiv = true;
            }
        });
    }
    
    $rootScope.$on("updaterequestlivefeedonpage", function(event, message){
        var key=$scope.keyword;
        $("#search_keywords").val(key);
        var hid = $('#hidden_activetab').val();
        if (hid == 1) {
            $scope.searchresultrequests( $scope.keyword, 1);
        }   
    });
    
    $rootScope.$on("updatelivefeedonpage", function(event, message){
        var key=$scope.keyword;
        $("#search_keywords").val(key);
        var hid = $('#hidden_activetab').val();
        if (hid == 1) {
            $scope.searchresultrequests( $scope.keyword, 1);
        }   
    });

   $scope.loadfunction = function() {
    var key=$scope.keyword;
    $("#search_keywords").val(key);
        var hid = $('#hidden_activetab').val();
        if (hid == 1) {
            $scope.searchresultrequests( $scope.keyword, 1);
        } else {
            $scope.searchresultmentors( $scope.keyword, 1);
        }
    }
    $scope.loadfunction();

    $scope.openrequesthistorytab = function(divid) {
        $("#loader").show();
        if (divid == 'allother') {
            $(".relatedrequestsdiv").hide();
            $(".allotherdiv").hide();
            $scope.searchresultmentors($scope.keyword, 0);
            $('#hidden_activetab').val(2);
        }
        if (divid == 'related_request') {
            $(".relatedrequestsdiv").hide();
            $(".allotherdiv").hide();
            $scope.searchresultrequests($scope.keyword, 0);
            $('#hidden_activetab').val(1);
        }

        $('.requesthistoryli').removeClass('in');
        $('.requesthistoryli').removeClass('active');
        $(".tab-pane").hide();
        $("#" + divid).fadeIn();

        $('.tab-pane').removeClass('in');
        $('.tab-pane').removeClass('active');

        $('#' + divid + "-tab").addClass('in');
        $('#' + divid + "-tab").addClass('active');
    }
    
    $rootScope.$on("updatesearchmentorlist", function(event, message){
       var key=$scope.keyword;
       $("#search_keywords").val(key);
        var hid = $('#hidden_activetab').val();
        if (hid == 1) {
        } else {
            $scope.searchresultmentors( $scope.keyword, 0);
        }
    });

   $scope.gridOptions = {
    urlKey      :     "original_url",
    sortKey     :     "nth",
    onClicked   :     function(image) {
                        $rootScope.$emit('showbigimageslidemain', image);                        
                      },
    onBuilded   :     function() {                       
                        $scope.$apply()
                      },
    margin      :     2
   }
    

});


