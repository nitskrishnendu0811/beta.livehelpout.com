'use strict';
/** 
  * controllers used for the user activation
*/
app.controller('useractivationCtrl', function ($rootScope, $scope, $http, $location, $sce, $cookies, $cookieStore, myAuth) {
console.log($scope.userid);   
 $scope.loaddetails = function() {
        $scope.showactivationdiv = true;
        $scope.showactivationdivbig = true;
        $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/getactivationuserdetails",
            data: $.param({'id': $scope.userid, 'type': 'activation'}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            if (data.msg_type == 2) {
                $scope.showactivationdiv = false;
                $scope.showactivationdivbig = true;
                $scope.alert = myAuth.addAlert('warning', data.msg);
            }
            else if (data.msg_type == 1) {
                $scope.showactivationdiv = true;
                $scope.showactivationdivbig = false;
                $scope.useremail = data.email;
                $scope.name = data.name;
                $scope.timezones = data.timezone;
                var tz = jstz.determine(); // Determines the time zone of the browser client
                $scope.timezone = tz.name();
            } else {
                $location.path("/frontend/home");
            }
        });
    }
    $scope.loaddetails();

    $scope.activate = function() {
        $scope.loading = true;
        var FormData = {
            'id': $scope.userid,
            'name': $scope.name,
            'username': $scope.username,
            'password': $scope.password,
            'confpassword': $scope.confpassword,
            'timezone': $scope.timezone
        };

        if ($scope.name == '' || $scope.name === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter your name.');
        }
        else if ($scope.username == '' || $scope.username === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter your username.');
        }
        else if ($scope.password == '' || $scope.password === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please enter password.');
        }
        else if ($scope.timezone == '' || $scope.timezone === undefined) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Please select a timezone.');
        }
        else if ($scope.password != $scope.confpassword) {
            $scope.loading = false;
            $scope.alertmessage = true;
            $scope.alert = myAuth.addAlert('danger', 'Password does not match.');
        } else {
            $http({
                method: "POST",
                url: $rootScope.serviceurl + "users/activateuser",
                data: $.param(FormData),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).success(function(data) {
                $scope.loading = false;
                $scope.alertmessage = true;
                if (data.msg_type == 1) {
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $cookieStore.put('users', data.userdetails);
                    $cookieStore.put('isactivation', 1);
                    setTimeout(function()
                    {
                        $scope.$apply(function() {
                            myAuth.updateUserinfo(myAuth.getUserAuthorisation());
                            $scope.loggedindetails = myAuth.getUserNavlinks();
                            $rootScope.$emit('update_parent_controller', $scope.loggedindetails);
                            if($scope.loggedindetails.role==1)
                            {
                              $location.path("/frontend/dashboard");
                            }
                            else
                            {
                              var helpstatus={'status':'mentor','userid':$scope.loggedindetails.id};
                              $rootScope.$broadcast('mentoractivation',helpstatus);
                              $location.path("/frontend/mentor-dashboard");
                            }
                        });
                    }, 2000);
                } else {
                    $scope.alertmessage = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
                }
            });
        }
    }

});




