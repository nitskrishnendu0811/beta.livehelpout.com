'use strict';
/** 
  * controllers used for the dashboard
*/
app.controller('helpeeCtrl', function ($rootScope, $scope,$location, ngTableParams, $filter,myAuth, $http, $timeout, $state, SweetAlert) {
    $scope.serviceurl=$rootScope.serviceurl;
    $scope.siteurl = myAuth.baseurl;
     $scope.loadlisting=function(){
        
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/index",
            data: $.param({'usertype': 2}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            $scope.users = data.users;
           
           
        });
         
     }
    
    $scope.getclass=function (index){
       
        if(index%2==0){
             return "odd";
        }else{
             return "even";
        }
        
    }
    $scope.sort = function(keyname){
        
		$scope.sortKey = keyname;   //set the sortKey to the param passed
		$scope.reverse = !$scope.reverse; //if true make it false and vice versa
	}
        
    
         $scope.deletethis=function(id){
            
            
            var r = confirm("Are you sure to delete this user?");
    if (r == true) {
           
            $http({
            method: "POST",
            url: $rootScope.serviceurl + "users/delete",
            data: $.param({'id': id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
               
                   if(data.error==1){
                
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
             }else{
                 
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    
                    $scope.loadlisting();
                    
                   
                    
                    
             }
            
           
                });
            } 
        }
        
        
    $scope.loadlisting();
   
});

