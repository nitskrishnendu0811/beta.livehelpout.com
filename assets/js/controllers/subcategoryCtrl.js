'use strict';
/** 
  * controllers used for the dashboard
*/
app.controller('subcategoryCtrl', function ($rootScope, $scope,$location, ngTableParams, $filter, $http, $timeout, $state, SweetAlert,myAuth) {
    $scope.serviceurl=$rootScope.serviceurl;
   $scope.siteurl = myAuth.baseurl;
    
     $scope.loadlisting=function(){
        
         $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/listsubcategories",
            data: $.param({'catid': $scope.catid}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
            //if(data.is_categories_exist==1){
            $scope.allcategories = data.allsubcategories;
            $scope.parentname=data.parent_name;
            //}
           
        });
         
     }
    
    $scope.getclass=function (index){
       
        if(index%2==0){
             return "odd";
        }else{
             return "even";
        }
        
    }
    $scope.sort = function(keyname){
        
		$scope.sortKey = keyname;   //set the sortKey to the param passed
		$scope.reverse = !$scope.reverse; //if true make it false and vice versa
	}
        
      $scope.editsubcategory = function(id){
        
		 $location.path("/admin/edit-sub-category/"+id );
	}   
        
        $scope.deletethis=function(id){
            
            
            var r = confirm("Are you sure to delete this subcategory?");
    if (r == true) {
           
            $http({
            method: "POST",
            url: $rootScope.serviceurl + "categories/deletesubcat",
            data: $.param({'id': id}),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        }).success(function(data) {
               
                   if(data.error==1){
                
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('danger', data.msg);
             }else{
                 
                    $scope.msg = true;
                    $scope.alert = myAuth.addAlert('success', data.msg);
                    $scope.loadlisting();
             }
            
           
                });
            } 
        }
        
        $scope.redaddsubcat=function(parentid){
             $location.path("/admin/add-sub-category/"+parentid );
            
        }
        
        
    $scope.loadlisting();
   
});

