var app = angular.module('LiveHelpoutApp', ['Live-Help-out']);
//////////////Facewbook Config////////////
app.config(['$facebookProvider', function($facebookProvider) {
    $facebookProvider.setAppId('854993934578994');
 }])
 .run( function( $rootScope ) {
  // Load the facebook SDK asynchronously
  (function(){
     // If we've already installed the SDK, we're done
     if (document.getElementById('facebook-jssdk')) {return;}

     // Get the first script element, which we'll use to find the parent node
     var firstScriptElement = document.getElementsByTagName('script')[0];

     // Create a new script element and set its id
     var facebookJS = document.createElement('script'); 
     facebookJS.id = 'facebook-jssdk';

     // Set the new script's source to the source of the Facebook JS SDK
     facebookJS.src = '//connect.facebook.net/en_US/all.js';

     // Insert the Facebook JS SDK into the DOM
     firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
   }());
 });

 app.config(['$locationProvider',function($locationProvider) {
    //$locationProvider.html5mode({ enabled: true, requir/eBase: false });
    //$locationProvider.html5Mode(true).requireBase(fasle);
     $locationProvider.html5Mode(true).hashPrefix('!');
}]);
//////////Linkedin Config//////////////////
app.config(['$linkedInProvider',function($linkedInProvider) {
   $linkedInProvider
    .set('appKey', '75zyq7nmiua3kw')
    .set('scope', 'r_basicprofile r_emailaddress')
    .set('authorize', true);
}]);

/////////Gplus Config///////////////////// 
 app.config(['GooglePlusProvider', function(GooglePlusProvider) {
     GooglePlusProvider.init({
        clientId: '277562301078-4hl2b5v5292jbbo91ha980a95c6vllst',
        apiKey: 'AIzaSyCf5OP6nJFnLaUPk1SM2BBUken_oYDB0YE',
        scopes: ['profile', 'email']
     });
}]);

 var isOnGitHub = window.location.hostname === 'blueimp.github.io',
     url = isOnGitHub ? '//jquery-file-upload.appspot.com/' : '/php/';

 app.config(['$httpProvider', 'fileUploadProvider', function($httpProvider, fileUploadProvider) {
         $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        fileUploadProvider.defaults.redirect = window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        );
        if (isOnGitHub) {
            // Demo settings:
            angular.extend(fileUploadProvider.defaults, {
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                maxFileSize: 999000,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf)$/i
            });
        }
 }])
 .controller('RequestFileUploadController', [
            '$scope', '$http', '$filter', '$window',
            function ($scope, $http) {
                $scope.options = {
                    url: url
                };
                if (!isOnGitHub) {
                    $scope.loadingFiles = true;
                    $http.get(url)
                        .then(
                            function (response) {
                                $scope.loadingFiles = false;
                                //$scope.queue = response.data.files || [];
                            },
                            function () {
                                $scope.loadingFiles = false;
                            }
                        );
                }
            }
  ])
  .controller('FileDestroyController', [
            '$scope', '$http',
            function ( $scope, $http) {
                var file = $scope.file,
                    state;
                if (file.url) {
                    file.$state = function () {
                        return state;
                    };
                    file.$destroy = function () {
                        state = 'pending';
                        return $http({
                            url: file.deleteUrl,
                            method: file.deleteType
                        }).then(
                            function () {
                                state = 'resolved';
                                $scope.clear(file);
                                $scope.$emit('deleterequestfile',file.name);
                            },
                            function () {
                                state = 'rejected';
                            }
                        );
                    };
                } else if (!file.$cancel && !file._index) {
                    file.$cancel = function () {
                        $scope.clear(file);
                    };
                }
            }
  ]);

 app.run(['$rootScope', '$state', '$stateParams',
 function ($rootScope, $state, $stateParams) {
    // Attach Fastclick for eliminating the 300ms delay between a physical tap and the firing of a click event on mobile browsers
    FastClick.attach(document.body);

    // Set some reference to access them from any scope
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    // GLOBAL APP SCOPE
    // set below basic information
    //$rootScope.serviceurl = "http://localhost/prohelp/";
    //$rootScope.serviceurl = "http://livehelpout.com/beta.admin";
    $rootScope.serviceurl = "http://livehelpout.com/beta.admin/";
    $rootScope.app = {
        name: 'LiveHelpout', // name of your project
        author: 'NITS', // author's name or company name
        //admindescription: 'LiveHelpout Admin', // brief description
       //frontdescription: 'LiveHelpout', // brief description
        description:'LiveHelpout',
        keywords:'LiveHelpout', 
        version: '1.0', // current version
        year: ((new Date()).getFullYear()), // automatic current year (for copyright information)
        isMobile: (function () {// true if the browser is a mobile device
            var check = false;
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                check = true;
            };
            return check;
        })(),
        layout: {
            isNavbarFixed: true, //true if you want to initialize the template with fixed header
            isSidebarFixed: true, // true if you want to initialize the template with fixed sidebar
            isSidebarClosed: false, // true if you want to initialize the template with closed sidebar
            isFooterFixed: false, // true if you want to initialize the template with fixed footer
            theme: 'theme-1', // indicate the theme chosen for your project
            logo: 'assets/images/logo.png', // relative path of the project logo
        }
    };
    $rootScope.user = {
        name: 'Peter',
        job: 'ng-Dev',
        picture: 'app/img/user/01.jpg'
    };
}]);

// translate config
app.config(['$translateProvider',
function ($translateProvider) {

    // prefix and suffix information  is required to specify a pattern
    // You can simply use the static-files loader with this pattern:
    $translateProvider.useStaticFilesLoader({
        prefix: 'assets/i18n/',
        suffix: '.json'
    });

    // Since you've now registered more then one translation table, angular-translate has to know which one to use.
    // This is where preferredLanguage(langKey) comes in.
    $translateProvider.preferredLanguage('en');

    // Store the language in the local storage
    $translateProvider.useLocalStorage();

}]);

// Angular-Loading-Bar
// configuration
app.config(['cfpLoadingBarProvider',
function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.includeSpinner = false;

}]);

// Stripe Config
app.config(function() {
    window.Stripe.setPublishableKey('pk_test_CqTE1EEu2t9EVQZm9OUSHQ9b');
});
