<?php
class DATABASE_CONFIG {
    public $default = array(
        'datasource' => 'Mongodb.MongodbSource',
        'host' => '127.0.0.1',
        'database' => 'betaprohelp',
        'port' => 27017,
        'prefix' => '',
        'persistent' => 'true',
    );
}
