<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
        
        public function beforeRender() {        
          $adminRoute = Configure::read('Routing.prefixes');
          $pref = $this->params['prefix'];
            if ($pref == 'admin') {
                    if ($this->params['controller']=='users' && ($this->params['action'] == 'admin_index' || $this->params['action'] == 'admin_login')) {
                    
                     }else{
                        if ($this->Session->read('userid') == '') {
                            return $this->redirect(array('controller' => 'users', 'action' => 'index'));
                        }
                    }
                    $this->layout = 'admin_default';
            } else {
                    $this->layout = 'default';
            }
         }
    
         public function beforeFilter() {
                $tmezone=$this->Session->read('clienttimezone');
                if(isset($tmezone) && $tmezone!=''){
                   Configure::write('Config.timezone', $tmezone);
                   date_default_timezone_set($tmezone);
                }
         }
         
         public function send_mail($from = null, $to = null, $subject = null, $body = null) {
             //mail('nits.bikash@gmail.com','Helloooo','hiiiiiiii');
                $Email = new CakeEmail('smtp');
                $Email->emailFormat('both');
                $Email->from(array($from => 'LiveHelpout'));
                $Email->to($to);
                $Email->subject($subject);
                $s = $Email->send($body);
         }
         
         public function whytolike(){
             
             $array["whytolike"]=array(
                 '1'=>'I am working on a side project.',
                 '2'=>'I am bulding a startup.',
                 '3'=>'I am a freelance developer.',
                 '4'=>'I am a student.',
                 '5'=>'I would like to find a job as developer.',
                 '6'=>'I am working as a full time developer at a company.'
             );
             return $array;
             
         }
	
 public function whyskip(){
             
             $array["whyskip"]=array(
                 '1'=>'Not related to my expertise.',
                 '2'=>'Budget is too low.',
                 '3'=>'This is a consulting project.',
                 '4'=>'Subject is too advanced.',
                 '5'=>'This is a spam.',
                 '6'=>''
             );
             return $array;
             
         }

}
