<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class CommentsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
   
     public function addComment()
    {
     $comm['Comment']['comment']=$_REQUEST['comment'];
     $comm['Comment']['help_id']=$_REQUEST['help_id'];
     $comm['Comment']['userid']=$_REQUEST['userid'];
     $comm['Comment']['datetime']=date('Y-m-d H:i');
      $this->Comment->create();
      $this->Comment->save($comm);
      $last=$this->Comment->getLastInsertId();
      $this->loadModel('User');
      $optionsmentor = array('conditions' => array('User.id' => $_REQUEST['userid']));
      $user=$this->User->find('first', $optionsmentor);
      if($user['User']['image']!=''){
      echo json_encode(array('lastid'=>$last,'name'=>$user['User']['username'],'image'=>$user['User']['image'],'time'=>date('M d, Y H:i a')));
      }else{
      echo json_encode(array('lastid'=>$last,'name'=>$user['User']['username'],'image'=>'noimage.png','time'=>date('M d, Y H:i a')));    
      }
     
      exit;
    }
    
    public function deletecomment()
    {
    $this->Comment->delete($_REQUEST['commentid']);
     
      exit;
    }
    
   
    
    
    
}
