<?php
App::uses('AppController', 'Controller');
/**
 * Countries Controller
 *
 * @property Country $Country
 * @property PaginatorComponent $Paginator
 */
class CountriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	
	public function admin_index() {
		$user_id=$this->Session->read('userid');
		$keyword = '';
		if(!isset($user_id) && $user_id==NULL)
		{
			return $this->redirect(array('action' => 'index'));
		}
		$title_for_layout = 'Country List';
		$this->set(compact('title_for_layout'));
		
		if($this->request->is('post')){
			if(isset($this->request->data['Country']['name']) && $this->request->data['Country']['name']!="")
			{
				$keyword=$this->request->data['Country']['name'];
				//$this->Paginator->settings = array('conditions' => array('Country.name_en LIKE' => $keyword.'%'));
				$this->Paginator->settings = array('conditions' => array('Country.id !='=>0,'OR'=>array('Country.name_en LIKE' => $keyword.'%','Country.name_fr LIKE' => $keyword.'%','Country.name_ar LIKE' => $keyword.'%','Country.name_pr LIKE' => $keyword.'%')));
			}else{
				$this->Paginator->settings = array('conditions' => array('Country.id !=' => 0));
			}
		}
		
		$this->Country->recursive = 0;
		$this->set('countries', $this->Paginator->paginate());
		$this->set('keyword', $keyword);
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
		$this->set('country', $this->Country->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Country->create();
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash(__('The country has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The country could not be saved. Please, try again.'));
			}
		}
	}

	public function admin_edit($id = null) {
		if (!$this->Country->exists($id)) {
			throw new NotFoundException(__('Invalid country'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Country->save($this->request->data)) {
				$this->Session->setFlash(__('The country has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The country could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Country.' . $this->Country->primaryKey => $id));
			$this->request->data = $this->Country->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Country->id = $id;
		if (!$this->Country->exists()) {
			throw new NotFoundException(__('Invalid country'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Country->delete()) {
			$this->Session->setFlash(__('The country has been deleted.'));
		} else {
			$this->Session->setFlash(__('The country could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
