<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class EmailTemplatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');




	public function admin_index() {		
		$title_for_layout = 'EmailTemplate List';
		 $this->paginate = array(
			'order' => array(
				'EmailTemplate.id' => 'asc'
			)
		);
		$this->EmailTemplate->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('emailtemplate', $this->Paginator->paginate());
		$this->set(compact('title_for_layout'));
	}
	
	

	public function admin_edit($id = null) {
		//echo $id;
		//echo '&nbsp;&nbsp;';
		//echo $_SESSION['tmp_id']=$id;exit;
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid Email Template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EmailTemplate->save($this->request->data)) {
                              $this->Session->setFlash('The email template has been saved.', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash(__('The email template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
			$this->request->data = $this->EmailTemplate->find('first', $options);
		}
	}
	
	
	//public function admin_add() {	
		//$this->loadModel('EmailTemplate');
		//$title_for_layout = 'Email Templates Add';
		//$userid = $this->Session->read('userid');
		//if(!isset($userid) && $userid=='')
		//{
			//$this->redirect('/controlpanel');
		//}
		//if ($this->request->is('post')) {
			//echo '<pre>';print_r($this->request->data);exit;
                            //$options = array('conditions' => array('EmailTemplate.mail_for'  => $this->request->data['EmailTemplate']['mail_for'],'EmailTemplate.mail_from'  => $this->request->data['EmailTemplate']['mail_from'],'EmailTemplate.subject'  => $this->request->data['EmailTemplate']['subject'],'EmailTemplate.content'  => $this->request->data['EmailTemplate']['content']));
                            //$name = $this->EmailTemplate->find('all', $options);
                            //if(empty($name))
                            //{
                                //$this->request->data['Group']['is_group'] = implode(',',$this->request->data['Group']['is_group']);
                                //if ($this->EmailTemplate->save($this->request->data)) {
					//$this->Session->setFlash('The group has been saved.', 'default', array('class' => 'success'));
					//return $this->redirect(array('action' => 'index'));
				//} else {
					//$this->Session->setFlash(__('The group could not be saved. Please, try again.'));
				//}
                            //}else {
				//$this->Session->setFlash(__('The group name already exists. Please, try again.'));
                            //}
				

			//} /**/
		//}
			   //$user = $this->EmailTemplate->find('all');  
                           //echo'<pre>';print_r($user);exit;
			   //$this->set(compact('EmailTemplate'));                        

		//$this->set(compact('title_for_layout'));
	//}

}
