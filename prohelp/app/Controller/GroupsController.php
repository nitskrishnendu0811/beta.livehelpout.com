<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class GroupsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
   public $components = array('Paginator');
	public function admin_index() {		
		$title_for_layout = 'Groups List';
		 $this->paginate = array(
			'order' => array(
				'Group.id' => 'asc'
			)
		);
		$this->Group->recursive = 0;
		$this->Paginator->settings = $this->paginate;
		$this->set('groups', $this->Paginator->paginate());
		$this->set(compact('title_for_layout'));
	}

	public function admin_edit($id = null) {
	$this->loadModel('User');
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Invalid Email Template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Group']['is_group'] = implode(',',$this->request->data['Group']['is_group']);
                    if ($this->Group->save($this->request->data)) {
                              $this->Session->setFlash('The group has been saved.', 'default', array('class' => 'success'));
                              
			} else {
				$this->Session->setFlash(__('The group could not be saved. Please, try again.'));
			}
                        $this->redirect('/admin/groups/edit/'.$id);
		} else {
			$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
			
			$this->request->data = $this->Group->find('first', $options);
                        
                        $user = $this->User->find('all');  
                        $this->set(compact('user')); 
		}
                
	}
	
	
	public function admin_mailsend($id = null) {
		
	$this->loadModel('User');
	$this->loadModel('EmailTemplate');
	$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Invalid Email Template'));
		}
		$temp_id= '5652e5752540922d2fe465e9';
         $options1 = array('conditions' => array('EmailTemplate.id' => $temp_id));
         $templete = $this->EmailTemplate->find('first',$options1);
		if ($this->request->is(array('post', 'put'))) {
//pr($this->request->data);exit();	
			$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
			$grps = $this->Group->find('first', $options);		
			$group = explode(',',$grps['Group']['is_group']);
			foreach($group as $gr_id)
                        {
                        $group_id=$this->admin_user_find($gr_id);
                        //echo $group_id['User']['name'].'('.$group_id['User']['email'].')'.'<br>';
                        $mail_body =str_replace(array('[USER]'),array($group_id['User']['name']),$this->request->data['Group']['content']);
            				$this->send_mail($templete['EmailTemplate']['mail_from'],$group_id['User']['email'],$this->request->data['subject'],$mail_body);
                        }
                   
                       $this->redirect('/admin/groups/mailsend/'.$id);
		} else {
			$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
			$this->request->data = $this->Group->find('first', $options);
			
         $subject=$templete['EmailTemplate']['subject'];
         $content=$templete['EmailTemplate']['content'];
                        //exit;  
                        //$templete = $this->EmailTemplate->find('all'); 
                        $this->set(compact('user')); 
                        $this->set(compact('templete'));
                        
                        //$conditn = array();
    //if ( isset($this->params['group_id'])){
      //$conditn['User.id'] = $this->params['author_id'];
    //}
     //$users = $this->User->find('all', array('conditions'=>$conditn));
     
     //if (isset($this->params['requested']) && $this->params['requested'] == 1){
      //return $users;
    //}
    	
     //print_r($users);exit; 
		}
                
	}
	
 public function admin_user_find($id=null)
 {
 	//echo $id;exit;
 	$this->loadModel('User');
 	  $options = array('conditions' => array('User.id'  => $id));
     $users = $this->User->find('first', $options);
     
     //if (isset($this->params['requested']) && $this->params['requested'] == 1){
     return $users;
 }
	
public function admin_add() {	
		$this->loadModel('User');
		$title_for_layout = 'Groups Add';
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		if ($this->request->is('post')) {
			//echo '<pre>';print_r($this->request->data);exit;
                            $options = array('conditions' => array('Group.title'  => $this->request->data['Group']['title']));
                            $name = $this->Group->find('first', $options);
                            if(empty($name))
                            {
                                $this->request->data['Group']['is_group'] = implode(',',$this->request->data['Group']['is_group']);
                                if ($this->Group->save($this->request->data)) {
					$this->Session->setFlash('The group has been saved.', 'default', array('class' => 'success'));
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The group could not be saved. Please, try again.'));
				}
                            }else {
				$this->Session->setFlash(__('The group name already exists. Please, try again.'));
                            }
				

			} /**/
		//}
			   $user = $this->User->find('all');  
                           //echo'<pre>';print_r($user);exit;
			   $this->set(compact('user'));                        

		$this->set(compact('title_for_layout'));
	}
	
	
	public function admin_delete($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid group'));
		}
		
	
		if ($this->Group->delete($id)) {
			$this->Session->setFlash('The group has been deleted.', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The group could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	public function admin_view($id = null) {
			$userid = $this->Session->read('userid');
			if(!isset($userid) && $userid=='')
			{
				$this->redirect('/controlpanel');
			}
			$title_for_layout = 'Subscriber View';
			if (!$this->Group->exists($id)) {
				throw new NotFoundException(__('Invalid Subscriber'));
			}
			$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
			$category = $this->Group->find('first', $options);
			$this->set(compact('title_for_layout','category'));
		}

}


             