<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class HelpsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function posthelp() {
        $this->loadModel('RequestFile');
        $this->loadModel('Notification');
        $userid = $_REQUEST['userid'];
        $selectedcats = trim($_REQUEST['selectedcats'], ',');
        $allselectedcatscats = trim($_REQUEST['allselectedcatscats'], ',');
        $allselectedcatscatsids = trim($_REQUEST['allselectedcatscatsids'], ',');
        $allselectedcatsids = trim($_REQUEST['allselectedcatsids'], ',');
        $budget = $_REQUEST['budget'];
        $title = $_REQUEST['title'];
        $details = $_REQUEST['details'];
        $is_private = $_REQUEST['is_private'];
        $session_type = $_REQUEST['session_type'];
        $datetime = date('Y-m-d H:i:s');
        $editrequestid = $_REQUEST['editrequestid'];
        $uploadfiles = $_REQUEST['uploadfiles'];
        $status = 0;
        if ($editrequestid == 0) {
            $this->request->data['Help']['userid'] = $userid;
            $this->request->data['Help']['selectedmaincats'] = $allselectedcatscats;
            $this->request->data['Help']['selectedmaincatids'] = $allselectedcatscatsids;
            $this->request->data['Help']['selectedsubcatids'] = $allselectedcatsids;
            $this->request->data['Help']['selectedcats'] = $selectedcats;
            $this->request->data['Help']['budget'] = $budget;
            $this->request->data['Help']['title'] = $title;
            $this->request->data['Help']['details'] = $details;
            $this->request->data['Help']['is_private'] = $is_private;
            $this->request->data['Help']['session_type'] = $session_type;
            $this->request->data['Help']['datetime'] = $datetime;
            $this->request->data['Help']['status'] = $status;
            $this->Help->create();
            if ($this->Help->save($this->request->data)) {
                $requestid = $this->Help->getLastInsertId();
                
                $notification['Notification']['helpid'] = $requestid;
                $notification['Notification']['touserid'] = '';
                $notification['Notification']['fromuserid'] = $userid;
                $notification['Notification']['date'] = $datetime;
                $notification['Notification']['type'] = 'request';
                $notification['Notification']['is_read'] = 0;
                $notification['Notification']['parent_table'] = 'Help';
                $notification['Notification']['parent_table_id'] = $requestid;
                $this->Notification->create();
                $this->Notification->save($notification);
                
                if($uploadfiles==1)
                {
                }
                else
                {
                  foreach($uploadfiles as $file)
                  {
                    $requestfile['RequestFile']['helpid'] = $requestid;
                    $requestfile['RequestFile']['userid'] = $userid;
                    $requestfile['RequestFile']['file'] = $file;
                    $requestfile['RequestFile']['date'] = $datetime;
                    $requestfile['RequestFile']['likes'] = 0;
                    $this->RequestFile->create();
                    $this->RequestFile->save($requestfile);
                  }
                }
                echo json_encode(array("type" => 1, "msg" => 'Success'));
            } else {
                echo json_encode(array("type " => 0, "msg" => 'Error'));
            }
        } else {
            $this->request->data['Help']['id'] = $editrequestid;
            $this->request->data['Help']['userid'] = $userid;
            $this->request->data['Help']['selectedmaincats'] = $allselectedcatscats;
            $this->request->data['Help']['selectedmaincatids'] = $allselectedcatscatsids;
            $this->request->data['Help']['selectedsubcatids'] = $allselectedcatsids;
            $this->request->data['Help']['selectedcats'] = $selectedcats;
            $this->request->data['Help']['budget'] = $budget;
            $this->request->data['Help']['title'] = $title;
            $this->request->data['Help']['details'] = $details;
            $this->request->data['Help']['is_private'] = $is_private;
            $this->request->data['Help']['session_type'] = $session_type;
            $this->request->data['Help']['datetime'] = $datetime;
            $this->request->data['Help']['status'] = $status;
            if ($this->Help->save($this->request->data)) {
                $requestid = $editrequestid;
                
                $optionnoti = array('conditions' => array('Notification.helpid' => $requestid, 'Notification.fromuserid' => $userid, 'Notification.type' => 'request'));
                $noti = $this->Notification->find('first', $optionnoti);
                 if($noti)
                 {
                    $this->Notification->delete($noti['Notification']['id']);
                    $notification['Notification']['helpid'] = $requestid;
                    $notification['Notification']['touserid'] = '';
                    $notification['Notification']['fromuserid'] = $userid;
                    $notification['Notification']['date'] = $datetime;
                    $notification['Notification']['type'] = 'request';
                    $notification['Notification']['is_read'] = 0;
                    $notification['Notification']['parent_table'] = 'Help';
                    $notification['Notification']['parent_table_id'] = $requestid;
                    $this->Notification->create();
                    $this->Notification->save($notification);
                 }
                
                if($uploadfiles==1)
                {
                }
                else
                {
                  foreach($uploadfiles as $file)
                  {
                    $requestfile['RequestFile']['helpid'] = $requestid;
                    $requestfile['RequestFile']['userid'] = $userid;
                    $requestfile['RequestFile']['file'] = $file;
                    $requestfile['RequestFile']['date'] = $datetime;
                    $requestfile['RequestFile']['likes'] = 0;
                    $this->RequestFile->create();
                    $this->RequestFile->save($requestfile);
                  }
                }
                echo json_encode(array("type" => 1, "msg" => 'Success'));
            } else {
                echo json_encode(array("type " => 0, "msg" => 'Error'));
            }
        }
        exit;
    }
    
    public function likerequest(){
       $this->loadModel('Like');
       $this->loadModel('Notification');
       $userid = $_REQUEST['userid'];
       $requestid = $_REQUEST['requestid'];
       $datetime = date('Y-m-d H:i:s');
       if($userid == '' || $requestid=='')
       {
         echo json_encode(array("type " => 0, "msg" => 'Error'));
       }
       else
       {
         $options = array('conditions' => array('Like.userid' => $userid, 'Like.helpid' => $requestid));
         $likes = $this->Like->find('first', $options);
         if($likes)
         {
            $this->Like->delete($likes['Like']['id']);
            $optionall = array('conditions' => array('Like.helpid' => $requestid));
            $alllikes = $this->Like->find('all', $optionall);
            echo json_encode(array("type" => 1, "msg" => 'Success', 'is_like'=>0, 'likecount'=> count($alllikes)));
         }
         else
         {
            $likerequest['Like']['helpid'] = $requestid;
            $likerequest['Like']['userid'] = $userid;
            $likerequest['Like']['date'] = $datetime;
            $this->Like->create();
            $this->Like->save($likerequest);
            $likeid = $this->Like->getLastInsertId();
            
            $optionrequest = array('conditions' => array('Help.id' => $requestid));
            $help = $this->Help->find('first', $optionrequest);
            if($help['Help']['userid']!=$userid)
            {
               $notification['Notification']['helpid'] = $requestid;
               $notification['Notification']['touserid'] = $help['Help']['userid'];
               $notification['Notification']['fromuserid'] = $userid;
               $notification['Notification']['date'] = $datetime;
               $notification['Notification']['type'] = 'like';
               $notification['Notification']['is_read'] = 0;
               $notification['Notification']['parent_table'] = 'Like';
               $notification['Notification']['parent_table_id'] = $likeid;
               $this->Notification->create();
               $this->Notification->save($notification);
            }
            
            $optionall = array('conditions' => array('Like.helpid' => $requestid));
            $alllikes = $this->Like->find('all', $optionall);
            echo json_encode(array("type" => 1, "msg" => 'Success', 'is_like'=>1, 'likecount'=> count($alllikes)));
         }
       }
      exit; 
    }

    public function getallactivehelps() {
        $APP_SITE_URL = Configure::read('APP_SITE_URL');
        $this->loadModel('User');
        $this->loadModel('Like');
        $this->loadModel('RequestFile');
        $userid = $_REQUEST['userid'];
        $allrequests = array();
        $optionsuser = array('conditions' => array('User.id' => $userid));
        $user = $this->User->find('first', $optionsuser);
        $options = array('conditions' => array('Help.userid' => $userid, 'Help.status' => 0),'order'=>array('modified'=>-1));
        $helps = $this->Help->find('all', $options);
        if (!empty($helps)) {
            foreach ($helps as $v) {
                $allfiles = array();
                $imagefileexist=0;
                $otherfileexist=0;
                $optionsfiles = array('conditions' => array('RequestFile.userid' => $userid, 'RequestFile.helpid' => $v['Help']['id']),'order'=>array('modified'=>-1));
                $files = $this->RequestFile->find('all', $optionsfiles);
                if(!empty($files))
                {
                  $imageswithfullurl=array();
                  foreach ($files as $file) 
                  {
                    $path = $file['RequestFile']['file'];
                    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                    if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                    {
                      $filetype='image';
                      $imagefileexist=1;
                      $imageswithfullurl[]=array('original_url'=>$APP_SITE_URL."assets/frontend/uploads/requestimages/".$file['RequestFile']['file'],'name'=>$file['RequestFile']['file'],'help_id'=>$v['Help']['id']);
                    }
                    else
                    {
                      $filetype='pdf';
                      $otherfileexist=1;
                    }
                    $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
                  }
                }
                else
                {
                  $imageswithfullurl=array();
                }
                if(!empty($allfiles))
                {
                  $file_exist=1;
                }
                else
                {
                  $file_exist=0;
                }
                
                $options = array('conditions' => array('Like.userid' => $userid, 'Like.helpid' => $v['Help']['id']));
                $likes = $this->Like->find('first', $options);
                if($likes)
                {
                  $already_liked=1;
                }
                else
                {
                  $already_liked=0;
                }
                $optionall = array('conditions' => array('Like.helpid' => $v['Help']['id']));
                $alllikes = $this->Like->find('all', $optionall);
         
                $allrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', ' ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['datetime'])), 'usertimezone' => $user['User']['timezone'],'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist,'images'=>$imageswithfullurl,'already_liked'=>$already_liked,'count_like'=>count($alllikes));
            }
            echo json_encode(array("is_active_request_exist" => 1, 'allrequests' => $allrequests));
        } else {
            echo json_encode(array("is_active_request_exist" => 0, 'allrequests' => $allrequests));
        }
        exit;
    }

    public function getrequestdetails() {
        $this->loadModel('Category');
        $this->loadModel('RequestFile');
        $request_id = $_REQUEST['request_id'];
        $requestdetails = array();
        $selectedcatsarr = array();
        $selectedmaincatsarr = array();
        $options = array('conditions' => array('Help.id' => $request_id));
        $help = $this->Help->find('first', $options);
        if (!empty($help)) {
            $allfiles = array();
            $imagefileexist=0;
            $otherfileexist=0;
            $optionsfiles = array('conditions' => array('RequestFile.helpid' => $request_id),'order'=>array('modified'=>-1));
            $files = $this->RequestFile->find('all', $optionsfiles);
            if(!empty($files))
            {
               foreach ($files as $file) 
               {
                  $path = $file['RequestFile']['file'];
                  $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                  if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                  {
                    $filetype='image';
                    $imagefileexist=1;
                  }
                  else
                  {
                    $filetype='pdf';
                    $otherfileexist=1;
                  }
                  $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
               }
             }
             if(!empty($allfiles))
             {
               $file_exist=1;
             }
             else
             {
               $file_exist=0;
             }
            
            $selectedcats_exp = explode(',', $help['Help']['selectedcats']);
            foreach ($selectedcats_exp as $vc) {
                $optioncats = array('conditions' => array('Category.parent_id' => array('$ne' => 0), 'Category.name' => new MongoRegex("/" . $vc . "/i")));
                $subcategry = $this->Category->find('first', $optioncats);

                if ($subcategry) {
                    $selectedcatsarr[] = array('cat' => $vc, 'id' => $subcategry['Category']['id']);
                } else {
                    $selectedcatsarr[] = array('cat' => $vc, 'id' => 0);
                }
            }
            
            $selectedmaincats_exp = explode(',', $help['Help']['selectedmaincats']);
            foreach ($selectedmaincats_exp as $vc) {
                $optionmaincats = array('conditions' => array('Category.parent_id' => 0, 'Category.name' => new MongoRegex("/" . $vc . "/i")));
                $maincategry = $this->Category->find('first', $optionmaincats);

                if ($maincategry) {
                    $selectedmaincatsarr[] = array('cat' => $vc, 'id' => $maincategry['Category']['id']);
                } else {
                    $selectedmaincatsarr[] = array('cat' => $vc, 'id' => 0);
                }
            }
            
            $allrequests = array('id' => $help['Help']['id'], 'title' => $help['Help']['title'], 'details' => $help['Help']['details'], 'budget' => $help['Help']['budget'], 'selectedcats' => $help['Help']['selectedcats'], 'selectedmaincats' => $help['Help']['selectedmaincats'], 'userid' => $help['Help']['userid'], 'selectedmaincatsids' => $help['Help']['selectedmaincatids'], 'selectedcatsarr' => $selectedcatsarr, 'selectedmaincatsarr' => $selectedmaincatsarr, 'selectedsubcatsids' => $help['Help']['selectedsubcatids'],'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist);
            echo json_encode(array("is_active_request_exist" => 1, 'allrequests' => $allrequests));
        } else {
            echo json_encode(array("is_active_request_exist" => 0, 'allrequests' => $allrequests));
        }
        exit;
    }
    
    public function deleterequestFile() {
      $this->loadModel('RequestFile');
      $appsiteurl = Configure::read('APP_SITE_URL');
      $fileid = $_REQUEST['fileid'];
      $optionsfiles = array('conditions' => array('RequestFile.id' => $fileid));
      $files = $this->RequestFile->find('first', $optionsfiles);
      $filename=$files['RequestFile']['file'];
      if($files)
      {
        if($this->RequestFile->delete($fileid))
        {
          $ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
          if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
          {
          }
          else
          {
          }
          echo json_encode(array("type" => 'success'));
        }
        else
        {
          echo json_encode(array("type" => 'error'));
        }
      }
      else
      {
        echo json_encode(array("type" => 'error'));
      }
      exit;
    }

    public function getfinishedrequests() {
        $APP_SITE_URL = Configure::read('APP_SITE_URL');
        $this->loadModel('User');
        $this->loadModel('Like');
        $this->loadModel('RequestFile');
        $userid = $_REQUEST['userid'];
        $allrequests = array();
        $optionsuser = array('conditions' => array('User.id' => $userid));
        $user = $this->User->find('first', $optionsuser);
        $options = array('conditions' => array('Help.userid' => $userid, 'Help.status' => 1),'order'=>array('modified'=>-1));
        $helps = $this->Help->find('all', $options);
        if (!empty($helps)) {
            foreach ($helps as $v) {
                $allfiles = array();
                $imagefileexist=0;
                $otherfileexist=0;
                $optionsfiles = array('conditions' => array('RequestFile.userid' => $userid, 'RequestFile.helpid' => $v['Help']['id']),'order'=>array('modified'=>-1));
                $files = $this->RequestFile->find('all', $optionsfiles);
                if(!empty($files))
                {
                  $imageswithfullurl=array();
                  foreach ($files as $file) 
                  {
                    $path = $file['RequestFile']['file'];
                    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                    if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                    {
                      $filetype='image';
                      $imagefileexist=1;
                      $imageswithfullurl[]=array('original_url'=>$APP_SITE_URL."assets/frontend/uploads/requestimages/".$file['RequestFile']['file'],'name'=>$file['RequestFile']['file'],'help_id'=>$v['Help']['id']);
                    }
                    else
                    {
                      $filetype='pdf';
                      $otherfileexist=1;
                    }
                    $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
                  }
                }
                else
                {
                  $imageswithfullurl=array();
                }
                if(!empty($allfiles))
                {
                  $file_exist=1;
                }
                else
                {
                  $file_exist=0;
                }
                
                $optionall = array('conditions' => array('Like.helpid' => $v['Help']['id']));
                $alllikes = $this->Like->find('all', $optionall);
                
                $allrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', ' ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['completed_datetime'])), 'usertimezone' => $user['User']['timezone'],'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist,'images'=>$imageswithfullurl,'count_like'=>count($alllikes));
            }
            echo json_encode(array("is_finished_request_exist" => 1, 'allrequests' => $allrequests));
        } else {
            echo json_encode(array("is_finished_request_exist" => 0, 'allrequests' => $allrequests));
        }
        exit;
    }

    public function getcancelledrequests() {
        $APP_SITE_URL = Configure::read('APP_SITE_URL');
        $this->loadModel('User');
        $this->loadModel('RequestFile');
        $this->loadModel('Like');
        $userid = $_REQUEST['userid'];
        $allrequests = array();
        $optionsuser = array('conditions' => array('User.id' => $userid));
        $user = $this->User->find('first', $optionsuser);
        $options = array('conditions' => array('Help.userid' => $userid, 'Help.status' => 2),'order'=>array('modified'=>-1));
        $helps = $this->Help->find('all', $options);
        if (!empty($helps)) {
            foreach ($helps as $v) {
                $allfiles = array();
                $imagefileexist=0;
                $otherfileexist=0;
                $optionsfiles = array('conditions' => array('RequestFile.userid' => $userid, 'RequestFile.helpid' => $v['Help']['id']),'order'=>array('modified'=>-1));
                $files = $this->RequestFile->find('all', $optionsfiles);
                if(!empty($files))
                {
                  $imageswithfullurl=array();
                  foreach ($files as $file) 
                  {
                    $path = $file['RequestFile']['file'];
                    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                    if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                    {
                      $filetype='image';
                      $imagefileexist=1;
                      $imageswithfullurl[]=array('original_url'=>$APP_SITE_URL."assets/frontend/uploads/requestimages/".$file['RequestFile']['file'],'name'=>$file['RequestFile']['file'],'help_id'=>$v['Help']['id']);
                    }
                    else
                    {
                      $filetype='pdf';
                      $otherfileexist=1;
                    }
                    $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
                  }
                }
                else
                {
                  $imageswithfullurl=array();
                }
                if(!empty($allfiles))
                {
                  $file_exist=1;
                }
                else
                {
                  $file_exist=0;
                }
                
                $optionall = array('conditions' => array('Like.helpid' => $v['Help']['id']));
                $alllikes = $this->Like->find('all', $optionall);
                
                $allrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', ' ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['cancelled_datetime'])), 'usertimezone' => $user['User']['timezone'],'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist,'images'=>$imageswithfullurl,'count_like'=>count($alllikes));
            }
            echo json_encode(array("is_cancel_request_exist" => 1, 'allrequests' => $allrequests));
        } else {
            echo json_encode(array("is_cancel_request_exist" => 0, 'allrequests' => $allrequests));
        }
        exit;
    }

    public function getallpreviousrequests() {
        $this->loadModel('User');
        $userid = $_REQUEST['userid'];
        $allrequests = array();
        $optionsuser = array('conditions' => array('User.id' => $userid));
        $user = $this->User->find('first', $optionsuser);
        $options = array('conditions' => array('Help.userid' => $userid, 'Help.status' => array('$in' => array(0, 1))),'order'=>array('modified'=>-1));
        $helps = $this->Help->find('all', $options);
        if (!empty($helps)) {
            foreach ($helps as $v) {
                $allrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', ' ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['datetime'])), 'usertimezone' => $user['User']['timezone'], 'status' => $v['Help']['status']);
            }
            echo json_encode(array("is_previous_request_exist" => 1, 'allrequests' => $allrequests));
        } else {
            echo json_encode(array("is_previous_request_exist" => 0, 'allrequests' => $allrequests));
        }
        exit;
    }

    public function getnoofprevioushelps() {
        $this->loadModel('User');
        $userid = $_REQUEST['userid'];
        $noresult = array();
        $options = array('conditions' => array('Help.userid' => $userid, 'Help.status' => array('$ne' => 0)));
        $helps = $this->Help->find('all', $options);
        if (!empty($helps)) {
            echo json_encode(array('allrequests' => $helps));
        } else {
            echo json_encode(array('allrequests' => $noresult));
        }
        exit;
    }

    public function updaterequest() {
        $request_id = $_REQUEST['request_id'];
        $request_title = $_REQUEST['request_title'];
        $request_details = $_REQUEST['request_details'];
        $options = array('conditions' => array('Help.id' => $request_id));
        $help = $this->Help->find('first', $options);
        if ($help) {
            $this->request->data['Help']['id'] = $request_id;
            $this->request->data['Help']['title'] = $request_title;
            $this->request->data['Help']['details'] = $request_details;
            if ($this->Help->save($this->request->data)) {
                echo json_encode(array("type" => 1, "msg" => 'Your request details updated successfully.'));
            } else {
                echo json_encode(array("type " => 0, "msg" => 'Some error occurred. Please try again later.'));
            }
        } else {
            echo json_encode(array("type " => 0, "msg" => 'Request could not found.'));
        }
        exit;
    }

    public function mark_as_solved() {
        $request_id = $_REQUEST['request_id'];
        $options = array('conditions' => array('Help.id' => $request_id));
        $help = $this->Help->find('first', $options);
        if ($help) {
            $this->request->data['Help']['id'] = $request_id;
            $this->request->data['Help']['status'] = 1;
            $this->request->data['Help']['completed_datetime'] = date('Y-m-d H:i:s');
            if ($this->Help->save($this->request->data)) {
                echo json_encode(array("type" => 1, "msg" => 'Your request has been marked as completed.'));
            } else {
                echo json_encode(array("type " => 0, "msg" => 'Some error occurred. Please try again later.'));
            }
        } else {
            echo json_encode(array("type " => 0, "msg" => 'Request could not found.'));
        }
        exit;
    }

    public function mark_as_cancel() {
        $request_id = $_REQUEST['request_id'];
        $options = array('conditions' => array('Help.id' => $request_id));
        $help = $this->Help->find('first', $options);
        if ($help) {
            $this->request->data['Help']['id'] = $request_id;
            $this->request->data['Help']['status'] = 2;
            $this->request->data['Help']['cancelled_datetime'] = date('Y-m-d H:i:s');
            if ($this->Help->save($this->request->data)) {
                echo json_encode(array("type" => 1, "msg" => 'Your request has been marked as cancelled.'));
            } else {
                echo json_encode(array("type " => 0, "msg" => 'Some error occurred. Please try again later.'));
            }
        } else {
            echo json_encode(array("type " => 0, "msg" => 'Request could not found.'));
        }
        exit;
    }
    
    public function getnotifications()
    {
      $this->loadModel('Notification');
      $this->loadModel('User');
      $allnotifications=array();
      $notifications = $this->Notification->find('all', array('conditions' => array('Notification.fromuserid' => array('$ne' => $_REQUEST['userid']),'$or' => array(array("Notification.touserid" => ''),array("Notification.touserid" =>  $_REQUEST['userid']))),'order'=>array('modified'=>-1)));
      if(!empty($notifications))
      {
        foreach ($notifications as $val) 
        {
          $optionsuser = array('conditions' => array('User.id' => $val['Notification']['fromuserid']));
          $user = $this->User->find('first', $optionsuser);
          if($val['Notification']['type'] == 'request')
          {
             $optionsmentor = array('conditions' => array('User.id' =>  $_REQUEST['userid']));
             $mentor = $this->User->find('first', $optionsmentor);
             $mentor_expertise_exp = explode(',', $mentor['User']['expertise']);
             $help = $this->Help->find('first', array('conditions' => array('Help.id' => $val['Notification']['helpid'])));
             $isrelated=0;
             if (isset($mentor['User']['is_profile_completed']) && $mentor['User']['is_profile_completed'] == 1) 
             {
               foreach ($mentor_expertise_exp as $v)
               {
                 if (strpos($help['Help']['selectedcats'], $v) !== false) 
                 {
                   $isrelated=1;
                 }
               }
             }
             if($isrelated==1)
             {
                $allnotifications[]=array('id'=>$val['Notification']['id'],'requestid'=>$val['Notification']['helpid'],'type'=>$val['Notification']['type'],'is_read'=>$val['Notification']['is_read'],'date'=>date('M d, Y H:i a',strtotime($val['Notification']['date'])),'username'=>$user['User']['name'],'userimage'=>$user['User']['image'],'notificationmsg'=>$help['Help']['title']);
             }
          }
          else
          {
            $help = $this->Help->find('first', array('conditions' => array('Help.id' => $val['Notification']['helpid'])));
            $allnotifications[]=array('id'=>$val['Notification']['id'],'requestid'=>$val['Notification']['helpid'],'type'=>$val['Notification']['type'],'is_read'=>$val['Notification']['is_read'],'date'=>date('M d, Y H:i a',strtotime($val['Notification']['date'])),'username'=>$user['User']['name'],'userimage'=>$user['User']['image'],'notificationmsg'=>$help['Help']['title']);
          }
        }
        if(!empty($allnotifications))
        {
           echo json_encode(array("notification_exist" => 1, 'allnotifications' => $allnotifications));
        }
        else
        {
           echo json_encode(array("notification_exist" => 0, 'allnotifications' => $allnotifications));
        }
      }
      else
      {
        echo json_encode(array("notification_exist" => 0, 'allnotifications' => $allnotifications));
      }
      exit;
    }

    public function getopenrequests() {
        $APP_SITE_URL = Configure::read('APP_SITE_URL');
        $this->loadModel('User');
        $this->loadModel('Like');
        $this->loadModel('Comment');
        $this->loadModel('Interest');
        $this->loadModel('RequestFile');
        $userid = $_REQUEST['userid'];
         
        $optionsmentor = array('conditions' => array('User.id' => $userid));
        $mentor = $this->User->find('first', $optionsmentor);
        $relatedrequests = array();
        if (!empty($mentor)) {
         if (isset($mentor['User']['is_profile_completed']) && $mentor['User']['is_profile_completed'] == 1) {
                $mentor_expertise_exp = explode(',', $mentor['User']['expertise']);
                $helps = $this->Help->find('all', array('conditions' => array('Help.userid' => array('$ne' => $userid), 'Help.status' => 0),'order'=>array('modified'=>-1)));
                $idarray = array();
                foreach ($mentor_expertise_exp as $v) {
                    foreach ($helps as $val) {
                        if (strpos($val['Help']['selectedcats'], $v) !== false) {
                            if (!in_array($val['Help']['id'], $idarray)) {
                                $helparray1[] = $val;
                                $idarray[] = $val['Help']['id'];
                            }
                        }
                    }
                }

                $helparray = $this->Help->find('all', array(
                    'conditions' => array(
                        'Help.userid' => array('$ne' => $userid),
                        'Help.id' => array('$in' => $idarray),
                        'Help.status' => 0
                    ),'order'=>array('modified'=>-1)
                ));
          }
          if (!empty($helparray)) {
                foreach ($helparray as $v) {
                    $intoption = array('conditions' => array('Interest.userid' => $userid, 'Interest.help_id' => $v['Help']['id']));
                    $findint = $this->Interest->find('first', $intoption);
                    if (empty($findint) || $findint['Interest']['is_skipped'] != 1) {
                    
                        if (($mentor['User']['price'] * 4) > $v['Help']['budget']) {
                            $msg = 'Budget of ' . number_format($v['Help']['budget'], 2) . '/hr is lower than your rate.';
                        } else {
                            $msg = '';
                        }

                        if (!empty($findint)) {
                            $interest = $findint['Interest']['is_interest'];
                        } else {
                            $interest = 0;
                        }

                        $optionsuser = array('conditions' => array('User.id' => $v['Help']['userid']));
                        $user = $this->User->find('first', $optionsuser);

                        $optioncomments = array('conditions' => array('Comment.help_id' => $v['Help']['id']));
                        $comments = $this->Comment->find('all', $optioncomments);

                        if (!empty($comments)) {
                            foreach ($comments as $comment) {
                                $userdetails = $this->User->find('first', array('conditions' => array('User.id' => $comment['Comment']['userid'])));
                                $commentarray[] = array('id' => $comment['Comment']['id'],
                                    'comment' => $comment['Comment']['comment'],
                                    'time' => date('M d, Y H:i a',strtotime($comment['Comment']['datetime'])),
                                    'image' => $userdetails['User']['image'],
                                    'name' => $userdetails['User']['username'],
                                    'userid' => $userdetails['User']['id']
                                );
                            }
                        } else {
                            $commentarray = array();
                        }
                        
                        $allfiles = array();
                        $imagefileexist=0;
                        $otherfileexist=0;
                        $optionsfiles = array('conditions' => array('RequestFile.helpid' => $v['Help']['id']),'order'=>array('modified'=>-1));
                        $files = $this->RequestFile->find('all', $optionsfiles);
                        if(!empty($files))
                        {
                          $imageswithfullurl=array();
                          foreach ($files as $file) 
                          {
                            $path = $file['RequestFile']['file'];
                            $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                            if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                            {
                              $filetype='image';
                              $imagefileexist=1;
                              $imageswithfullurl[]=array('original_url'=>$APP_SITE_URL."assets/frontend/uploads/requestimages/".$file['RequestFile']['file'],'name'=>$file['RequestFile']['file'],'help_id'=>$v['Help']['id']);
                            }
                            else
                            {
                              $filetype='pdf';
                              $otherfileexist=1;
                            }
                            $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
                          }
                        }
                        else
                        {
                          $imageswithfullurl=array();
                        }
                        if(!empty($allfiles))
                        {
                          $file_exist=1;
                        }
                        else
                        {
                          $file_exist=0;
                        }
                        
                        $cat_arr=array();
                        $subcat_arr=array();
                        if(isset($v['Help']['selectedmaincats']) && $v['Help']['selectedmaincats']!='')
                        {
                            $selectedmaincats_exp=explode(',',$v['Help']['selectedmaincats']);
                            foreach($selectedmaincats_exp as $exv)
                            {
                              $cat_arr[]=array('tag'=>$exv);
                            }
                        }
                        if(isset($v['Help']['selectedcats']) && $v['Help']['selectedcats']!='')
                        {
                            $selectedcats_exp=explode(',',$v['Help']['selectedcats']);
                            foreach($selectedcats_exp as $exv)
                            {
                              $subcat_arr[]=array('tag'=>$exv);
                            }
                        }
                        
                        $options = array('conditions' => array('Like.userid' => $userid, 'Like.helpid' => $v['Help']['id']));
                        $likes = $this->Like->find('first', $options);
                        if($likes)
                        {
                          $already_liked=1;
                        }
                        else
                        {
                          $already_liked=0;
                        }
                        $optionall = array('conditions' => array('Like.helpid' => $v['Help']['id']));
                        $alllikes = $this->Like->find('all', $optionall);

                        $relatedrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', '  ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['datetime'])), 'usertimezone' => $user['User']['timezone'], 'showbudgetmessage' => $msg, 'interest' => $interest, 'comments' => $commentarray,'categories'=>$cat_arr,'subcategories'=>$subcat_arr,'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist,'images'=>$imageswithfullurl,'already_liked'=>$already_liked,'count_like'=>count($alllikes));
                        
                    }
                }

                echo json_encode(array("related_request_exist" => 1, 'relatedrequests' => $relatedrequests, 'total' => count($relatedrequests)));
            } else {
                echo json_encode(array("related_request_exist" => 0, 'relatedrequests' => $relatedrequests));
            }
            exit;
          }  
        /*if (!empty($mentor)) {
            if (isset($mentor['User']['is_profile_completed']) && $mentor['User']['is_profile_completed'] == 1) {
                $mentor_expertise_exp = explode(',', $mentor['User']['expertise']);
                $helps = $this->Help->find('all', array('conditions' => array('Help.userid' => array('$ne' => $userid), 'Help.status' => 0),'order'=>array('modified'=>-1)));
                $idarray = array();
                foreach ($mentor_expertise_exp as $v) {
                    foreach ($helps as $val) {
                        $val['is_interest']=0;
                        $val['comments']=array();
                        $intoption = array('conditions' => array('Interest.userid' => $userid, 'Interest.help_id' => $val['Help']['id']));
                        $findint = $this->Interest->find('first', $intoption);
                        if (empty($findint) || $findint['Interest']['is_skipped'] != 1) {
                            if (!empty($findint)) {
                                $interest = $findint['Interest']['is_interest'];
                            } else {
                                $interest = 0;
                            }
                            if (strpos($val['Help']['selectedcats'], $v) !== false) {
                                if (!in_array($val['Help']['id'], $idarray)) {
                                    if (($mentor['User']['price'] * 4) > $val['Help']['budget']) {
                                        $val['showbudgetmessage'] = 'Budget of ' . number_format($val['Help']['budget'], 2) . '/hr is lower than your rate.';
                                    } else {
                                        $val['showbudgetmessage'] = '';
                                    }
                                    $val['is_interest'] = $interest;
                                    $optioncomments = array('conditions' => array('Comment.help_id' => $val['Help']['id']));
                                    $comments = $this->Comment->find('all', $optioncomments);
                                    
                                    if (!empty($comments)) {
                                        foreach ($comments as $comment) {
                                            $userdetails = $this->User->find('first', array('conditions' => array('User.id' => $comment['Comment']['userid'])));
                                            $commentarray[] = array('id' => $comment['Comment']['id'],
                                                'comment' => $comment['Comment']['comment'],
                                                'time' => date('M d, Y H:i a',strtotime($comment['Comment']['datetime'])),
                                                'image' => $userdetails['User']['image'],
                                                'name' => $userdetails['User']['username'],
                                                'userid' => $userdetails['User']['id']
                                            );
                                        }
                                    } else {
                                        $commentarray = array();
                                    }
                                    $val['comments'] = $commentarray;
                                    $helparray[] = $val;
                                    $idarray[] = $val['Help']['id'];
                                }
                            }
                        }
                    }
                }
            } else {
                
            }
        }*/
        /*if (!empty($helparray)) {
            foreach ($helparray as $v) {
                $optionsuser = array('conditions' => array('User.id' => $v['Help']['userid']));
                $user = $this->User->find('first', $optionsuser);
                $cat_arr=array();
                $subcat_arr=array();
                
                $allfiles = array();
                $imagefileexist=0;
                $otherfileexist=0;
                $optionsfiles = array('conditions' => array('RequestFile.helpid' => $v['Help']['id']),'order'=>array('modified'=>-1));
                $files = $this->RequestFile->find('all', $optionsfiles);
                if(!empty($files))
                {
                  $imageswithfullurl=array();
                  foreach ($files as $file) 
                  {
                    $path = $file['RequestFile']['file'];
                    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                    if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                    {
                      $filetype='image';
                      $imagefileexist=1;
                      $imageswithfullurl[]=array('original_url'=>$APP_SITE_URL."assets/frontend/uploads/requestimages/".$file['RequestFile']['file'],'name'=>$file['RequestFile']['file'],'help_id'=>$v['Help']['id']);
                    }
                    else
                    {
                      $filetype='pdf';
                      $otherfileexist=1;
                    }
                    $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
                  }
                }
                else
                {
                  $imageswithfullurl=array();
                }
                if(!empty($allfiles))
                {
                  $file_exist=1;
                }
                else
                {
                  $file_exist=0;
                }
                
                if(isset($v['Help']['selectedmaincats']) && $v['Help']['selectedmaincats']!='')
                {
                    $selectedmaincats_exp=explode(',',$v['Help']['selectedmaincats']);
                    foreach($selectedmaincats_exp as $exv)
                    {
                      $cat_arr[]=array('tag'=>$exv);
                    }
                }
                if(isset($v['Help']['selectedcats']) && $v['Help']['selectedcats']!='')
                {
                    $selectedcats_exp=explode(',',$v['Help']['selectedcats']);
                    foreach($selectedcats_exp as $exv)
                    {
                      $subcat_arr[]=array('tag'=>$exv);
                    }
                }
                
                $options = array('conditions' => array('Like.userid' => $userid, 'Like.helpid' => $v['Help']['id']));
                $likes = $this->Like->find('first', $options);
                if($likes)
                {
                  $already_liked=1;
                }
                else
                {
                  $already_liked=0;
                }
                $optionall = array('conditions' => array('Like.helpid' => $v['Help']['id']));
                $alllikes = $this->Like->find('all', $optionall);
                  
                $relatedrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', '  ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['datetime'])), 'usertimezone' => $user['User']['timezone'], 'showbudgetmessage' => $v['showbudgetmessage'], 'interest' => $val['is_interest'], 'comments' => $v['comments'],'categories'=>$cat_arr,'subcategories'=>$subcat_arr,'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist,'images'=>$imageswithfullurl,'already_liked'=>$already_liked,'count_like'=>count($alllikes));
            }

            echo json_encode(array("related_request_exist" => 1, 'relatedrequests' => $relatedrequests, 'total' => count($relatedrequests)));
        } else {
            echo json_encode(array("related_request_exist" => 0, 'relatedrequests' => $relatedrequests));
        }
        exit;
        */
    }

    public function allotherrequests() {
        $APP_SITE_URL = Configure::read('APP_SITE_URL');
        $this->loadModel('Comment');
        $this->loadModel('User');
        $this->loadModel('Like');
        $this->loadModel('Interest');
        $this->loadModel('RequestFile');
        $userid = $_REQUEST['userid'];
        $optionsmentor = array('conditions' => array('User.id' => $userid));
        $mentor = $this->User->find('first', $optionsmentor);
        $relatedrequests = array();
        if (!empty($mentor)) {
            if (isset($mentor['User']['is_profile_completed']) && $mentor['User']['is_profile_completed'] == 1) {
                $mentor_expertise_exp = explode(',', $mentor['User']['expertise']);
                $helps = $this->Help->find('all', array('conditions' => array('Help.userid' => array('$ne' => $userid), 'Help.status' => 0),'order'=>array('modified'=>-1)));
                $idarray = array();
                foreach ($mentor_expertise_exp as $v) {
                    foreach ($helps as $val) {
                        if (strpos($val['Help']['selectedcats'], $v) !== false) {
                            if (!in_array($val['Help']['id'], $idarray)) {
                                $helparray1[] = $val;
                                $idarray[] = $val['Help']['id'];
                            }
                        }
                    }
                }

                $helparray = $this->Help->find('all', array(
                    'conditions' => array(
                        'Help.userid' => array('$ne' => $userid),
                        'Help.id' => array('$nin' => $idarray),
                        'Help.status' => 0
                    ),'order'=>array('modified'=>-1)
                ));
            }
            if (!empty($helparray)) {
                foreach ($helparray as $v) {
                    $intoption = array('conditions' => array('Interest.userid' => $userid, 'Interest.help_id' => $v['Help']['id']));
                    $findint = $this->Interest->find('first', $intoption);
                    if (empty($findint) || $findint['Interest']['is_skipped'] != 1) {
                    
                        if (($mentor['User']['price'] * 4) > $v['Help']['budget']) {
                            $msg = 'Budget of ' . number_format($v['Help']['budget'], 2) . '/hr is lower than your rate.';
                        } else {
                            $msg = '';
                        }

                        if (!empty($findint)) {
                            $interest = $findint['Interest']['is_interest'];
                        } else {
                            $interest = 0;
                        }

                        $optionsuser = array('conditions' => array('User.id' => $v['Help']['userid']));
                        $user = $this->User->find('first', $optionsuser);

                        $optioncomments = array('conditions' => array('Comment.help_id' => $v['Help']['id']));
                        $comments = $this->Comment->find('all', $optioncomments);

                        if (!empty($comments)) {
                            foreach ($comments as $comment) {
                                $userdetails = $this->User->find('first', array('conditions' => array('User.id' => $comment['Comment']['userid'])));
                                $commentarray[] = array('id' => $comment['Comment']['id'],
                                    'comment' => $comment['Comment']['comment'],
                                    'time' => date('M d, Y H:i a',strtotime($comment['Comment']['datetime'])),
                                    'image' => $userdetails['User']['image'],
                                    'name' => $userdetails['User']['username'],
                                    'userid' => $userdetails['User']['id']
                                );
                            }
                        } else {
                            $commentarray = array();
                        }
                        
                        $allfiles = array();
                        $imagefileexist=0;
                        $otherfileexist=0;
                        $optionsfiles = array('conditions' => array('RequestFile.helpid' => $v['Help']['id']),'order'=>array('modified'=>-1));
                        $files = $this->RequestFile->find('all', $optionsfiles);
                        if(!empty($files))
                        {
                          $imageswithfullurl=array();
                          foreach ($files as $file) 
                          {
                            $path = $file['RequestFile']['file'];
                            $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                            if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                            {
                              $filetype='image';
                              $imagefileexist=1;
                              $imageswithfullurl[]=array('original_url'=>$APP_SITE_URL."assets/frontend/uploads/requestimages/".$file['RequestFile']['file'],'name'=>$file['RequestFile']['file'],'help_id'=>$v['Help']['id']);
                            }
                            else
                            {
                              $filetype='pdf';
                              $otherfileexist=1;
                            }
                            $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
                          }
                        }
                        else
                        {
                          $imageswithfullurl=array();
                        }
                        if(!empty($allfiles))
                        {
                          $file_exist=1;
                        }
                        else
                        {
                          $file_exist=0;
                        }
                        
                        $cat_arr=array();
                        $subcat_arr=array();
                        if(isset($v['Help']['selectedmaincats']) && $v['Help']['selectedmaincats']!='')
                        {
                            $selectedmaincats_exp=explode(',',$v['Help']['selectedmaincats']);
                            foreach($selectedmaincats_exp as $exv)
                            {
                              $cat_arr[]=array('tag'=>$exv);
                            }
                        }
                        if(isset($v['Help']['selectedcats']) && $v['Help']['selectedcats']!='')
                        {
                            $selectedcats_exp=explode(',',$v['Help']['selectedcats']);
                            foreach($selectedcats_exp as $exv)
                            {
                              $subcat_arr[]=array('tag'=>$exv);
                            }
                        }
                        
                        $options = array('conditions' => array('Like.userid' => $userid, 'Like.helpid' => $v['Help']['id']));
                        $likes = $this->Like->find('first', $options);
                        if($likes)
                        {
                          $already_liked=1;
                        }
                        else
                        {
                          $already_liked=0;
                        }
                        $optionall = array('conditions' => array('Like.helpid' => $v['Help']['id']));
                        $alllikes = $this->Like->find('all', $optionall);

                        $relatedrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', '  ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['datetime'])), 'usertimezone' => $user['User']['timezone'], 'showbudgetmessage' => $msg, 'interest' => $interest, 'comments' => $commentarray,'categories'=>$cat_arr,'subcategories'=>$subcat_arr,'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist,'images'=>$imageswithfullurl,'already_liked'=>$already_liked,'count_like'=>count($alllikes));
                        
                    }
                }

                echo json_encode(array("related_request_exist" => 1, 'relatedrequests' => $relatedrequests, 'total' => count($relatedrequests)));
            } else {
                echo json_encode(array("related_request_exist" => 0, 'relatedrequests' => $relatedrequests));
            }
            exit;
        }
    }
    
    public function getfilterresults() {
        $APP_SITE_URL = Configure::read('APP_SITE_URL');
        $this->loadModel('User');
        $this->loadModel('Like');
        $this->loadModel('Category');
        $this->loadModel('RequestFile');
        $userid = $_REQUEST['userid'];
        $main_cats=$_REQUEST['main_cats'];
        $main_cats_exp=explode(',',trim($_REQUEST['main_cats'],','));
        $sub_cats=$_REQUEST['sub_cats'];
        $sub_cats_exp=explode(',',trim($_REQUEST['sub_cats'],','));
        $relatedrequests=array();
        $helparray=array();
        $idarray=array();
        $sortedarray=array();
        
        if(!empty($sub_cats_exp)){
          $subcatarrays = $this->Category->find('all', array(
                    'conditions' => array(
                        'Category.id' => array('$in' => $sub_cats_exp)
                    )
          ));
          foreach($subcatarrays as $subcats){
                if(in_array($subcats['Category']['parent_id'],$main_cats_exp)){
                  $searchkey=array_search($subcats['Category']['parent_id'],$main_cats_exp);
                  unset($main_cats_exp[$searchkey]);
                }
                $sortedarray[]=$subcats['Category']['name'];
          }
        }
       
        if(!empty($main_cats_exp))
        {
          $arr=array_values($main_cats_exp);
          $maincatarrays = $this->Category->find('all', array(
            'conditions' => array(
                'Category.id' => array('$in' => $arr)
            )
          ));
         foreach($maincatarrays as $maincatarray){
           $sortedarray[]=$maincatarray['Category']['name'];
         }  
       }
      
       $helps = $this->Help->find('all', array('conditions' => array('Help.userid' => array('$ne' => $userid), 'Help.status' => 0),'order'=>array('modified'=>-1)));
        
       if(!empty($helps))
       {
           if(!empty($sortedarray))
           {
             foreach(array_unique($sortedarray) as $vc)
             {
             
                foreach ($helps as $val) 
                {
                if (strpos(strtolower($val['Help']['selectedmaincats']), strtolower($vc)) !== false || strpos(strtolower($val['Help']['selectedcats']), strtolower($vc)) !== false) {
                   $helparray[] = $val;
                   $idarray[] = $val['Help']['id'];
                   }
                }
             }
           
           }

           if (!empty($helparray)) {
            foreach ($helparray as $v) {
                $optionsuser = array('conditions' => array('User.id' => $v['Help']['userid']));
                $user = $this->User->find('first', $optionsuser);
                $cat_arr=array();
                $subcat_arr=array();
                $allfiles = array();
                $imagefileexist=0;
                $otherfileexist=0;
                $optionsfiles = array('conditions' => array('RequestFile.helpid' => $v['Help']['id']),'order'=>array('modified'=>-1));
                $files = $this->RequestFile->find('all', $optionsfiles);
                if(!empty($files))
                {
                  $imageswithfullurl=array();
                  foreach ($files as $file) 
                  {
                    $path = $file['RequestFile']['file'];
                    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                    if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                    {
                      $filetype='image';
                      $imagefileexist=1;
                      $imageswithfullurl[]=array('original_url'=>$APP_SITE_URL."assets/frontend/uploads/requestimages/".$file['RequestFile']['file'],'name'=>$file['RequestFile']['file'],'help_id'=>$v['Help']['id']);
                    }
                    else
                    {
                      $filetype='pdf';
                      $otherfileexist=1;
                    }
                    $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
                  }
                }
                else
                {
                  $imageswithfullurl=array();
                }
                if(!empty($allfiles))
                {
                  $file_exist=1;
                }
                else
                {
                  $file_exist=0;
                }
                
                if(isset($v['Help']['selectedmaincats']) && $v['Help']['selectedmaincats']!='')
                {
                    $selectedmaincats_exp=explode(',',$v['Help']['selectedmaincats']);
                    foreach($selectedmaincats_exp as $exv)
                    {
                      $cat_arr[]=array('tag'=>$exv);
                    }
                }
                if(isset($v['Help']['selectedcats']) && $v['Help']['selectedcats']!='')
                {
                    $selectedcats_exp=explode(',',$v['Help']['selectedcats']);
                    foreach($selectedcats_exp as $exv)
                    {
                      $subcat_arr[]=array('tag'=>$exv);
                    }
                }
                
                $options = array('conditions' => array('Like.userid' => $userid, 'Like.helpid' => $v['Help']['id']));
                $likes = $this->Like->find('first', $options);
                if($likes)
                {
                  $already_liked=1;
                }
                else
                {
                  $already_liked=0;
                }
                $optionall = array('conditions' => array('Like.helpid' => $v['Help']['id']));
                $alllikes = $this->Like->find('all', $optionall);
                
                $relatedrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', '  ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['datetime'])), 'usertimezone' => $user['User']['timezone'], 'categories'=>$cat_arr,'subcategories'=>$subcat_arr,'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist,'images'=>$imageswithfullurl,'already_liked'=>$already_liked,'count_like'=>count($alllikes));
             }
             echo json_encode(array("filter_result_exist" => 1, 'relatedrequests' => $relatedrequests));
         } else {
             echo json_encode(array("filter_result_exist" => 0, 'relatedrequests' => $relatedrequests));
         }
        }
        else
        {
          echo json_encode(array("filter_result_exist" => 0, 'relatedrequests' => $relatedrequests));
        }
        exit;     
    }
    
    public function getrequestimages()
    {
       $this->loadModel('RequestFile');
       $allfiles = array();
       $optionsfiles = array('conditions' => array('RequestFile.helpid' => $_REQUEST['helpid']),'order'=>array('modified'=>-1));
       $files = $this->RequestFile->find('all', $optionsfiles);
       if(!empty($files))
       {
          foreach ($files as $file) 
          {
            $path = $file['RequestFile']['file'];
            $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
            if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
            {
              $allfiles[]=$file['RequestFile']['file'];
            }
            else
            {
              
            }
          }
        }
        if(!empty($allfiles))
        {
          $file_count=count($allfiles);
        }
        else
        {
          $file_count=0;
        }
        echo json_encode(array("file_count" =>$file_count, 'allfiles' => $allfiles));
        exit;
    }
    
    public function searchresultrequests() {
	$APP_SITE_URL = Configure::read('APP_SITE_URL');
        $this->loadModel('Like');
        $this->loadModel('User');
        $keyword=$_REQUEST['keyword'];
        $this->loadModel('Interest');
        $this->loadModel('RequestFile');
        $relatedrequests=array();
        $helps = $this->Help->find('all', array('conditions' => array('Help.status' => 0),'order'=>array('modified'=>-1)));
        $idarray = array();
        foreach ($helps as $val) {
                if($keyword!=''){
                    $keywordexp=explode('+',$_REQUEST['keyword']);
                    foreach($keywordexp as $ev)
                    {
                       if (strpos(strtolower($val['Help']['selectedmaincats']), strtolower($ev)) !== false || strpos(strtolower($val['Help']['selectedcats']), strtolower($ev)) !== false) {
                        if (!in_array($val['Help']['id'], $idarray)) {
                            
                            $helparray[] = $val;
                            $idarray[] = $val['Help']['id'];
                         }
                       }
                    }
                }else{
                    $helparray[] = $val;
                    $idarray[] = $val['Help']['id'];
                }
          }
          if (!empty($helparray)) {
            foreach ($helparray as $v) {
                
                if(isset($_REQUEST['userid']) && $_REQUEST['userid']!=''){
                $userid=$_REQUEST['userid'];
                $options = array('conditions' => array('Like.userid' => $userid,'Like.helpid' => $v['Help']['id']));
                $likes = $this->Like->find('first', $options);
                if($likes)
                {
                  $already_liked=1;
                }
                else
                {
                  $already_liked=0;
                }
                }else{
                     $already_liked=0;
                }
                $optionall = array('conditions' => array('Like.helpid' => $v['Help']['id']));
                $alllikes = $this->Like->find('all', $optionall);
                
                
                $optionsuser = array('conditions' => array('User.id' => $v['Help']['userid']));
                $user = $this->User->find('first', $optionsuser);
                $cat_arr=array();
                $subcat_arr=array();
                
                $allfiles = array();
                $imagefileexist=0;
                $otherfileexist=0;
                $optionsfiles = array('conditions' => array('RequestFile.helpid' => $v['Help']['id']),'order'=>array('modified'=>-1));
                $files = $this->RequestFile->find('all', $optionsfiles);
                if(!empty($files))
                {
		  $imageswithfullurl=array();
                  foreach ($files as $file) 
                  {
                    $path = $file['RequestFile']['file'];
                    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
                    if($ext=='png' || $ext=='jpg' || $ext=='jpeg' || $ext=='gif')
                    {
                      $filetype='image';
                      $imagefileexist=1;
		      $imageswithfullurl[]=array('original_url'=>$APP_SITE_URL."assets/frontend/uploads/requestimages/".$file['RequestFile']['file'],'name'=>$file['RequestFile']['file'],'help_id'=>$v['Help']['id']);
                    }
                    else
                    {
                      $filetype='pdf';
                      $otherfileexist=1;
                    }
                    $allfiles[]=array('id'=>$file['RequestFile']['id'],'likes'=>$file['RequestFile']['likes'],'file'=>$file['RequestFile']['file'],'filetype'=>$filetype);
                  }
                }
                else
                {
                  $imageswithfullurl=array();
                }
                if(!empty($allfiles))
                {
                  $file_exist=1;
                }
                else
                {
                  $file_exist=0;
                }
                
                if(isset($v['Help']['selectedmaincats']) && $v['Help']['selectedmaincats']!='')
                {
                    $selectedmaincats_exp=explode(',',$v['Help']['selectedmaincats']);
                    foreach($selectedmaincats_exp as $exv)
                    {
                      $cat_arr[]=array('tag'=>$exv);
                    }
                }
                if(isset($v['Help']['selectedcats']) && $v['Help']['selectedcats']!='')
                {
                    $selectedcats_exp=explode(',',$v['Help']['selectedcats']);
                    foreach($selectedcats_exp as $exv)
                    {
                      $subcat_arr[]=array('tag'=>$exv);
                    }
                }
                
                $relatedrequests[] = array('id' => $v['Help']['id'], 'title' => $v['Help']['title'], 'details' => $v['Help']['details'], 'budget' => $v['Help']['budget'], 'selectedcats' => str_replace(',', '  ', $v['Help']['selectedcats']), 'selectedmaincats' => str_replace(',', ', ', $v['Help']['selectedmaincats']), 'userid' => $v['Help']['userid'], 'username' => $user['User']['name'], 'userimage' => $user['User']['image'], 'posttime' => date('M d, Y H:i a', strtotime($v['Help']['datetime'])), 'usertimezone' => $user['User']['timezone'], 'categories'=>$cat_arr,'subcategories'=>$subcat_arr,'files'=>$allfiles,'file_exist'=>$file_exist,'imagefileexist'=>$imagefileexist,'otherfileexist'=>$otherfileexist,'images'=>$imageswithfullurl,'already_liked'=>$already_liked,'count_like'=>count($alllikes));
            }

            echo json_encode(array("related_request_exist" => 1, 'relatedrequests' => $relatedrequests, 'total' => count($relatedrequests)));
        } else {
            echo json_encode(array("related_request_exist" => 0, 'relatedrequests' => $relatedrequests));
        }
        exit;
    }
    
    public function searchresultmentors() {
        $this->loadModel('User');
        $keyword=$_REQUEST['keyword'];
        $this->loadModel('Interest');
        $optionsmentor = array('conditions' => array('User.role' => 2,'User.is_profile_completed'=>1,'User.is_active'=>1));
        $mentors = $this->User->find('all', $optionsmentor);
        $mentorval=array();
        if(!empty($mentors)){
            foreach($mentors as $mentor){
                $cat_arr=array();
                if($mentor['User']['is_logged_in']==1)
                {
                    if($mentor['User']['login_status']==1)
                    {
                      $login_status=1;
                    }
                    else if($mentor['User']['login_status']==2)
                    {
                      $login_status=2;
                    }
                    else
                    {
                      $login_status=0;
                    }
                }
                else
                {
                   $login_status=0;
                }
              if($keyword!=''){
                if(strpos(strtolower($mentor['User']['expertise']), strtolower($keyword)) !== false)
                {
                   if(isset($mentor['User']['expertise']) && $mentor['User']['expertise']!='')
                   {
                    $selectedmaincats_exp=explode(',',$mentor['User']['expertise']);
                    foreach($selectedmaincats_exp as $exv)
                    {
                      $cat_arr[]=array('tag'=>$exv);
                    }
                   }
                   $mentorval[]=array('userid' => $mentor['User']['id'], 'username' => $mentor['User']['name'], 'userimage' => $mentor['User']['image'],'expertise'=>$mentor['User']['expertise'],'headline'=>$mentor['User']['headline'],'about'=>$mentor['User']['about'],'price'=>$mentor['User']['price'],'login_status'=>$login_status,'is_logged_in'=>$mentor['User']['is_logged_in'],'expertize'=>$cat_arr);
                }
              }else
              {
                  if(isset($mentor['User']['expertise']) && $mentor['User']['expertise']!='')
                  {
                    $selectedmaincats_exp=explode(',',$mentor['User']['expertise']);
                    foreach($selectedmaincats_exp as $exv)
                    {
                      $cat_arr[]=array('tag'=>$exv);
                    }
                  }
                  $mentorval[]=array('userid' => $mentor['User']['id'], 'username' => $mentor['User']['name'], 'userimage' => $mentor['User']['image'],'expertise'=>$mentor['User']['expertise'],'headline'=>$mentor['User']['headline'],'about'=>$mentor['User']['about'],'price'=>$mentor['User']['price'],'login_status'=>$login_status,'is_logged_in'=>$mentor['User']['is_logged_in'],'expertize'=>$cat_arr);  
              }
            }
           }
           if(!empty($mentorval)){
              echo json_encode(array("ismentorexist" => 1, 'mentors' => $mentorval, 'total' => count($mentorval)));  
           }else{
              echo json_encode(array("ismentorexist" => 0, 'mentors' => $mentorval, 'total' => count($mentorval)));
           }
        exit;
    }
}
