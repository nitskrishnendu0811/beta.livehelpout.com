<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class InterestsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
   
   public function skipreason()
   {
     $array=$this->whyskip();
     echo json_encode($array);
     exit;
   }
    
   public function submitskipreason()
   {
     $interest['Interest']['userid']=$_REQUEST['userid'];
     $interest['Interest']['help_id']=$_REQUEST['help_id'];
     $interest['Interest']['is_skipped']=  intval($_REQUEST['is_skipped']);
     $interest['Interest']['skipreason']=$_REQUEST['skipreason'];
     $interest['Interest']['is_interest']=0;
     $interest['Interest']['interest_details']='';
     $this->Interest->create();
     $this->Interest->save($interest);
     exit;
  }

  public function showinterest()
  {
     $this->loadModel('Help');
     $this->loadModel('Notification');
     $datetime = date('Y-m-d H:i:s');
     
     $interest['Interest']['userid']=$_REQUEST['userid'];
     $interest['Interest']['help_id']=$_REQUEST['help_id'];
     $interest['Interest']['is_skipped']=  0;
     $interest['Interest']['skipreason']='';
     $interest['Interest']['is_interest']=1;
     $interest['Interest']['interest_details']='';
     $this->Interest->create();
     $this->Interest->save($interest);
     $interestid = $this->Interest->getLastInsertId();
     
     $optionrequest = array('conditions' => array('Help.id' => $_REQUEST['help_id']));
     $help = $this->Help->find('first', $optionrequest);
     
     $notification['Notification']['helpid'] = $_REQUEST['help_id'];
     $notification['Notification']['touserid'] = $help['Help']['userid'];
     $notification['Notification']['fromuserid'] = $_REQUEST['userid'];
     $notification['Notification']['date'] = $datetime;
     $notification['Notification']['type'] = 'interest';
     $notification['Notification']['is_read'] = 0;
     $notification['Notification']['parent_table'] = 'Interest';
     $notification['Notification']['parent_table_id'] = $interestid;
     $this->Notification->create();
     $this->Notification->save($notification);
     exit;
  }
    
    
    
}
