<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');
    
    public $paginate = array(
          'limit' =>15,
          'order' => array(
             'Users.order_rank' => 'desc'
           )
     ); 

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $usertype=(int)$_REQUEST['usertype'];
	$options = array('conditions' => array('User.role' => $usertype));
        $users=$this->User->find('all', $options);
	
       
        
	$uerarray=array();
	foreach($users as $user){
	   $userarray[]=array('id'=>$user['User']['id'],'name'=>$user['User']['name'],'email'=>$user['User']['email'],'username'=>$user['User']['username'],'is_profile_completed'=>$user['User']['is_profile_completed'],'userimage'=>$user['User']['image']);
	}

	if(!empty($userarray)){
	echo json_encode(array('usercount'=>count($userarray),'users'=>$userarray));
	}
	exit;
        //echo 'here';
    }

    public function delete() {
        $id=$_REQUEST['id'];
	$this->loadModel('Help');
	$this->loadModel('Comment');
	$this->loadModel('Interest');
	$this->loadModel('Like');
	$this->loadModel('RequestFile');
	$this->loadModel('Notification');

	$conditioncomment=array('conditions'=>array('Comment.userid'=>$id));
	$comments=$this->Comment->find('all', $conditioncomment);
	foreach($comments as $comment){
	$conditionnotification=array('conditions'=>array('Notification.parent_table_id'=>$comment['Comment']['id'],'Notification.parent_table'=>'Comment'));
	$notification=$this->NPaginatorPaginatorotification->find('first', $conditionnotification);	
	if(!empty($notification)){
	$this->Notification->delete($notification['Notification']['id']);
	}
	$this->Comment->delete($comment['Comment']['id']);
	}

	$conditionInterest=array('conditions'=>array('Interest.userid'=>$id));
	$Interests=$this->Interest->find('all', $conditionInterest);
	foreach($Interests as $Interest){
	$conditionnotification=array('conditions'=>array('Notification.parent_table_id'=>$Interest['Interest']['id'],'Notification.parent_table'=>'Interest'));
	$notification=$this->Notification->find('first', $conditionnotification);	
	if(!empty($notification)){
	$this->Notification->delete($notification['Notification']['id']);
	}
	$this->Interest->delete($Interest['Interest']['id']);
	}

	$conditionRequest=array('conditions'=>array('Help.userid'=>$id));
	$Helps=$this->Help->find('all', $conditionRequest);
	foreach($Helps as $Help){
	$conditionnotification=array('conditions'=>array('Notification.parent_table_id'=>$Help['Help']['id'],'Notification.parent_table'=>'Help'));
	$notification=$this->Notification->find('first', $conditionnotification);	
	if(!empty($notification)){
	$this->Notification->delete($notification['Notification']['id']);
	}

	$filescond=array('conditions'=>array('RequestFile.helpid'=>$Help['Help']['id']));
	$files=$this->RequestFile->find('all', $filescond);
	foreach($files as $file){
	$this->RequestFile->delete($file['RequestFile']['id']);
	}
	$this->Help->delete($Help['Help']['id']);
	}

	$conditionlike=array('conditions'=>array('Like.userid'=>$id));
	$likes=$this->Like->find('all', $conditionlike);
	foreach($likes as $like){
	$conditionnotification=array('conditions'=>array('Notification.parent_table_id'=>$like['Like']['id'],'Notification.parent_table'=>'Like'));
	$notification=$this->Notification->find('first', $conditionnotification);	
	if(!empty($notification)){
	$this->Notification->delete($notification['Notification']['id']);
	}
	$this->Like->delete($like['Like']['id']);
	}

	$this->User->delete($id);
	
	
	
	exit;
    }


    
    public function signupuser() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email']));
           $user=$this->User->find('first', $options);
           if(empty($user)){
             $user['User']['email']=$_REQUEST['email'];
             $user['User']['name']='';
             $user['User']['username']='';
             $user['User']['company_name']='';
             $user['User']['website']='';
             $user['User']['image']='';
             $user['User']['role']=1;
             $user['User']['login_status']=0;
             $user['User']['is_active']=0;
             $user['User']['is_profile_completed']=0;
             $user['User']['get_help_completed']=0;
             $user['User']['required_categories']='';
             $user['User']['expert_level']='';
             $user['User']['why_to_like']='';
             $user['User']['price']=0;
             $length = 5;
             //$randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
             //$user['User']['password']=  md5($randomString);
             
             
             $this->User->create();
             $this->User->save($user);
             $userid = $this->User->getLastInsertId();
             
             $this->loadModel('EmailTemplate'); 
             $siteurl = Configure::read('APP_SITE_URL');
             $REGISTRATION_EMAIL_TEMPLATE = Configure::read('REGISTRATION_EMAIL_TEMPLATE');
             $link='<a href='.$siteurl.'frontend/account-activation/'.base64_encode($userid).'><b>Confirm my account</b></a>';
             $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_id'=>$REGISTRATION_EMAIL_TEMPLATE)));
             $mail_body =str_replace(array('[CONFIRMATION LINK]'),array($link),$EmailTemplate['EmailTemplate']['content']);
            // echo $link;
             //$mail_body =str_replace(array('[CONFIRMATION LINK]','[EMAIL]','[PASSWORD]'),array('',$_REQUEST['email'],$link),$EmailTemplate['EmailTemplate']['content']);
	     $this->send_mail($EmailTemplate['EmailTemplate']['mail_from'],$_REQUEST['email'],$EmailTemplate['EmailTemplate']['subject'],$mail_body);

             echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully signed up. Please check your mail and confirm signup.')); 
           }else{
             echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different email.'));
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function signupmentor() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='' && $_REQUEST['name']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email']));
           $user=$this->User->find('first', $options);
           if(empty($user)){
             $user['User']['email']=$_REQUEST['email'];
             $user['User']['name']=$_REQUEST['name'];
             $user['User']['username']='';
             $user['User']['company_name']='';
             $user['User']['website']='';
             $user['User']['login_status']=0;
             $user['User']['image']='';
             $user['User']['role']=2;
             $user['User']['is_active']=0;
             $user['User']['is_profile_completed']=0;
             $user['User']['get_help_completed']=0;
             $user['User']['required_categories']='';
             $user['User']['expert_level']='';
             $user['User']['why_to_like']='';
             $user['User']['price']=0;
             $this->User->create();
             $this->User->save($user);
             $userid = $this->User->getLastInsertId();
             $base64encodeduserid=base64_encode($userid);
             
             $this->loadModel('EmailTemplate');
             $siteurl = Configure::read('SITE_URL');
             $REGISTRATION_EMAIL_TEMPLATE = Configure::read('REGISTRATION_EMAIL_TEMPLATE');
             $link='<a href='.$siteurl.'#/frontend/account-activation/'.base64_encode($userid).'><b>Confirm my account</b></a>';
             $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_id'=>$REGISTRATION_EMAIL_TEMPLATE)));
             $mail_body =str_replace(array('[CONFIRMATION LINK]'),array($link),$EmailTemplate['EmailTemplate']['content']);
	     $this->send_mail($EmailTemplate['EmailTemplate']['mail_from'],$_REQUEST['email'],$EmailTemplate['EmailTemplate']['subject'],$mail_body);

             echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully signed up. Please check your mail and confirm signup.',"userid"=>$base64encodeduserid)); 
           }else{
             echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different email.'));
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your signup details.'));
      }
        exit;
    }
    
    public function fbsignupmentor() 
    {
      if(isset($_REQUEST['email']) && isset($_REQUEST['fbid'])  && $_REQUEST['email']!='' && $_REQUEST['fbid']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email']));
           $user=$this->User->find('first', $options);
           if(empty($user)){
             echo json_encode(array("msg_type"=>1));
           
            /*$length = 6;
				$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.'0123456789';
				$str = '';
				$max = strlen($chars) - 1;
				for ($i=0; $i < $length; $i++)
				$str .= $chars[rand(0, $max)];
				
             $user['User']['email']=$_REQUEST['email'];
             $user['User']['name']=$_REQUEST['name'];
             $user['User']['fbid']=$_REQUEST['fbid'];
             $user['User']['username']=trim($_REQUEST['name']," ").$str;
             $user['User']['company_name']='';
             $user['User']['website']='';
             $user['User']['login_status']=2;
             $user['User']['image']='';
             $user['User']['role']=2;
             $user['User']['is_active']=1;
             $user['User']['is_profile_completed']=0;
             $user['User']['get_help_completed']=0;
             $user['User']['required_categories']='';
             $user['User']['expert_level']='';
             $user['User']['why_to_like']='';
             $user['User']['price']=0;
             $this->User->create();
             $this->User->save($user);
             $userid = $this->User->getLastInsertId();
             $base64encodeduserid=base64_encode($userid);
             
             $this->loadModel('EmailTemplate');
             $siteurl = Configure::read('SITE_URL');
             $REGISTRATION_EMAIL_TEMPLATE = Configure::read('REGISTRATION_EMAIL_TEMPLATE');
             $link='<a href='.$siteurl.'#/frontend/account-activation/'.base64_encode($userid).'><b>Confirm my account</b></a>';
             $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_id'=>$REGISTRATION_EMAIL_TEMPLATE)));
             $mail_body =str_replace(array('[CONFIRMATION LINK]'),array($link),$EmailTemplate['EmailTemplate']['content']);
	        $this->send_mail($EmailTemplate['EmailTemplate']['mail_from'],$_REQUEST['email'],$EmailTemplate['EmailTemplate']['subject'],$mail_body);

             echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully signed up. Please check your mail and confirm signup.',"userid"=>$base64encodeduserid)); */
           }else{
             
              $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.fbid'=>$_REQUEST['fbid']));
              $fbuser=$this->User->find('first', $options);
              if(!empty($fbuser))
              {
					$loggedinuser['User']['id']=$fbuser['User']['id'];
					$loggedinuser['User']['is_logged_in']=1;
					$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
					$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
					$loggedinuser['User']['image'] = 'https://graph.facebook.com/'.$_REQUEST['fbid'].'/picture?type=large&height=250&width=250';
					$this->User->save($loggedinuser);

					$userdetails['email'] = $fbuser['User']['email'];
					$userdetails['username'] = $fbuser['User']['username'];
					$userdetails['name'] = $fbuser['User']['name'];
					$userdetails['image'] = 'https://graph.facebook.com/'.$_REQUEST['fbid'].'/picture?type=large&height=250&width=250';
					$userdetails['id'] = $fbuser['User']['id'];
					$userdetails['isloggedin'] = true;
					$userdetails['role'] = $fbuser['User']['role'];
					if($userdetails['role']==2)
					{
						$userdetails['price']=$fbuser['User']['price'];
					}
					else
					{
						$userdetails['price']=0;
					}
					$userdetails['login_status'] = $fbuser['User']['login_status'];
					echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
              }
              else{
              		echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different Facebook account.'));
              }
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your signup details.'));
      }
        exit;
    }
    
    public function fbloginuser() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.fbid'=>$_REQUEST['fbid']));
           $fbuser=$this->User->find('first', $options);
           if(!empty($fbuser))
              {
					$loggedinuser['User']['id']=$fbuser['User']['id'];
					$loggedinuser['User']['is_logged_in']=1;
					$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
					$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
					$loggedinuser['User']['image'] = 'https://graph.facebook.com/'.$_REQUEST['fbid'].'/picture?type=large&height=250&width=250';
					$this->User->save($loggedinuser);

					$userdetails['fbid'] = '';$userdetails['imgtype']='1';
					if(isset($fbuser['User']['fbid']) && $fbuser['User']['fbid']!='')
					{
						$userdetails['fbid']=$fbuser['User']['fbid'];$userdetails['imgtype']='2';
					}
					$userdetails['email'] = $fbuser['User']['email'];
					$userdetails['username'] = $fbuser['User']['username'];
					$userdetails['name'] = $fbuser['User']['name'];
					$userdetails['image'] = 'https://graph.facebook.com/'.$_REQUEST['fbid'].'/picture?type=large&height=250&width=250';
					$userdetails['id'] = $fbuser['User']['id'];
					$userdetails['isloggedin'] = true;
					$userdetails['role'] = $fbuser['User']['role'];
					if($userdetails['role']==2)
					{
						$userdetails['price']=$fbuser['User']['price'];
					}
					else
					{
						$userdetails['price']=0;
					}
					$userdetails['login_status'] = $fbuser['User']['login_status'];
					echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
              }
              else{
              		echo json_encode(array("msg_type"=>0,"msg"=>'The facebook account is not registered yet. Please signup using the facebook account then Sign In.'));
              }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function fbsignupuser() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email']));
           $user=$this->User->find('first', $options);
           if(empty($user)){
            echo json_encode(array("msg_type"=>1));
           }else{
             
              $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.fbid'=>$_REQUEST['fbid']));
              $fbuser=$this->User->find('first', $options);
              if(!empty($fbuser))
              {
					$loggedinuser['User']['id']=$fbuser['User']['id'];
					$loggedinuser['User']['is_logged_in']=1;
					$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
					$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
					$loggedinuser['User']['image'] = 'https://graph.facebook.com/'.$_REQUEST['fbid'].'/picture?type=large&height=250&width=250';
					$this->User->save($loggedinuser);

					$userdetails['fbid'] = '';$userdetails['imgtype']='1';
					if(isset($fbuser['User']['fbid']) && $fbuser['User']['fbid']!='')
					{
						$userdetails['fbid']=$fbuser['User']['fbid'];$userdetails['imgtype']='2';
					}
					$userdetails['email'] = $fbuser['User']['email'];
					$userdetails['username'] = $fbuser['User']['username'];
					$userdetails['name'] = $fbuser['User']['name'];
					$userdetails['image'] = 'https://graph.facebook.com/'.$_REQUEST['fbid'].'/picture?type=large&height=250&width=250';
					$userdetails['id'] = $fbuser['User']['id'];
					$userdetails['isloggedin'] = true;
					$userdetails['role'] = $fbuser['User']['role'];
					if($userdetails['role']==2)
					{
						$userdetails['price']=$fbuser['User']['price'];
					}
					else
					{
						$userdetails['price']=0;
					}
					$userdetails['login_status'] = $fbuser['User']['login_status'];
					echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
              }
              else{
              		echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different Facebook account.'));
              }
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function fbsignupusername() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           
           if(isset($_REQUEST['username']) && $_REQUEST['username']!='')
		 {
		      $options = array('conditions' => array('User.username' => $_REQUEST['username']));
		      $userUsername=$this->User->find('first', $options);
		      if(empty($userUsername))
			 {
				 $options = array('conditions' => array('User.email' => $_REQUEST['email']));
				 $user=$this->User->find('first', $options);
				 if(empty($user)){
				  
				
				   $user['User']['email']=$_REQUEST['email'];
				   $user['User']['name']=$_REQUEST['name'];
				   $user['User']['fbid']=$_REQUEST['fbid'];
				   $user['User']['username']=$_REQUEST['username'];
				   $user['User']['company_name']='';
				   $user['User']['timezone']='';
				   $user['User']['website']='';
				   $user['User']['image']='https://graph.facebook.com/'.$_REQUEST['fbid'].'/picture?type=large&height=250&width=250';
				   $user['User']['role']=1;
				   $user['User']['login_status']=2;
				   $user['User']['is_active']=1;
				   $user['User']['is_profile_completed']=0;
				   $user['User']['get_help_completed']=0;
				   $user['User']['required_categories']='';
				   $user['User']['expert_level']='';
				   $user['User']['why_to_like']='';
				   $user['User']['price']=0;
				   $length = 5;
				   $this->User->create();
				   $this->User->save($user);
				   $userid = $this->User->getLastInsertId();
				   $base64encodeduserid=base64_encode($userid);
				   
				    $options = array('conditions' => array('User.id' => $userid));
				    $fbuser=$this->User->find('first', $options);
				    if(!empty($fbuser))
				    {
							$loggedinuser['User']['id']=$fbuser['User']['id'];
							$loggedinuser['User']['is_logged_in']=1;
							$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
							$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
							$this->User->save($loggedinuser);

							$userdetails['fbid'] = '';$userdetails['imgtype']='1';
							if(isset($fbuser['User']['fbid']) && $fbuser['User']['fbid']!='')
							{
								$userdetails['fbid']=$fbuser['User']['fbid'];$userdetails['imgtype']='2';
							}
							$userdetails['email'] = $fbuser['User']['email'];
							$userdetails['username'] = $fbuser['User']['username'];
							$userdetails['name'] = $fbuser['User']['name'];
							$userdetails['image'] = $fbuser['User']['image'];
							$userdetails['id'] = $fbuser['User']['id'];
							$userdetails['isloggedin'] = true;
							$userdetails['role'] = $fbuser['User']['role'];
							if($userdetails['role']==2)
							{
								$userdetails['price']=$fbuser['User']['price'];
							}
							else
							{
								$userdetails['price']=0;
							}
							$userdetails['login_status'] = $fbuser['User']['login_status'];
							echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
				    }
				   

				   //echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully signed up. Please check your mail and confirm signup.',"userid"=>$base64encodeduserid)); 
				 }else{
				   
				    $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.fbid'=>$_REQUEST['fbid']));
				    $fbuser=$this->User->find('first', $options);
				    if(!empty($fbuser))
				    {
							$loggedinuser['User']['id']=$fbuser['User']['id'];
							$loggedinuser['User']['is_logged_in']=1;
							$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
							$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
							$this->User->save($loggedinuser);

							$userdetails['email'] = $fbuser['User']['email'];
							$userdetails['username'] = $fbuser['User']['username'];
							$userdetails['name'] = $fbuser['User']['name'];
							$userdetails['image'] = $fbuser['User']['image'];
							$userdetails['id'] = $fbuser['User']['id'];
							$userdetails['isloggedin'] = true;
							$userdetails['role'] = $fbuser['User']['role'];
							if($userdetails['role']==2)
							{
								$userdetails['price']=$fbuser['User']['price'];
							}
							else
							{
								$userdetails['price']=0;
							}
							$userdetails['login_status'] = $fbuser['User']['login_status'];
							echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
				    }
				    else{
				    		echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different Facebook account.'));
				    }
				 }
		      }
		      else{
		      	echo json_encode(array("msg_type"=>0,"msg"=>'Username already exists. Please use different Username.'));
		      }
           }
           else{
           	echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your username.'));
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function gploginuser() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.gpid'=>$_REQUEST['gpid']));
           $fbuser=$this->User->find('first', $options);
           if(!empty($fbuser))
              {
					$loggedinuser['User']['id']=$fbuser['User']['id'];
					$loggedinuser['User']['is_logged_in']=1;
					$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
					$loggedinuser['User']['image'] = $_REQUEST['image'];
					$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
					$this->User->save($loggedinuser);

					$userdetails['gpid'] = '';$userdetails['imgtype']='4';
					if(isset($fbuser['User']['gpid']) && $fbuser['User']['gpid']!='')
					{
						$userdetails['gpid']=$fbuser['User']['gpid'];
					}if($fbuser['User']['image']!=''){
						$userdetails['imgtype']='4';
					}
					$userdetails['email'] = $fbuser['User']['email'];
					$userdetails['username'] = $fbuser['User']['username'];
					$userdetails['name'] = $fbuser['User']['name'];
					$userdetails['image'] = $_REQUEST['image'];
					$userdetails['id'] = $fbuser['User']['id'];
					$userdetails['isloggedin'] = true;
					$userdetails['role'] = $fbuser['User']['role'];
					if($userdetails['role']==2)
					{
						$userdetails['price']=$fbuser['User']['price'];
					}
					else
					{
						$userdetails['price']=0;
					}
					$userdetails['login_status'] = $fbuser['User']['login_status'];
					echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
              }
              else{
              		echo json_encode(array("msg_type"=>0,"msg"=>'The google account is not registered yet. Please signup using the google account then Sign In.'));
              }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function gpsignupuser() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email']));
           $user=$this->User->find('first', $options);
           if(empty($user)){
            echo json_encode(array("msg_type"=>1));
            
           }else{
             
              $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.gpid'=>$_REQUEST['gpid']));
              $fbuser=$this->User->find('first', $options);
              if(!empty($fbuser))
              {
					$loggedinuser['User']['id']=$fbuser['User']['id'];
					$loggedinuser['User']['is_logged_in']=1;
					$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
					$loggedinuser['User']['image'] = $_REQUEST['image'];
					$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
					$this->User->save($loggedinuser);

					$userdetails['gpid'] = '';$userdetails['imgtype']='4';
					if(isset($fbuser['User']['gpid']) && $fbuser['User']['gpid']!='')
					{
						$userdetails['gpid']=$fbuser['User']['gpid'];
					}if($fbuser['User']['image']!=''){
						$userdetails['imgtype']='4';
					}
					$userdetails['email'] = $fbuser['User']['email'];
					$userdetails['username'] = $fbuser['User']['username'];
					$userdetails['name'] = $fbuser['User']['name'];
					$userdetails['image'] = $_REQUEST['image'];
					$userdetails['id'] = $fbuser['User']['id'];
					$userdetails['isloggedin'] = true;
					$userdetails['role'] = $fbuser['User']['role'];
					if($userdetails['role']==2)
					{
						$userdetails['price']=$fbuser['User']['price'];
					}
					else
					{
						$userdetails['price']=0;
					}
					$userdetails['login_status'] = $fbuser['User']['login_status'];
					echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
              }
              else{
              		echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different Google account.'));
              }
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function gpsignupusername() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           
           if(isset($_REQUEST['username']) && $_REQUEST['username']!='')
		 {
		      $options = array('conditions' => array('User.username' => $_REQUEST['username']));
		      $userUsername=$this->User->find('first', $options);
		      if(empty($userUsername))
			 {
				 $options = array('conditions' => array('User.email' => $_REQUEST['email']));
				 $user=$this->User->find('first', $options);
				 if(empty($user)){
				  
				
				   $user['User']['email']=$_REQUEST['email'];
				   $user['User']['name']=$_REQUEST['name'];
				   $user['User']['gpid']=$_REQUEST['gpid'];
				   $user['User']['username']=$_REQUEST['username'];
				   //$user['User']['location']=$_REQUEST['location'];
				   $user['User']['image']=$_REQUEST['image'];
				   $user['User']['company_name']='';
				   $user['User']['timezone']='';
				   $user['User']['website']='';
				   $user['User']['role']=1;
				   $user['User']['login_status']=2;
				   $user['User']['is_active']=1;
				   $user['User']['is_profile_completed']=0;
				   $user['User']['get_help_completed']=0;
				   $user['User']['required_categories']='';
				   $user['User']['expert_level']='';
				   $user['User']['why_to_like']='';
				   $user['User']['price']=0;
				   $length = 5;
				   $this->User->create();
				   $this->User->save($user);
				   $userid = $this->User->getLastInsertId();
				   $base64encodeduserid=base64_encode($userid);
				   
				    $options = array('conditions' => array('User.id' => $userid));
				    $fbuser=$this->User->find('first', $options);
				    if(!empty($fbuser))
				    {
							$loggedinuser['User']['id']=$fbuser['User']['id'];
							$loggedinuser['User']['is_logged_in']=1;
							$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
							$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
							$this->User->save($loggedinuser);

							$userdetails['gpid'] = '';$userdetails['imgtype']='4';
							if(isset($fbuser['User']['gpid']) && $fbuser['User']['gpid']!='')
							{
								$userdetails['gpid']=$fbuser['User']['gpid'];
							}if($fbuser['User']['image']!=''){
								$userdetails['imgtype']='4';
							}
							$userdetails['email'] = $fbuser['User']['email'];
							$userdetails['username'] = $fbuser['User']['username'];
							$userdetails['name'] = $fbuser['User']['name'];
							$userdetails['image'] = $fbuser['User']['image'];
							$userdetails['id'] = $fbuser['User']['id'];
							$userdetails['isloggedin'] = true;
							$userdetails['role'] = $fbuser['User']['role'];
							if($userdetails['role']==2)
							{
								$userdetails['price']=$fbuser['User']['price'];
							}
							else
							{
								$userdetails['price']=0;
							}
							$userdetails['login_status'] = $fbuser['User']['login_status'];
							echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
				    }
				   

				   //echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully signed up. Please check your mail and confirm signup.',"userid"=>$base64encodeduserid)); 
				 }else{
				   
				    $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.gpid'=>$_REQUEST['gpid']));
				    $fbuser=$this->User->find('first', $options);
				    if(!empty($fbuser))
				    {
							$loggedinuser['User']['id']=$fbuser['User']['id'];
							$loggedinuser['User']['is_logged_in']=1;
							$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
							$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
							$this->User->save($loggedinuser);

							$userdetails['email'] = $fbuser['User']['email'];
							$userdetails['username'] = $fbuser['User']['username'];
							$userdetails['name'] = $fbuser['User']['name'];
							$userdetails['image'] = $fbuser['User']['image'];
							$userdetails['id'] = $fbuser['User']['id'];
							$userdetails['isloggedin'] = true;
							$userdetails['role'] = $fbuser['User']['role'];
							if($userdetails['role']==2)
							{
								$userdetails['price']=$fbuser['User']['price'];
							}
							else
							{
								$userdetails['price']=0;
							}
							$userdetails['login_status'] = $fbuser['User']['login_status'];
							echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
				    }
				    else{
				    		echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different Google account.'));
				    }
				 }
		      }
		      else{
		      	echo json_encode(array("msg_type"=>0,"msg"=>'Username already exists. Please use different Username.'));
		      }
           }
           else{
           	echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your username.'));
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function lnloginuser() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.lnid'=>$_REQUEST['lnid']));
           $fbuser=$this->User->find('first', $options);
           if(!empty($fbuser))
              {
					$loggedinuser['User']['id']=$fbuser['User']['id'];
					$loggedinuser['User']['is_logged_in']=1;
					$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
					$loggedinuser['User']['image'] = $_REQUEST['image'];
					$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
					$this->User->save($loggedinuser);

					$userdetails['lnid'] = '';$userdetails['imgtype']='3';
					if(isset($fbuser['User']['lnid']) && $fbuser['User']['lnid']!='')
					{
						$userdetails['lnid']=$fbuser['User']['lnid'];
					}if($fbuser['User']['image']!=''){
						$userdetails['imgtype']='3';
					}
					$userdetails['email'] = $fbuser['User']['email'];
					$userdetails['username'] = $fbuser['User']['username'];
					$userdetails['name'] = $fbuser['User']['name'];
					$userdetails['image'] = $_REQUEST['image'];
					$userdetails['id'] = $fbuser['User']['id'];
					$userdetails['isloggedin'] = true;
					$userdetails['role'] = $fbuser['User']['role'];
					if($userdetails['role']==2)
					{
						$userdetails['price']=$fbuser['User']['price'];
					}
					else
					{
						$userdetails['price']=0;
					}
					$userdetails['login_status'] = $fbuser['User']['login_status'];
					echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
              }
              else{
              		echo json_encode(array("msg_type"=>0,"msg"=>'The linkedin account is not registered yet. Please signup using the linkedin account then Sign In.'));
              }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function lnsignupuser() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           $options = array('conditions' => array('User.email' => $_REQUEST['email']));
           $user=$this->User->find('first', $options);
           if(empty($user)){
            echo json_encode(array("msg_type"=>1));
            /*$length = 6;
				$chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.'0123456789';
				$str = '';
				$max = strlen($chars) - 1;
				for ($i=0; $i < $length; $i++)
				$str .= $chars[rand(0, $max)];
				
             $user['User']['email']=$_REQUEST['email'];
             $user['User']['name']=$_REQUEST['name'];
             $user['User']['fbid']=$_REQUEST['fbid'];
             $user['User']['username']=trim($_REQUEST['name']," ").$str;
             $user['User']['company_name']='';
             $user['User']['website']='';
             $user['User']['image']='';
             $user['User']['role']=1;
             $user['User']['login_status']=2;
             $user['User']['is_active']=1;
             $user['User']['is_profile_completed']=0;
             $user['User']['get_help_completed']=0;
             $user['User']['required_categories']='';
             $user['User']['expert_level']='';
             $user['User']['why_to_like']='';
             $user['User']['price']=0;
             $length = 5;
             $this->User->create();
             $this->User->save($user);
             $userid = $this->User->getLastInsertId();
             $base64encodeduserid=base64_encode($userid);
             
              $options = array('conditions' => array('User.id' => $userid));
              $fbuser=$this->User->find('first', $options);
              if(!empty($fbuser))
              {
					$loggedinuser['User']['id']=$fbuser['User']['id'];
					$loggedinuser['User']['is_logged_in']=1;
					$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
					$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
					$this->User->save($loggedinuser);

					$userdetails['email'] = $fbuser['User']['email'];
					$userdetails['username'] = $fbuser['User']['username'];
					$userdetails['name'] = $fbuser['User']['name'];
					$userdetails['image'] = $fbuser['User']['image'];
					$userdetails['id'] = $fbuser['User']['id'];
					$userdetails['isloggedin'] = true;
					$userdetails['role'] = $fbuser['User']['role'];
					if($userdetails['role']==2)
					{
						$userdetails['price']=$fbuser['User']['price'];
					}
					else
					{
						$userdetails['price']=0;
					}
					$userdetails['login_status'] = $fbuser['User']['login_status'];
					echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
              }
             

             echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully signed up. Please check your mail and confirm signup.',"userid"=>$base64encodeduserid)); */
           }else{
             
              $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.lnid'=>$_REQUEST['lnid']));
              $fbuser=$this->User->find('first', $options);
              if(!empty($fbuser))
              {
					$loggedinuser['User']['id']=$fbuser['User']['id'];
					$loggedinuser['User']['is_logged_in']=1;
					$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
					$loggedinuser['User']['image'] = $_REQUEST['image'];
					$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
					$this->User->save($loggedinuser);

					$userdetails['lnid'] = '';$userdetails['imgtype']='3';
					if(isset($fbuser['User']['lnid']) && $fbuser['User']['lnid']!='')
					{
						$userdetails['lnid']=$fbuser['User']['lnid'];
					}if($fbuser['User']['image']!=''){
						$userdetails['imgtype']='3';
					}
					$userdetails['email'] = $fbuser['User']['email'];
					$userdetails['username'] = $fbuser['User']['username'];
					$userdetails['name'] = $fbuser['User']['name'];
					$userdetails['image'] = $_REQUEST['image'];
					$userdetails['id'] = $fbuser['User']['id'];
					$userdetails['isloggedin'] = true;
					$userdetails['role'] = $fbuser['User']['role'];
					if($userdetails['role']==2)
					{
						$userdetails['price']=$fbuser['User']['price'];
					}
					else
					{
						$userdetails['price']=0;
					}
					$userdetails['login_status'] = $fbuser['User']['login_status'];
					echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
              }
              else{
              		echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different Linkedin account.'));
              }
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function lnsignupusername() 
    {
      if(isset($_REQUEST['email']) && $_REQUEST['email']!='')
      {
        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }
        else
        {
           
           if(isset($_REQUEST['username']) && $_REQUEST['username']!='')
		 {
		      $options = array('conditions' => array('User.username' => $_REQUEST['username']));
		      $userUsername=$this->User->find('first', $options);
		      if(empty($userUsername))
			 {
				 $options = array('conditions' => array('User.email' => $_REQUEST['email']));
				 $user=$this->User->find('first', $options);
				 if(empty($user)){
				  
				
				   $user['User']['email']=$_REQUEST['email'];
				   $user['User']['name']=$_REQUEST['name'];
				   $user['User']['lnid']=$_REQUEST['lnid'];
				   $user['User']['username']=$_REQUEST['username'];
				   $user['User']['location']=$_REQUEST['location'];
				   $user['User']['image']=$_REQUEST['image'];
				   $user['User']['company_name']='';
				   $user['User']['timezone']='';
				   $user['User']['website']='';
				   $user['User']['role']=1;
				   $user['User']['login_status']=2;
				   $user['User']['is_active']=1;
				   $user['User']['is_profile_completed']=0;
				   $user['User']['get_help_completed']=0;
				   $user['User']['required_categories']='';
				   $user['User']['expert_level']='';
				   $user['User']['why_to_like']='';
				   $user['User']['price']=0;
				   $length = 5;
				   $this->User->create();
				   $this->User->save($user);
				   $userid = $this->User->getLastInsertId();
				   $base64encodeduserid=base64_encode($userid);
				   
				    $options = array('conditions' => array('User.id' => $userid));
				    $fbuser=$this->User->find('first', $options);
				    if(!empty($fbuser))
				    {
							$loggedinuser['User']['id']=$fbuser['User']['id'];
							$loggedinuser['User']['is_logged_in']=1;
							$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
							$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
							$this->User->save($loggedinuser);

							$userdetails['lnid'] = '';$userdetails['imgtype']='3';
							if(isset($fbuser['User']['lnid']) && $fbuser['User']['lnid']!='')
							{
								$userdetails['lnid']=$fbuser['User']['lnid'];
							}if($fbuser['User']['image']!=''){
								$userdetails['imgtype']='3';
							}
							$userdetails['email'] = $fbuser['User']['email'];
							$userdetails['username'] = $fbuser['User']['username'];
							$userdetails['name'] = $fbuser['User']['name'];
							$userdetails['image'] = $fbuser['User']['image'];
							$userdetails['id'] = $fbuser['User']['id'];
							$userdetails['isloggedin'] = true;
							$userdetails['role'] = $fbuser['User']['role'];
							if($userdetails['role']==2)
							{
								$userdetails['price']=$fbuser['User']['price'];
							}
							else
							{
								$userdetails['price']=0;
							}
							$userdetails['login_status'] = $fbuser['User']['login_status'];
							echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
				    }
				   

				   //echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully signed up. Please check your mail and confirm signup.',"userid"=>$base64encodeduserid)); 
				 }else{
				   
				    $options = array('conditions' => array('User.email' => $_REQUEST['email'],'User.lnid'=>$_REQUEST['lnid']));
				    $fbuser=$this->User->find('first', $options);
				    if(!empty($fbuser))
				    {
							$loggedinuser['User']['id']=$fbuser['User']['id'];
							$loggedinuser['User']['is_logged_in']=1;
							$loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
							$loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
							$this->User->save($loggedinuser);

							$userdetails['email'] = $fbuser['User']['email'];
							$userdetails['username'] = $fbuser['User']['username'];
							$userdetails['name'] = $fbuser['User']['name'];
							$userdetails['image'] = $fbuser['User']['image'];
							$userdetails['id'] = $fbuser['User']['id'];
							$userdetails['isloggedin'] = true;
							$userdetails['role'] = $fbuser['User']['role'];
							if($userdetails['role']==2)
							{
								$userdetails['price']=$fbuser['User']['price'];
							}
							else
							{
								$userdetails['price']=0;
							}
							$userdetails['login_status'] = $fbuser['User']['login_status'];
							echo json_encode(array("msg_type"=>2,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
				
				    }
				    else{
				    		echo json_encode(array("msg_type"=>0,"msg"=>'Email already exists. Please use different Linkedin account.'));
				    }
				 }
		      }
		      else{
		      	echo json_encode(array("msg_type"=>0,"msg"=>'Username already exists. Please use different Username.'));
		      }
           }
           else{
           	echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your username.'));
           }
        }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your email.'));
      }
        exit;
    }
    
    public function loginuser() 
    {
      if(isset($_REQUEST['username']) && $_REQUEST['username']!='' && $_REQUEST['password']!='')
      {
           $options = array('conditions' => array('User.is_active' => 1,'User.password'=>md5($_REQUEST['password']),'$or' => array(array("User.username" => $_REQUEST['username']),array("User.email" => $_REQUEST['username']))));
           $user=$this->User->find('first', $options);
           if(!empty($user)){
           
            
             $loggedinuser['User']['id']=$user['User']['id'];
             $loggedinuser['User']['is_logged_in']=1;
              $loggedinuser['User']['ip'] = $_SERVER['REMOTE_ADDR'];
              $loggedinuser['User']['last_login'] = date('Y-m-d H:i:s');
             $this->User->save($loggedinuser);
           
               /*$userdetails['fbid'] = '';
			if(isset($user['User']['fbid']) && $user['User']['fbid']!='')
			{
				$userdetails['fbid']=$user['User']['fbid'];
			}*/
			$userdetails['fbid'] = '';
			$userdetails['lnid'] = '';
			$userdetails['gid'] = '';
			$userdetails['imgtype'] = '1';
			
			if(isset($user['User']['fbid']) && $user['User']['fbid']!='')
			{
				$userdetails['fbid']=$user['User']['fbid'];
				$userdetails['imgtype'] = '2';
			}else if(isset($user['User']['lnid']) && $user['User']['lnid']!='')
			{
				$userdetails['lnid']=$user['User']['lnid'];
				$userdetails['imgtype'] = '3';
			}else if(isset($user['User']['gid']) && $user['User']['gid']!='')
			{
				$userdetails['gid']=$user['User']['gid'];
				$userdetails['imgtype'] = '4';
			}
             $userdetails['email'] = $user['User']['email'];
             $userdetails['username'] = $user['User']['username'];
             $userdetails['name'] = $user['User']['name'];
             $userdetails['image'] = $user['User']['image'];
             $userdetails['id'] = $user['User']['id'];
             $userdetails['isloggedin'] = true;
             $userdetails['role'] = $user['User']['role'];
             if($userdetails['role']==2)
             {
               $userdetails['price']=$user['User']['price'];
             }
             else
             {
               $userdetails['price']=0;
             }
             $userdetails['login_status'] = $user['User']['login_status'];
             echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
           }else{
             echo json_encode(array("msg_type"=>0,"msg"=>'Invalid login details or activate your account.'));
           }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your login details.'));
      }
        exit;
    }
    
    
    public function forgotpassword(){        
        $email=$_REQUEST['forgotemail'];
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
        {
           echo json_encode(array("msg_type"=>0,"msg"=>'Please provide valid email address.'));
        }else{
           $options = array('conditions' => array('User.email' => $email,'User.is_active'=>1));
           $user=$this->User->find('first', $options);
           if(empty($user)){
               echo json_encode(array("msg_type"=>0,"msg"=>'This email is not registered with us or email not activated.'));
           }else{
                  $this->loadModel('EmailTemplate');
                  $siteurl = Configure::read('APP_SITE_URL');
                  $FORGOTPASSWORD_EMAIL_TEMPLATE = Configure::read('FORGOTPASSWORD_EMAIL_TEMPLATE');
                  $link='<a href='.$siteurl.'#/frontend/reset-password/'.base64_encode($user['User']['id']).'>Reset Password</a>';
                  $EmailTemplate=$this->EmailTemplate->find('first',array('conditions'=>array('EmailTemplate.template_id'=>$FORGOTPASSWORD_EMAIL_TEMPLATE)));
		  $mail_body =str_replace(array('[USER]','[RESETPASSWORD LINK]'),array($user['User']['name'],$link),$EmailTemplate['EmailTemplate']['content']);
		  $this->send_mail($EmailTemplate['EmailTemplate']['mail_from'],$email,$EmailTemplate['EmailTemplate']['subject'],$mail_body);
                 echo json_encode(array("msg_type"=>1,"msg"=>'Your forgot password request processed successfully. Please check your mail and reset password.'));
           }
           
        }
       exit; 
    }
    
    
    public function resetpassword(){
         $id=  base64_decode($_REQUEST['id']);
         $password=$_REQUEST['password'];
         $user['User']['id']=$id;
         $user['User']['password']=md5($password);
         $this->User->save($user);  
         echo json_encode(array("msg_type"=>1,"msg"=>'Your password is successfully reset.'));  
         exit;
    }
    
    
    
    public function changerole(){
         $loggedinuser['User']['id']=$_REQUEST['userid'];
         if($_REQUEST['role']=='mentor')
         {
           $loggedinuser['User']['role']=2;
         }
         elseif($_REQUEST['role']=='helpee')
         {
           $loggedinuser['User']['role']=1;
         }
         $this->User->save($loggedinuser);
         echo json_encode(array("msg_type"=>1));  
         exit;
    }
    
    
    public function getuserdetails()
    {
      $userid=$_REQUEST['userid'];
      $options = array('conditions' => array('User.id' => $userid));
      $user=$this->User->find('first', $options);
      if($user)
      {
        $userdetails['email'] = $user['User']['email'];
        $userdetails['username'] = $user['User']['username'];
        $userdetails['name'] = $user['User']['name'];
        //$userdetails['company_name'] = $user['User']['company_name'];
        //$userdetails['website'] = $user['User']['website'];
        $userdetails['image'] = $user['User']['image'];
        $userdetails['id'] = $user['User']['id'];
        $userdetails['isloggedin'] = true;
        $userdetails['role'] = $user['User']['role'];
        $userdetails['price']=$user['User']['price'];
        $userdetails['login_status'] = $user['User']['login_status'];
        $is_profile_completed = $user['User']['is_profile_completed'];
        $get_help_completed = $user['User']['get_help_completed'];
        $encoded_id = base64_encode($user['User']['id']);
        echo json_encode(array("msg_type"=>1,"msg"=>'Your account activated successfully.','userdetails'=>$userdetails,'is_profile_completed'=>$is_profile_completed,'encoded_id'=>$encoded_id,'get_help_completed'=>$get_help_completed));  
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'User not found.'));  
      }
      exit;
    }
    
    public function getswitcheduserdetails()
    {
      $userid=$_REQUEST['userid'];
      $options = array('conditions' => array('User.id' => base64_decode($userid)));
      $user=$this->User->find('first', $options);
      if($user)
      {
        $userdetails['email'] = $user['User']['email'];
        $userdetails['username'] = $user['User']['username'];
        $userdetails['name'] = $user['User']['name'];
        $userdetails['image'] = $user['User']['image'];
        $userdetails['id'] = $user['User']['id'];
        $userdetails['isloggedin'] = true;
        $userdetails['role'] = $user['User']['role'];
        $userdetails['price']=$user['User']['price'];
        $userdetails['login_status'] = $user['User']['login_status'];
        $is_profile_completed = $user['User']['is_profile_completed'];
        $get_help_completed = $user['User']['get_help_completed'];
        $encoded_id = base64_encode($user['User']['id']);
        echo json_encode(array("msg_type"=>1,"msg"=>'Your account activated successfully.','userdetails'=>$userdetails,'is_profile_completed'=>$is_profile_completed,'encoded_id'=>$encoded_id,'get_help_completed'=>$get_help_completed));  
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'User not found.'));  
      }
      exit;
    }
    
    public function updateAccountSetting(){
        $id=  ($_REQUEST['id']);
        $email=  $_REQUEST['email'];
        $username=  $_REQUEST['username'];
        $password=  $_REQUEST['password'];
        $options = array('conditions' => array('User.username' => $username,'User.id !=' => $id));
        $userUsername=$this->User->find('first', $options);
        if(empty($user)){
            $options = array('conditions' => array('User.email' => $email,'User.id !=' => $id));
            $userEmail=$this->User->find('first', $options);
            if(empty($userEmail))
            {
                $user['User']['id']=$id;
                $user['User']['email']=$email;
                $user['User']['username']=$username;
                if($password!='' || !empty($password))
                {
                   $user['User']['password']=md5($password); 
                }
                $this->User->save($user);

                $optionuserfind = array('conditions' => array('User.id' => $id));
                $userfind=$this->User->find('first', $optionuserfind);

                $userdetails['email'] = $userfind['User']['email'];
                $userdetails['username'] = $userfind['User']['username'];
                $userdetails['name'] = $userfind['User']['name'];
                $userdetails['image'] = $userfind['User']['image'];
                $userdetails['id'] = $userfind['User']['id'];
                $userdetails['isloggedin'] = true;
                $userdetails['role'] = $userfind['User']['role'];
                $userdetails['price']=$userfind['User']['price'];
                $userdetails['login_status'] = $userfind['User']['login_status'];

                echo json_encode(array("msg_type"=>1,"msg"=>'Your account activated successfully.','userdetails'=>$userdetails));
            }else{
                echo json_encode(array("msg_type"=>0,"msg"=>'Email already exist. Please use different email.'));                  }
              
        }else{
            echo json_encode(array("msg_type"=>0,"msg"=>'Username already exist. Please use different username.'));  
        }
        exit;
    }
    
    public function updateTaxInfo(){
		$this->LoadModel('TaxInformation');
		$id=  ($_REQUEST['id']);
		$full_name=  $_REQUEST['full_name'];
		$business_name=  $_REQUEST['business_name'];
		$address=  $_REQUEST['address'];
		$city=  $_REQUEST['city'];
		$state=  $_REQUEST['state'];
		$zip_code=  $_REQUEST['zip_code'];
		$classification=  $_REQUEST['classification'];
		$ssn=  $_REQUEST['ssn'];
		$ein=  $_REQUEST['ein'];
		$us_citizen=  $_REQUEST['us_citizen'];
		//$type=  $_REQUEST['type'];
		
		$options = array('conditions' => array('TaxInformation.user_id' => $id));
		$tx=$this->TaxInformation->find('first', $options);
		if(empty($tx)){
		  $user['TaxInformation']['user_id']=$id;
		  $user['TaxInformation']['full_name']=$full_name;
		  $user['TaxInformation']['business_name']=$business_name;
		  $user['TaxInformation']['address']=$address;
		  $user['TaxInformation']['city']=$city;
		  $user['TaxInformation']['state']=$state;
		  $user['TaxInformation']['zip_code']=$zip_code;
		  $user['TaxInformation']['classification']=$classification;
		  $user['TaxInformation']['ssn']=$ssn;
		  $user['TaxInformation']['ein']=$ein;
		  $user['TaxInformation']['us_citizen']=$us_citizen;
		  //$user['TaxInformation']['type']=$type;
		  $this->TaxInformation->save($user);
		  
		  
                   
                
		  echo json_encode(array("msg_type"=>1,"msg"=>'Your tax information saved successfully.'));  
		}else{
		  $user['TaxInformation']['id']=$tx['TaxInformation']['id'];
		  $user['TaxInformation']['user_id']=$id;
		  $user['TaxInformation']['full_name']=$full_name;
		  $user['TaxInformation']['business_name']=$business_name;
		  $user['TaxInformation']['address']=$address;
		  $user['TaxInformation']['city']=$city;
		  $user['TaxInformation']['state']=$state;
		  $user['TaxInformation']['zip_code']=$zip_code;
		  $user['TaxInformation']['classification']=$classification;
		  $user['TaxInformation']['ssn']=$ssn;
		  $user['TaxInformation']['ein']=$ein;
		  $user['TaxInformation']['us_citizen']=$us_citizen;
		  //$user['TaxInformation']['type']=$type;
		  $this->TaxInformation->save($user);
		  
		  
		  echo json_encode(array("msg_type"=>1,"msg"=>'Your tax information saved successfully.'));  
		}
		exit;
    }
    
    public function getusertaxinfo(){
		$this->LoadModel('TaxInformation');
		$id=  ($_REQUEST['id']);
		
		
		$optionuserfind = array('conditions' => array('TaxInformation.user_id' => $id));
		$userfind=$this->TaxInformation->find('first', $optionuserfind);
		if(!empty($userfind)){
		  $userdetails['full_name'] = $userfind['TaxInformation']['full_name'];
                 $userdetails['business_name'] = $userfind['TaxInformation']['business_name'];
                 $userdetails['address'] = $userfind['TaxInformation']['address'];
                 $userdetails['city'] = $userfind['TaxInformation']['city'];
                 $userdetails['state'] = $userfind['TaxInformation']['state'];
                 $userdetails['zip_code'] = $userfind['TaxInformation']['zip_code'];
                 $userdetails['classification'] = $userfind['TaxInformation']['classification'];
                 $userdetails['ssn'] = $userfind['TaxInformation']['ssn'];
                 $userdetails['ein'] = $userfind['TaxInformation']['ein'];
                 $userdetails['us_citizen']=$userfind['TaxInformation']['us_citizen'];  
                 $userdetails['id'] = $userfind['TaxInformation']['id'];
                
		  echo json_encode(array("msg_type"=>1,"msg"=>'Your tax information saved successfully.','userdetails'=>$userdetails));  
		}else{
		  $userdetails['full_name'] = '';
                 $userdetails['business_name'] = '';
                 $userdetails['address'] = '';
                 $userdetails['city'] = '';
                 $userdetails['state'] = '';
                 $userdetails['zip_code'] = '';
                 $userdetails['classification'] = '';
                 $userdetails['ssn'] = '';
                 $userdetails['ein'] = '';
                 $userdetails['us_citizen']= '';  
		  echo json_encode(array("msg_type"=>0,"msg"=>'Your tax information saved successfully.','userdetails'=>$userdetails));  
		}
		exit;
    }
    
    public function activateuser(){
        $id=  base64_decode($_REQUEST['id']);
        $name=  $_REQUEST['name'];
        $username=  $_REQUEST['username'];
        $password=  $_REQUEST['password'];
        $timezone=  $_REQUEST['timezone'];
        $options = array('conditions' => array('User.username' => $username));
        $user=$this->User->find('first', $options);
        if(empty($user)){
            $user['User']['id']=$id;
            $user['User']['name']=$name;
            $user['User']['username']=$username;
            $user['User']['timezone']=$timezone;
            $user['User']['password']=md5($password);
            $user['User']['is_active']=1;
            $user['User']['login_status']=2;
            $user['User']['is_logged_in']=1;
            $this->User->save($user);
            
            $optionuserfind = array('conditions' => array('User.id' => $id));
            $userfind=$this->User->find('first', $optionuserfind);
           
            $userdetails['email'] = $userfind['User']['email'];
            $userdetails['username'] = $userfind['User']['username'];
            $userdetails['name'] = $userfind['User']['name'];
            $userdetails['image'] = $userfind['User']['image'];
            $userdetails['id'] = $userfind['User']['id'];
            $userdetails['isloggedin'] = true;
            $userdetails['role'] = $userfind['User']['role'];
            $userdetails['price']=$userfind['User']['price'];
            $userdetails['login_status'] = $userfind['User']['login_status'];
            
            echo json_encode(array("msg_type"=>1,"msg"=>'Your account activated successfully.','userdetails'=>$userdetails));  
        }else{
            echo json_encode(array("msg_type"=>0,"msg"=>'Username already exist. Please use different username.'));  
        }
        exit;
    }
    
    public function updatesetting(){
        
        $id=  ($_REQUEST['id']);
        $name=  $_REQUEST['name'];
        $company_name=  $_REQUEST['company_name'];
        $website=  $_REQUEST['website'];
        $timezone=  $_REQUEST['timezone'];
        $options = array('conditions' => array('User.id' => $id));
        $user=$this->User->find('first', $options);
        //print_r($user);exit;
        if(!empty($user)){
            $user['User']['id']=$id;
            $user['User']['name']=$name;
            $user['User']['company_name']=$company_name;
            $user['User']['timezone']=$timezone;
            $user['User']['website']=$website;
            if($this->User->save($user))
            {
                $optionuserfind = array('conditions' => array('User.id' => $id));
                $userfind=$this->User->find('first', $optionuserfind);

                $userdetails['name'] = $userfind['User']['name'];
                $userdetails['image'] = $userfind['User']['image'];
                $userdetails['id'] = $userfind['User']['id'];
                $userdetails['isloggedin'] = true;
                $userdetails['website'] = $userfind['User']['website'];
                $userdetails['company_name']=$userfind['User']['company_name'];
                $userdetails['timezone'] = $userfind['User']['timezone'];
                $userdetails['login_status'] = $userfind['User']['login_status'];

                echo json_encode(array("msg_type"=>1,"msg"=>'Your personal setting updated successfully.','userdetails'=>$userdetails)); 
            }
            else{
                echo json_encode(array("msg_type"=>0,"msg"=>'Sorry! Couldnot update your personal setting.'));
            }
            
              
        }else{
            echo json_encode(array("msg_type"=>0,"msg"=>'Sorry! Not valid user. Please try again.'));  
        }
        exit;
    }
    
    public function getalltimezones($type=null)
    {
        $list = DateTimeZone::listAbbreviations();
        $idents = DateTimeZone::listIdentifiers();
        $data = $offset = $added = array();
        foreach ($list as $abbr => $info) {
                foreach ($info as $zone) {
                    if ( ! empty($zone['timezone_id'])
                        AND
                        ! in_array($zone['timezone_id'], $added)
                        AND 
                          in_array($zone['timezone_id'], $idents)) {
                        $z = new DateTimeZone($zone['timezone_id']);
                        $c = new DateTime(null, $z);
                        $zone['time'] = $c->format('H:i a');
                        $data[] = $zone;
                        $offset[] = $z->getOffset($c);
                        $added[] = $zone['timezone_id'];
                    }
                }
         }
         array_multisort($offset, SORT_ASC, $data);
         $options = array();
         foreach ($data as $key => $row) {
                $options[$row['timezone_id']] = '('. $this->formatOffset($row['offset']) .')'. ' ' . $row['timezone_id'];
         }
         
         if($type!='')
         {
           return $options;
         }
         else
         {
           echo json_encode($options);
           exit;
         }
    }
    
    public function getactivationuserdetails(){
        if ( base64_encode(base64_decode($_REQUEST['id'])) === $_REQUEST['id']){
         $id=base64_decode($_REQUEST['id']);
         $options = array('conditions' => array('User.id' => $id));
         $user=$this->User->find('first', $options);
        if($_REQUEST['type']=='activation'){
           if(isset($user['User']['is_active']) && $user['User']['is_active']==1){
               $array=array("msg_type"=>2,'msg'=>'This account is already activated. You can login to your account.');  
                echo json_encode($array);exit;
           } 
        }
        $alltimezones=$this->getalltimezones('activation');
        if(!empty($user)){
        $array=array("msg_type"=>1,"id"=>$user['User']['id'],"email"=>$user['User']['email'],"name"=>$user['User']['name'],'timezone'=>$alltimezones);
        }else{
           $array=array("msg_type"=>0);  
        }
        
        }else {
        $array=array("msg_type"=>0);  
        }
        echo json_encode($array);
        exit;
    }
    
    
    
    public function timezoneset($timezone=null){
       $this->Session->write('clienttimezone', $_REQUEST['timezones']);
       exit;
    }
    public function admin_logout() {
      $this->Session->delete('userid');
      $this->Session->delete('email');
      $this->redirect('index');
    }
    public function admin_index() {
       #$this->render(false);
        $this->layout=false;
        #$this->autoRender = false ;
        #$this->User->recursive = 0;
        #$this->set('users', $this->Paginator->paginate());
    }
    
  //start sayani 
   
   public function admin_list() {		
		$title_for_layout = 'User List';
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->User->recursive = 0;
                $this->Paginator->settings = array(
                 'conditions' =>array(
                 	'User.is_active' => 1
                 ),
                 'limit' =>15,
                 'order' => array(
                    'User.id' => 'desc'
                 )
               );
		$this->set('users', $this->Paginator->paginate('User'));
		$this->set(compact('title_for_layout'));
	}
	
	public function admin_subcategories($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
                $cat_id = $id;
		$title_for_layout = 'Sub User List';
		$options = array('conditions' => array('User.id' => $id));
		$categoryname = $this->User->find('list', $options);
		if($categoryname){
			$categoryname = $categoryname[$id];
		} else {
			$categoryname = '';
		}
		$this->User->recursive = 0;
                $this->Paginator->settings = array(
			        'limit' =>15,
			        'order' => array(
				        'User.id' => 'desc'
			        )
                );
		$this->set('users', $this->Paginator->paginate('User', array('User.parent_id' => $id)));
		$this->set(compact('title_for_layout','categoryname','cat_id'));
	}

	public function admin_edit($id=null)
	{
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid User'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
                              $this->Session->setFlash('The user has been saved.', 'default', array('class' => 'success'));
                              return $this->redirect(array('action' => 'list'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}	
	
	public function admin_delete($id = null) {
		$userid = $this->Session->read('userid');
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		/*$options1 = array('conditions' => array('User.parent_id' => $id));
		$subcat = $this->User->find('list', $options1);
		if($subcat){
			foreach($subcat as $k1=>$v1){
				$this->User->delete($k1);
			}
		}
		
		$options2 = array('conditions' => array('User.id' => $id));
		$maincat = $this->User->find('first', $options2);
		if($maincat)
		{
		   $uploadFolder = "upload/user_images";
		   $uploadPath = WWW_ROOT . $uploadFolder;
		   $imageName=$maincat['User']['image'];
		   $full_image_path = $uploadPath . '/' . $imageName;
		   //unlink($full_image_path);
		}*/
	
		if ($this->User->delete($id)) {
			$this->Session->setFlash('The user has been deleted.', 'default', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'list'));
	}
	
	//end sayani  
	
	public function admin_dashboard() {
        $userid = $this->Session->read('userid');
	if(!isset($userid) && $userid=='')
	{
		$this->redirect('/controlpanel');
	}
    }  

     public function admin_home() {
     $this->loadModel('User');
     $userid = $this->Session->read('userid');	
     if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}
		
	  $options4 = array('conditions' => array('User.is_logged_in' => 1,'User.is_active' => 1));
        $log_user=$this->User->find('count', $options4);
       $options5 = array('conditions' => array('User.is_logged_in' => 0,'User.is_active' => 1));
       $notlog_user=$this->User->find('count', $options5);
       
       $options1 = array('conditions' => array('User.is_active' => 1));
       $all_user=$this->User->find('all',$options1);
       $options2 = array('conditions' => array('User.is_active' => 1));
       $count1 = $this->User->find('count',$options2);
//echo '<pre>';       print_r($count1);
//exit;     
       $this->set(compact('all_user','count1','log_user','notlog_user'));
       //$this->set(compact('title_for_layout'));
       //return $this->redirect(array('action' => 'home'));
    }
    
    public function admin_count() {
		$this->loadModel('User');
		$userid = $this->Session->read('userid');	
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}

		
		$options2 = array('conditions' => array('User.is_active' => 1));
		$count1 = $this->User->find('count',$options2);
		echo $count1;
		exit;
    }
    
    public function admin_chart_online() {
		$this->loadModel('User');
		$userid = $this->Session->read('userid');	
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}

		$options4 = array('conditions' => array('User.is_logged_in' => 1,'User.is_active' => 1));
		$log_user=$this->User->find('count', $options4);
		$options5 = array('conditions' => array('User.is_logged_in' => 0,'User.is_active' => 1));
		$notlog_user=$this->User->find('count', $options5);

		$options2 = array('conditions' => array('User.is_active' => 1));
		$count1 = $this->User->find('count',$options2);

		$total = $count1;
		$logUser = $log_user;
		$logoutUser = $notlog_user;
		$percntLogUser = ($logUser/$total)*100;
		$percntLogoutUser = ($logoutUser/$total)*100;
		echo round($percntLogUser);
       exit;
    }
    
    public function admin_chart_ofline() {
		$this->loadModel('User');
		$userid = $this->Session->read('userid');	
		if(!isset($userid) && $userid=='')
		{
			$this->redirect('/controlpanel');
		}

		$options4 = array('conditions' => array('User.is_logged_in' => 1,'User.is_active' => 1));
		$log_user=$this->User->find('count', $options4);
		$options5 = array('conditions' => array('User.is_logged_in' => 0,'User.is_active' => 1));
		$notlog_user=$this->User->find('count', $options5);

		$options2 = array('conditions' => array('User.is_active' => 1));
		$count1 = $this->User->find('count',$options2);

		$total = $count1;
		$logUser = $log_user;
		$logoutUser = $notlog_user;
		$percntLogUser = ($logUser/$total)*100;
		$percntLogoutUser = ($logoutUser/$total)*100;
		echo round($percntLogoutUser);
       exit;
    }
    
      public function admin_extra() {
    	$this->loadModel('User');
    	$options = array('conditions' => array('User.is_active' => 1));
    	$all_user=$this->User->find('all',$options);
    	foreach($all_user as $user)
    	{
    	?>
    	
    	
    	<div class="msg-time-chat">
                                    
                                     <div class="message-body msg-in">
                                         <span class="arrow"></span>
                                         <div class="text">
                                             <p class="attribution"><a href="#"><?php if(isset($user['User']['name']) && $user['User']['name']!='') {echo $user['User']['name'];} ?></a> &nbsp;&nbsp;<b><?php if(isset($user['User']['email']) && $user['User']['email']!='') {echo $user['User']['email'];} ?></b></p>
                                            
                                             <p><?php if(isset($user['User']['is_logged_in'])) {if($user['User']['is_logged_in']=='1') { echo 'Logged In';} else { echo 'Offline';}}?></p>
                                             <p><?php 
                                             if(isset($user['User']['is_logged_in']) && isset($user['User']['login_status'])) 
                                             {
                                               if($user['User']['is_logged_in']=='1') 
                                                { 
                                                 if(isset($user['User']['login_status'])) 
                                                   {
                                                     if($user['User']['login_status']=='1') 
                                                        { ?>
                                                         <img src="<?php echo $this->webroot;?>img/online.png" title="online" width="10" height="12">&nbsp;INSTANT 
                                                        <?php } 
                                                         if($user['User']['login_status']=='2') 
                                                         {?>
                                                           <img src="<?php echo $this->webroot;?>img/online.png" title="online" width="10" height="12">&nbsp;ONLINE 
                                                         <?php } 
                                                           if($user['User']['login_status']=='3') 
                                                             { ?>
                                                             <img src="<?php echo $this->webroot;?>img/away.png" title="online" width="10" height="12">&nbsp;AWAY
                                                              
                                                            <?php } 
                                                               if($user['User']['login_status']=='4')
                                                               {?> 
                                                               <img src="<?php echo $this->webroot;?>img/offline.png" title="offline" width="10" height="12">&nbsp;OFFLINE
                                                               <?php }
                                                               }
                                                              }
                                                            }
                                                          ?></p>
                                             
                                             <p><b>Last Login:&nbsp;<?php if(isset($user['User']['last_login'])) { echo $user['User']['last_login'];}?></b></p>
                                         </div>
                                         
                                     </div>
                                     
                                 </div>
    	
    	
    	
    	<?php
    	}
    	exit;
    	}
    
    public function admin_login() 
    {
        $this->loadModel('Admin');
        if ($this->request->is('post')) 
        {
            $options = array('conditions' => array('Admin.username' => $this->request->data['User']['email'], 'Admin.password' => md5($this->request->data['User']['password'])));
            $loginuser = $this->Admin->find('first', $options);
            if (!$loginuser) {
                $this->Session->setFlash(__('Invalid username or password, try again', 'default', array('class' => 'error')));
                return $this->redirect(array('action' => 'index'));
            } else {

                $this->Session->write('userid', $loginuser['Admin']['id']);
                $this->Session->write('username', $loginuser['Admin']['email']);

                $this->Session->setFlash('You have been successfully logged in', 'default', array('class' => 'success'));
                return $this->redirect(array('action' => 'dashboard'));
            }
        }
    }
    
    public function loginadmin() 
    {
      $this->loadModel('Admin');
      if(isset($_REQUEST['username']) && $_REQUEST['username']!='' && $_REQUEST['password']!='')
      {
           $options = array('conditions' => array('Admin.password'=>md5($_REQUEST['password']),'Admin.username'=>$_REQUEST['username']));
           $user=$this->Admin->find('first', $options);
           if(!empty($user)){
             $userdetails['username'] = $user['Admin']['username'];
             $userdetails['id'] = $user['Admin']['id'];
             $userdetails['isloggedin'] = true;
             echo json_encode(array("msg_type"=>1,"msg"=>'You have successfully logged in.','userdetails'=>$userdetails)); 
           }else{
             echo json_encode(array("msg_type"=>0,"msg"=>'Invalid login details.'));
           }
      }
      else
      {
        echo json_encode(array("msg_type"=>0,"msg"=>'Please provide your login details.'));
      }
        exit;
    }
    
   public function formatOffset($offset) {
        $hours = $offset / 3600;
        $remainder = $offset % 3600;
        $sign = $hours > 0 ? '+' : '-';
        $hour = (int) abs($hours);
        $minutes = (int) abs($remainder / 60);

        if ($hour == 0 AND $minutes == 0) {
            $sign = ' ';
        }
        return 'GMT' . $sign . str_pad($hour, 2, '0', STR_PAD_LEFT) 
                .':'. str_pad($minutes,2, '0');
   }
   
   public function mentorsignupstep1() 
   {
      $this->loadModel('Expertise');
      if(!isset($_REQUEST['headline']) || $_REQUEST['headline']=='')
      {
         echo json_encode(array("msg_type"=>0,"msg"=>'Please add your profile headline.'));
      }
      else if(!isset($_REQUEST['about']) || $_REQUEST['about']=='')
      {
         echo json_encode(array("msg_type"=>0,"msg"=>'Please tell something about yourself.'));
      }
      else if(!isset($_REQUEST['price']) || $_REQUEST['price']=='')
      {
         echo json_encode(array("msg_type"=>0,"msg"=>'Please enter price.'));
      }
      else if(!isset($_REQUEST['selectedcats']) || $_REQUEST['selectedcats']=='')
      {
         echo json_encode(array("msg_type"=>0,"msg"=>'Please choose your expertise.'));
      }
      else
      {
             $userid=$_REQUEST['userid'];
             $optionsexpertise = array('conditions' => array('Expertise.userid' => base64_decode($userid)));
	     $expertiselist = $this->Expertise->find('list', $optionsexpertise);
	     if($expertiselist){
	        foreach($expertiselist as $k1=>$v1){
		        $this->Expertise->delete($k1);
	        }
	     }
             
             $selectedcats=trim($_REQUEST['selectedcats'],',');
             $selectedcats_exp=explode(',',$selectedcats);
             $about=$_REQUEST['about'];
             $headline=$_REQUEST['headline'];
             $price=$_REQUEST['price'];
             $twitter=$_REQUEST['twitter'];
             $stackoverflow=$_REQUEST['stackoverflow'];
             $chosencats='';
             if(!empty($selectedcats_exp))
             {
               foreach($selectedcats_exp as $val)
               {
                 $expertise['Expertise']['userid']=base64_decode($userid);
                 $expertise['Expertise']['name']=$val;
                 $expertise['Expertise']['related_tags']='';
                 $expertise['Expertise']['experience_years']='';
                 $expertise['Expertise']['domain_details']='';
                 $expertise['Expertise']['donain_rating']='';
                 $this->Expertise->create();
                 $this->Expertise->save($expertise);  
               }
             }
             if(!empty($_REQUEST['chosencats']))
             {
               foreach($_REQUEST['chosencats'] as $v)
               {
                 $expertisetag['Expertise']['userid']=base64_decode($userid);
                 $expertisetag['Expertise']['name']=$v['text'];
                 $expertisetag['Expertise']['related_tags']='';
                 $expertisetag['Expertise']['experience_years']='';
                 $expertisetag['Expertise']['domain_details']='';
                 $expertisetag['Expertise']['donain_rating']='';
                 $this->Expertise->create();
                 $this->Expertise->save($expertisetag);  
                 $chosencats.=$v['text'].',';
               }
               $chosencats=trim($chosencats,',');
             }
             $finalcats=$chosencats.','.$selectedcats;
             $user['User']['id']=base64_decode($userid);
             $user['User']['expertise']=trim($finalcats,',');
             $user['User']['headline']=$headline;
             $user['User']['about']=$about;
             $user['User']['price']=$price;
             $user['User']['twitter']=$twitter;
             $user['User']['stackoverflow']=$stackoverflow;
             $this->User->save($user);  
             echo json_encode(array("msg_type"=>1,"msg"=>'Your information is successfully saved.'));
      }
      exit;
   }
   
   public function mentorsignupstep3()
   {
     $this->loadModel('Expertise');
     $chosencats='';
     $userid=$_REQUEST['userid'];
     $options = array('conditions' => array('Expertise.userid' => base64_decode($userid)));
     $expertise = $this->Expertise->find('list', $options);
      if($expertise){
	foreach($expertise as $k1=>$v1){
          $this->Expertise->delete($k1);
	}
      }
      if(!empty($_REQUEST['tagdetails']))
      {
       foreach($_REQUEST['tagdetails'] as $v2)
       {
         $chosenrelatedcats='';
         $expertisedetails=json_decode($v2);
         if(!empty($expertisedetails->expertise_related_tags))
         {
           foreach($expertisedetails->expertise_related_tags as $k3=>$v3)
           {
             $chosenrelatedcats.=$v3->text.',';
           }
         }
         $expertisetag['Expertise']['userid']=base64_decode($userid);
         $expertisetag['Expertise']['name']=$expertisedetails->tagname;
         $expertisetag['Expertise']['related_tags']=trim($chosenrelatedcats,',');
         $expertisetag['Expertise']['experience_years']=$expertisedetails->expertise_experience;
         $expertisetag['Expertise']['domain_details']=$expertisedetails->expertise_moredetails;
         $expertisetag['Expertise']['donain_rating']=$expertisedetails->expertise_rating;
         $this->Expertise->create();
         $this->Expertise->save($expertisetag);  
         $chosencats.=$expertisedetails->tagname.',';
       }
       $user['User']['id']=base64_decode($userid);
       $user['User']['expertise']=trim($chosencats,',');
       $user['User']['is_profile_completed']=1;
       $this->User->save($user);  
       echo json_encode(array("msg_type"=>1,"msg"=>'Your information is successfully saved.'));
     }
     else
     {
       echo json_encode(array("msg_type"=>0,"msg"=>'Please choose atleast one expertise.'));
     }
     exit;
   }
   
   public function updateExpert()
   {
     $this->loadModel('Expertise');
     $chosencats='';
     $userid=$_REQUEST['userid'];
     
      if(!empty($_REQUEST['tagdetails']))
      {
		$options = array('conditions' => array('Expertise.userid' => ($userid)));
		$expertise = $this->Expertise->find('list', $options);
		if($expertise){
			foreach($expertise as $k1=>$v1){
				$this->Expertise->delete($k1);
			}
		}
       
       foreach($_REQUEST['tagdetails'] as $v2)
       {
         $chosenrelatedcats='';
         $expertisedetails=json_decode($v2);
         /*if(!empty($expertisedetails->expertise_related_tags))
         {
           foreach($expertisedetails->expertise_related_tags as $k3=>$v3)
           {
             $chosenrelatedcats.=$v3->text.',';
           }
         }*/
         $expertisetag['Expertise']['userid']=($userid);
         $expertisetag['Expertise']['name']=$expertisedetails->tagname;
         $expertisetag['Expertise']['related_tags']=$expertisedetails->expertise_related_tags;//trim($chosenrelatedcats,',');
         $expertisetag['Expertise']['experience_years']=$expertisedetails->expertise_experience;
         $expertisetag['Expertise']['domain_details']=$expertisedetails->expertise_moredetails;
         $expertisetag['Expertise']['donain_rating']=$expertisedetails->expertise_rating;
         $this->Expertise->create();
         $this->Expertise->save($expertisetag);  
         $chosencats.=$expertisedetails->tagname.',';
       }
       /*$user['User']['id']=($userid);
       $user['User']['expertise']=trim($chosencats,',');
       $user['User']['is_profile_completed']=1;
       $this->User->save($user);  */
       echo json_encode(array("msg_type"=>1,"msg"=>'Your information is successfully saved.'));
     }
     else
     {
       echo json_encode(array("msg_type"=>0,"msg"=>'Please choose atleast one expertise.'));
     }
     exit;
   }
   
   public function getexpertisedetails() 
   {
     $this->loadModel('Expertise');
     $userid=$_REQUEST['userid'];
     $optionsexpertise = array('conditions' => array('Expertise.userid' => base64_decode($userid)));
     $expertiselist = $this->Expertise->find('all', $optionsexpertise);
     $expertisearr=array();
     if(!empty($expertiselist))
     {
       foreach($expertiselist as $v)
       {
          $expertisearr[]=array('id'=>$v['Expertise']['id'],'name'=>$v['Expertise']['name'],'related_tags'=>$v['Expertise']['related_tags'],'experience_years'=>$v['Expertise']['experience_years'],'domain_details'=>$v['Expertise']['domain_details'],'donain_rating'=>$v['Expertise']['donain_rating']);
       }
     }
      echo json_encode($expertisearr);
      exit;
   }
   
   public function getexpert() 
   {
     $this->loadModel('Expertise');
     $userid=$_REQUEST['userid'];
     $optionsexpertise = array('conditions' => array('Expertise.userid' => ($userid)));
     $expertiselist = $this->Expertise->find('all', $optionsexpertise);
     $expertisearr=array();
     if(!empty($expertiselist))
     {
       foreach($expertiselist as $v)
       {
          $expertisearr[]=array('id'=>$v['Expertise']['id'],'name'=>$v['Expertise']['name'],'related_tags'=>$v['Expertise']['related_tags'],'experience_years'=>$v['Expertise']['experience_years'],'domain_details'=>$v['Expertise']['domain_details'],'donain_rating'=>$v['Expertise']['donain_rating']);
       }
     }
      echo json_encode($expertisearr);
      exit;
     
     /*$this->loadModel('Expertise');
     $userid=$_REQUEST['userid'];
     $optionsexpertise = array('conditions' => array('Expertise.userid' => ($userid)));
     $expertiselist = $this->Expertise->find('all', $optionsexpertise);
     $expertisearr=array();
     $req_help_arr=array();
     if(!empty($expertiselist))
     {
       foreach($expertiselist as $v)
       {
          if(isset($v['Expertise']['related_tags']) && $v['Expertise']['related_tags']!='')
		  {
		    $required_categories_exp=explode(',',$v['Expertise']['related_tags']);
		    foreach($required_categories_exp as $exr)
		    {
		       $req_help_arr[]=array('text'=>$exr);
		    }
		  } 
          $expertisearr[]=array('id'=>$v['Expertise']['id'],'name'=>$v['Expertise']['name'],'tags'=>$req_help_arr,'experience_years'=>$v['Expertise']['experience_years'],'domain_details'=>$v['Expertise']['domain_details'],'donain_rating'=>$v['Expertise']['donain_rating']);
       }
     }
      echo json_encode($expertisearr);
      exit;*/
   }
   
   public function mentorsignupstep2() 
   {
     $userid=$_REQUEST['userid'];
     $availability=$_REQUEST['availablity'];
     $timezone=$_REQUEST['timezone'];
     $company=$_REQUEST['company'];
     $job_title=$_REQUEST['job_title'];
     $why_mentor=$_REQUEST['why_mentor'];
     $experience_details=$_REQUEST['experience_details'];
     $video_url=$_REQUEST['video_url'];
     $website_url=$_REQUEST['website_url'];
     
     $user['User']['id']=base64_decode($userid);
     $user['User']['availability']=$availability;
     $user['User']['timezone']=$timezone;
     $user['User']['company']=$company;
     $user['User']['job_title']=$job_title;
     $user['User']['why_mentor']=$why_mentor;
     $user['User']['experience_details']=$experience_details;
     $user['User']['video_url']=$video_url;
     $user['User']['website_url']=$website_url;
     $this->User->save($user);  
     echo json_encode(array("msg_type"=>1,"msg"=>'Your information is successfully saved.'));
     exit;
   }
   
   public function updatementorloginstatus() 
   {
     $userid=$_REQUEST['userid'];
     $status=$_REQUEST['status'];
     $user['User']['id']=$userid;
     $user['User']['login_status']=intval($status);
     $this->User->save($user); 
     $options = array('conditions' => array('User.id' => $userid));
     $user=$this->User->find('first', $options);
     if(!empty($user)){
         $userdetails['email'] = $user['User']['email'];
         $userdetails['username'] = $user['User']['username'];
         $userdetails['name'] = $user['User']['name'];
         $userdetails['image'] = '';
         $userdetails['id'] = $user['User']['id'];
         $userdetails['isloggedin'] = true;
         $userdetails['role'] = $user['User']['role'];
	 $userdetails['login_status'] = $user['User']['login_status'];
     }
     echo json_encode(array("msg_type"=>1,"msg"=>'Your login status updated successfully.',"userdetails"=>$userdetails));
     exit;
   }
   
   public function updateuserrequirementcats()
   {
     $userid=$_REQUEST['userid'];
     $selectedcats=$_REQUEST['selectedcats'];
     $chosencats='';
     if(!empty($_REQUEST['chosencats']))
     {
       foreach($_REQUEST['chosencats'] as $v)
       {
         $chosencats.=$v['text'].',';
       }
       $chosencats=trim($chosencats,',');
     }
     $finalcats=$chosencats.$selectedcats;
     $user['User']['id']=$userid;
     $user['User']['required_categories']=trim($finalcats,',');
     $user['User']['get_help_completed']=1;
     $this->User->save($user);  
     echo json_encode(array("msg_type"=>1));
     exit;
   }
   
   public function updateexpertlabel()
   {
     $userid=$_REQUEST['userid'];
     $lavel=$_REQUEST['lavel'];
     $user['User']['id']=$userid;
     $user['User']['expert_level']=$lavel;
     $user['User']['get_help_completed']=1;
     $this->User->save($user);  
     echo json_encode(array("msg_type"=>1));
     exit;
   }
   
   public function getallmentors()
   {
     $this->loadModel('Expertise');
     $expertisearr=array();
     $experts=array();
     if(isset($_REQUEST['userid']) && $_REQUEST['userid']!='')
     {
      $userid=$_REQUEST['userid'];
      $optionsuser = array('conditions' => array('User.id' => $userid));
      $user=$this->User->find('first', $optionsuser);
      if(!empty($user))
      {
       if(isset($user['User']['get_help_completed']) && $user['User']['get_help_completed']==1)
       {
         $user_required_exp=explode(',',$user['User']['required_categories']);
         foreach($user_required_exp as $v)
         {
           $optionsexpertise = array('conditions' => array('Expertise.name' => $v,'Expertise.userid'=>array('$ne' => $userid)));
           $expertises=$this->Expertise->find('all', $optionsexpertise);
           if(!empty($expertises))
           {
             foreach($expertises as $ev)
             {
               if(in_array($ev['Expertise']['userid'],$expertisearr))
               {
               }
               else
               {
                 $expertisearr[]=$ev['Expertise']['userid'];
               }
             }
           }
           else
           {
             
           }
         }
         if(!empty($expertisearr))
         {
            $optionsmentor = array('conditions' => array('User.role' => 2,'User.is_active' => 1,'User.is_profile_completed' => 1,'User.id'=>array('$in'=>$expertisearr),'User.id'=>array('$ne' => $userid)),'order'=>array('is_logged_in'=>-1,'login_status'=>1));
            $montors=$this->User->find('all', $optionsmentor);
         }
         else
         {
            $optionsmentor = array('conditions' => array('User.role' => 2,'User.is_active' => 1,'User.is_profile_completed' => 1,'User.id'=>array('$ne' => $userid)),'order'=>array('is_logged_in'=>-1,'login_status'=>1));
            $montors=$this->User->find('all', $optionsmentor);
         }
       }
       else
       {
         $optionsmentor = array('conditions' => array('User.role' => 2,'User.is_active' => 1,'User.is_profile_completed' => 1,'User.id'=>array('$ne' => $userid)),'order'=>array('is_logged_in'=>-1,'login_status'=>1));
         $montors=$this->User->find('all', $optionsmentor);
       }
      }
     }
     else
     {
       $optionsmentor = array('conditions' => array('User.role' => 2,'User.is_active' => 1,'User.is_profile_completed' => 1),'order'=>array('is_logged_in'=>-1,'login_status'=>1));
       $montors=$this->User->find('all', $optionsmentor);
     }
     if(!empty($montors))
     {
       foreach($montors as $mv)
       {
          $expertise_arr=array();
          if(isset($mv['User']['expertise']) && $mv['User']['expertise']!='')
          {
            $expert_exp=explode(',',$mv['User']['expertise']);
            foreach($expert_exp as $exv)
            {
              $expertise_arr[]=array('tag'=>$exv);
            }
          }
          if(strlen($mv['User']['about']) > 200)
          {
            $about=substr($mv['User']['about'],0,195).'...';
          }
          else
          {
           $about=$mv['User']['about'];
          }
          if(strlen($mv['User']['about']) > 80)
          {
            $aboutsmall=substr($mv['User']['about'],0,75).'...';
          }
          else
          {
           $aboutsmall=$mv['User']['about'];
          }
          if(strlen($mv['User']['headline']) > 25)
          {
            $headlinesmall=substr($mv['User']['headline'],0,22).'...';
          }
          else
          {
           $headlinesmall=$mv['User']['headline'];
          }
          if($mv['User']['is_logged_in']==1)
          {
            /*if($mv['User']['login_status']==1)
            {
              $login_status=1;
            }
            else if($mv['User']['login_status']==2)
            {
              $login_status=2;
            }            
            else
            {
              $login_status=0;
            }*/
            $login_status = $mv['User']['login_status'];
          }
          else
          {
           $login_status=0;
          }
          $experts[]=array('id'=>$mv['User']['id'],'name'=>$mv['User']['name'],'headline'=>$mv['User']['headline'],'headlinesmall'=>$headlinesmall,'about'=>$about,'image'=>$mv['User']['image'],'expertises'=>$expertise_arr,'login_status'=>$login_status,'priceperhour'=>($mv['User']['price']*4),'aboutsmall'=>$aboutsmall);
       }
     }
     if(!empty($experts))
     {
       echo json_encode(array("is_experts_available"=>1,"experts"=>$experts));
     }
     else
     {
       echo json_encode(array("is_experts_available"=>0,"experts"=>$experts));
     }
     exit;
   }
   
   public function getdetails()
   {
     $userid=$_REQUEST['userid'];
     $optionsuser = array('conditions' => array('User.id' => $userid));
     $user=$this->User->find('first', $optionsuser);
     $userdetails=array();
     $helprequired=array();
     $why_to_like=array();
     $req_help_arr=array();
     $whytolike=$this->whytolike();
     if(!empty($user))
     {
		$fbid = '';
		$lnid = '';
		$gid = '';
		$imgtype = '1';
		if(isset($user['User']['fbid']) && $user['User']['fbid']!='')
		{
			$fbid=$user['User']['fbid'];
			$imgtype = '2';
		}else if(isset($user['User']['lnid']) && $user['User']['lnid']!='')
		{
			$lnid=$user['User']['lnid'];
			$imgtype = '3';
		}else if(isset($user['User']['gid']) && $user['User']['gid']!='')
		{
			$gid=$user['User']['gid'];
			$imgtype = '4';
		}
       if(isset($user['User']['get_help_completed']) && $user['User']['get_help_completed']==1)
       {
         $user_required_exp=explode(',',$user['User']['required_categories']);
         foreach($user_required_exp as $exv)
         {
            $helprequired[]=array('tag'=>$exv);
         }
       }
       if(isset($user['User']['why_to_like']) && $user['User']['why_to_like']!='')
       {
         $why_to_like_exp=explode(',',$user['User']['why_to_like']);
         foreach($why_to_like_exp as $exw)
         {
            $why_to_like[]=array('whytolike'=>$whytolike["whytolike"][$exw]);
         }
       } 
       if(isset($user['User']['required_categories']) && $user['User']['required_categories']!='')
       {
         $required_categories_exp=explode(',',$user['User']['required_categories']);
         foreach($required_categories_exp as $exr)
         {
            $req_help_arr[]=array('text'=>$exr);
         }
       }   
       $alltimezones=$this->getalltimezones('activation');
       $userdetails=array('id'=>$user['User']['id'],'name'=>$user['User']['name'],'company_name'=>$user['User']['company_name'],'website'=>$user['User']['website'],'timezone'=>$user['User']['timezone'],'expert_level'=>ucwords($user['User']['expert_level']),'image'=>$user['User']['image'],'helprequired'=>$helprequired,'why_to_like'=>$why_to_like,'alltimezones'=>$alltimezones,'tags'=>$req_help_arr,'whytolike'=>$whytolike,'fbid'=>$fbid,'lnid'=>$lnid,'gid'=>$gid,'imgtype'=>$imgtype);
     }
     if(!empty($userdetails))
     {
       echo json_encode(array("is_experts_available"=>1,"userdetails"=>$userdetails));
     }
     else
     {
       echo json_encode(array("is_experts_available"=>0,"userdetails"=>$userdetails));
     }
     exit;
   }
   
   
   public function getdetailsByUserName()
   {
     $userid=$_REQUEST['username'];
     $this->loadModel('Expertise');
     $this->loadModel('Tip');
     $this->User->recursive = 2;
     $optionsuser = array('conditions' => array('User.username' => $userid));
     $user=$this->User->find('first', $optionsuser);
     
     $option = array('conditions' => array('Expertise.userid' => $user['User']['id']));
     $expertise=$this->Expertise->find('all', $option);
     
     $option = array('conditions' => array('Tip.user_id' => $user['User']['id']));
     $tips=$this->Tip->find('all', $option);
     
     $userdetails=array();
     $userExp=array();
     $userTip=array();
     $helprequired=array();
     $why_to_like=array();
     $req_help_arr=array();
     $fbid = '';
     $whytolike=$this->whytolike();
     if(isset($user['User']['fbid']) && $user['User']['fbid']!='')
     {
     	$fbid=$user['User']['fbid'];
     }
     $fbid = '';
			$lnid = '';
			$gid = '';
			$imgtype = '1';
			if(isset($user['User']['fbid']) && $user['User']['fbid']!='')
			{
				$fbid=$user['User']['fbid'];
				$imgtype = '2';
			}else if(isset($user['User']['lnid']) && $user['User']['lnid']!='')
			{
				$lnid=$user['User']['lnid'];
				$imgtype = '3';
			}else if(isset($user['User']['gid']) && $user['User']['gid']!='')
			{
				$gid=$user['User']['gid'];
				$imgtype = '4';
			}
     if(!empty($user))
     {
       if(isset($expertise) && !empty($expertise))
       {
         foreach($expertise as $exp)
         {
            $userExp[]=array('name'=>$exp['Expertise']['name'],'experience_years'=>$exp['Expertise']['experience_years'],'domain_details'=>$exp['Expertise']['domain_details']);
         }
       }
       if(isset($tips) && !empty($tips))
       {
         foreach($tips as $tip)
         {
            $userTip[]=array('title'=>$tip['Tip']['title'],'description'=>$tip['Tip']['description'],'tags'=>$tip['Tip']['tags'],'id'=>$tip['Tip']['id']);
         }
       }
       if(isset($user['User']['get_help_completed']) && $user['User']['get_help_completed']==1)
       {
         $user_required_exp=explode(',',$user['User']['required_categories']);
         foreach($user_required_exp as $exv)
         {
            $helprequired[]=array('tag'=>$exv);
         }
       }
       if(isset($user['User']['why_to_like']) && $user['User']['why_to_like']!='')
       {
         $why_to_like_exp=explode(',',$user['User']['why_to_like']);
         foreach($why_to_like_exp as $exw)
         {
            $why_to_like[]=array('whytolike'=>$whytolike["whytolike"][$exw]);
         }
       } 
       if(isset($user['User']['required_categories']) && $user['User']['required_categories']!='')
       {
         $required_categories_exp=explode(',',$user['User']['required_categories']);
         foreach($required_categories_exp as $exr)
         {
            $req_help_arr[]=array('text'=>$exr);
         }
       }   
       $alltimezones=$this->getalltimezones('activation');
       $userdetails=array('id'=>$user['User']['id'],'name'=>$user['User']['name'],'company_name'=>$user['User']['company_name'],'website'=>$user['User']['website'],'timezone'=>$user['User']['timezone'],'expert_level'=>ucwords($user['User']['expert_level']),'image'=>$user['User']['image'],'helprequired'=>$helprequired,'why_to_like'=>$why_to_like,'alltimezones'=>$alltimezones,'tags'=>$req_help_arr,'whytolike'=>$whytolike,'fbid'=>$fbid,'lnid'=>$lnid,'gid'=>$gid,'imgtype'=>$imgtype);
     }
     if(!empty($userdetails))
     {
       echo json_encode(array("is_experts_available"=>1,"userdetails"=>$userdetails,"expertise"=>$userExp,"tips"=>$userTip));
     }
     else
     {
       echo json_encode(array("is_experts_available"=>0,"userdetails"=>$userdetails));
     }
     exit;
   }
   
   public function getdetailsById()
   {
     $userid=$_REQUEST['username'];
     $this->loadModel('Expertise');
     $this->loadModel('Tip');
     $this->User->recursive = 2;
     $optionsuser = array('conditions' => array('User.id' => $userid));
     $user=$this->User->find('first', $optionsuser);
     
     $option = array('conditions' => array('Expertise.userid' => $user['User']['id']));
     $expertise=$this->Expertise->find('all', $option);
     
     $option = array('conditions' => array('Tip.user_id' => $user['User']['id']));
     $tips=$this->Tip->find('all', $option);
     
     $userdetails=array();
     $userExp=array();
     $userTip=array();
     $helprequired=array();
     $why_to_like=array();
     $req_help_arr=array();
     $fbid = '';
     $whytolike=$this->whytolike();
     if(isset($user['User']['fbid']) && $user['User']['fbid']!='')
     {
     	$fbid=$user['User']['fbid'];
     }
     $fbid = '';
			$lnid = '';
			$gid = '';
			$imgtype = '1';
			if(isset($user['User']['fbid']) && $user['User']['fbid']!='')
			{
				$fbid=$user['User']['fbid'];
				$imgtype = '2';
			}else if(isset($user['User']['lnid']) && $user['User']['lnid']!='')
			{
				$lnid=$user['User']['lnid'];
				$imgtype = '3';
			}else if(isset($user['User']['gid']) && $user['User']['gid']!='')
			{
				$gid=$user['User']['gid'];
				$imgtype = '4';
			}
     if(!empty($user))
     {
       if(isset($expertise) && !empty($expertise))
       {
         foreach($expertise as $exp)
         {
            $userExp[]=array('name'=>$exp['Expertise']['name'],'experience_years'=>$exp['Expertise']['experience_years'],'domain_details'=>$exp['Expertise']['domain_details']);
         }
       }
       if(isset($tips) && !empty($tips))
       {
         foreach($tips as $tip)
         {
            $userTip[]=array('title'=>$tip['Tip']['title'],'description'=>$tip['Tip']['description'],'tags'=>$tip['Tip']['tags'],'id'=>$tip['Tip']['id']);
         }
       }
       if(isset($user['User']['get_help_completed']) && $user['User']['get_help_completed']==1)
       {
         $user_required_exp=explode(',',$user['User']['required_categories']);
         foreach($user_required_exp as $exv)
         {
            $helprequired[]=array('tag'=>$exv);
         }
       }
       if(isset($user['User']['why_to_like']) && $user['User']['why_to_like']!='')
       {
         $why_to_like_exp=explode(',',$user['User']['why_to_like']);
         foreach($why_to_like_exp as $exw)
         {
            $why_to_like[]=array('whytolike'=>$whytolike["whytolike"][$exw]);
         }
       } 
       if(isset($user['User']['required_categories']) && $user['User']['required_categories']!='')
       {
         $required_categories_exp=explode(',',$user['User']['required_categories']);
         foreach($required_categories_exp as $exr)
         {
            $req_help_arr[]=array('text'=>$exr);
         }
       }   
       $alltimezones=$this->getalltimezones('activation');
       $userdetails=array('id'=>$user['User']['id'],'name'=>$user['User']['name'],'company_name'=>$user['User']['company_name'],'website'=>$user['User']['website'],'timezone'=>$user['User']['timezone'],'expert_level'=>ucwords($user['User']['expert_level']),'image'=>$user['User']['image'],'helprequired'=>$helprequired,'why_to_like'=>$why_to_like,'alltimezones'=>$alltimezones,'tags'=>$req_help_arr,'whytolike'=>$whytolike,'fbid'=>$fbid,'lnid'=>$lnid,'gid'=>$gid,'imgtype'=>$imgtype);
     }
     if(!empty($userdetails))
     {
       echo json_encode(array("is_experts_available"=>1,"userdetails"=>$userdetails,"expertise"=>$userExp,"tips"=>$userTip));
     }
     else
     {
       echo json_encode(array("is_experts_available"=>0,"userdetails"=>$userdetails));
     }
     exit;
   }
   
   public function getBadgeByUserName()
   {
     $userid=$_REQUEST['userid'];
     $this->loadModel('Expertise');
     $this->loadModel('Tip');
     $this->User->recursive = 2;
     $optionsuser = array('conditions' => array('User.username' => $userid));
     $user=$this->User->find('first', $optionsuser);
     
     $option = array('conditions' => array('Expertise.userid' => $user['User']['id']));
     $expertise=$this->Expertise->find('all', $option);
     
          
     $userdetails=array();
     $userExp=array();
     $userTip=array();
     $helprequired=array();
     $why_to_like=array();
     $req_help_arr=array();
     $whytolike=$this->whytolike();
     $expertName = '';
     if(!empty($user))
     {
       if(isset($expertise) && !empty($expertise))
       {
         foreach($expertise as $exp)
         {
            $expertName .= $exp['Expertise']['name'].',';
            //$userExp[]=array('name'=>$exp['Expertise']['name'],'experience_years'=>$exp['Expertise']['experience_years'],'domain_details'=>$exp['Expertise']['domain_details']);
         }
       }
       $expertName = rtrim($expertName,',');
       
         
       $userdetails=array('id'=>$user['User']['id'],'name'=>$user['User']['name'],'company_name'=>$user['User']['company_name'],'website'=>$user['User']['website'],'expert_level'=>ucwords($user['User']['expert_level']),'image'=>$user['User']['image'],'tags'=>$user['User']['required_categories'],'expert'=>$expertName);
     }
     if(!empty($userdetails))
     {
       echo json_encode(array("userdetails"=>$userdetails));
       // echo $_GET['callback']. '(' . json_encode(array("userdetails"=>$userdetails)) . ');';
     }
     else
     {
       echo json_encode(array("userdetails"=>$userdetails));
       //echo $_GET['callback']. '(' . json_encode(array("userdetails"=>$userdetails)) . ');';
     }
     exit;
   }
   
   public function updateexpertlevel()
   {
     $userid=$_REQUEST['userid'];
     $expertlevel=$_REQUEST['expertlevel'];
     $user['User']['id']=$userid;
     $user['User']['expert_level']=strtolower($expertlevel);
     $this->User->save($user);  
     echo json_encode(array("msg_type"=>1));
     exit;
   }
   
   public function addTip()
   {
     //echo '<pre>';print_r($_REQUEST);exit;
     $this->loadModel('Tip');
     $usertip['Tip']['user_id'] = $_REQUEST['userid'];
     $usertip['Tip']['id'] = $_REQUEST['tipid'];
     $usertip['Tip']['title'] = $_REQUEST['title'];
     $usertip['Tip']['description'] = $_REQUEST['description'];
     foreach($_REQUEST['tags'] as $k=>$v)
     {
     	$taggs[] = $v['text'];
     }
     //$taggs = call_user_func_array('array_merge', $_REQUEST['tags']);
    // echo '<pre>';print_r($taggs);exit;
     $usertip['Tip']['tags'] = implode(',',$taggs);
     
     if($_REQUEST['tipid']=='' || empty($_REQUEST['tipid']))
     {
     	$this->Tip->create();
     }
     $this->Tip->save($usertip);  
     echo json_encode(array("msg_type"=>1));
     exit;
   }
   
   /*public function getTip()
   {
     //echo '<pre>';print_r($_REQUEST);exit;
     $this->loadModel('Tip');
     $user_id = $_REQUEST['userid'];
     $option = array('conditions' => array('Tip.userid' => $user_id));
     $expertise=$this->Tip->find('all', $option);
     echo json_encode(array("msg_type"=>1));
     exit;
   }*/
   
   public function updatetimezone()
   {
     $userid=$_REQUEST['userid'];
     $timezone=$_REQUEST['timezone'];
     $user['User']['id']=$userid;
     $user['User']['timezone']=$timezone;
     $this->User->save($user);  
     echo json_encode(array("msg_type"=>1));
     exit;
   }
   
   public function updaterequirement()
   {
     $userid=$_REQUEST['userid'];
     $chosencats='';
     if(!empty($_REQUEST['chosencats']))
     {
       foreach($_REQUEST['chosencats'] as $v)
       {
         $chosencats.=$v['text'].',';
       }
       $chosencats=trim($chosencats,',');
     }
     $user['User']['id']=$userid;
     $user['User']['required_categories']=trim($chosencats,',');
     $this->User->save($user);  
     echo json_encode(array("msg_type"=>1));
     exit;
   }
   
   public function whytolikelist()
   {
     $whytolike=$this->whytolike();
     echo json_encode($whytolike);
     exit;
   }
    
   public function updatewhytolike()
   {
     $userid=$_REQUEST['userid'];
     $updatewhytolike=$_REQUEST['updatewhytolike'];
     $user['User']['id']=$userid;
     $user['User']['why_to_like']=$updatewhytolike;
     $user['User']['get_help_completed']=1;
     $this->User->save($user);  
     echo json_encode(array("msg_type"=>1));
     exit;
   }

   public function makearrayandreturnjson()
   {
        $chosencats='';
	if(isset($_REQUEST['expertise_related_tags']) && $_REQUEST['expertise_related_tags']!='')
	{
	 foreach($_REQUEST['expertise_related_tags'] as $v)
         {
          $chosencats.=$v['text'].',';
         }
        }
        $array=array();
        $array=array(
	'expertise_related_tags'=>$_REQUEST['expertise_related_tags'],	
	'expertise_experience'=>$_REQUEST['expertise_experience'],
	'expertise_moredetails'=>$_REQUEST['expertise_moredetails'],
	'expertise_rating'=>$_REQUEST['expertise_rating']
	);
       echo json_encode($array);
       exit;
   }
   
   public function logoutuser(){
         $loggedinuser['User']['id']=$_REQUEST['userid'];
         $loggedinuser['User']['is_logged_in']=0;
         $this->User->save($loggedinuser);
         echo json_encode(array("msg_type"=>1));  
         exit;
    }
    
    public function testsocket(){
         //echo json_encode(array("msg_type"=>"This is the app"));  
         
         echo "This is the app"; 
         exit;
    }


}
