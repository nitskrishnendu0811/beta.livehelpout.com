<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property State $State
 * @property City $City
 */
class Group extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	
}
