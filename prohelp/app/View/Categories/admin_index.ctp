<!-- BEGIN PAGE HEADER-->   
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
          Categories
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
                <span class="divider">/</span>
            </li>
            <!--<li>
                <a href="#">Metro Lab</a>
                <span class="divider">/</span>
            </li>-->
            <li class="active">
                Categories
            </li>
            <!--<li class="pull-right search-wrap">
                <form action="search_result.html" class="hidden-phone">
                    <div class="input-append search-input-area">
                        <input class="" id="appendedInputButton" type="text">
                        <button class="btn" type="button"><i class="icon-search"></i> </button>
                    </div>
                </form>
            </li>-->
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
    <!-- BEGIN EXAMPLE TABLE widget-->
    <div class="widget red">
        <div class="widget-title">
            <h4><i class="icon-reorder"></i> Categories</h4>
                <span class="tools">
                    <?php echo $this->Html->link(__('Add New Category'), array('controller' => 'categories', 'action' => 'add')); ?>
                </span>
        </div>
        <div class="widget-body">
            <table class="table table-striped table-bordered" id="sample_1">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('name'); ?></th>
                    <th><?php echo $this->Paginator->sort('category_image'); ?></th>
                    <th><?php echo $this->Paginator->sort('is_active'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($categories as $category): ?>
                        <tr>
                            <td>
                            <?php echo ($this->Html->link(__($category['Category']['name']), array('action' => 'subcategories', $category['Category']['id'])));?>
                            </td>
                            <td>
                            <img src="<?php echo $this->webroot;?>upload/category_images/<?php echo $category['Category']['category_image']?>" width="47" height="47">
                            </td>
                            <td><?php echo h($category['Category']['is_active']==1?'Yes':'No'); ?>&nbsp;</td>
                            <td class="actions actionbtn">
                             <a href="<?php echo $this->webroot;?>admin/categories/addsubcategory/<?php echo $category['Category']['id'];?>"><img src="<?php echo $this->webroot;?>img/subcat_add.png" title="Add Sub Category" width="22" height="21"></a>

                             <a href="<?php echo $this->webroot;?>admin/categories/edit/<?php echo $category['Category']['id'];?>"><img src="<?php echo $this->webroot;?>img/edit.png" title="Edit Category" width="22" height="21"></a>

                             <a href="<?php echo $this->webroot;?>admin/categories/delete/<?php echo $category['Category']['id'];?>" onclick="return confirm('Are you sure to delete this category?')"><img src="<?php echo $this->webroot;?>img/delete.png" title="Delete Category" width="24" height="24"></a>
                            </td>
                        </tr>
                   
                    <?php endforeach; ?>
                </tbody>
            </table>
            <ul>
<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	</p>
		<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
		</div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
