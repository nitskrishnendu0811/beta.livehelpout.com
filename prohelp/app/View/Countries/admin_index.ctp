
<div class="row-fluid">
	<!-- block -->
	<div class="block">
		<div class="navbar navbar-inner block-header">
			<div class="muted pull-left"><?php echo __('Countries'); ?></div>
			
		</div>
		<div class="block-content collapse in">
		<div class="categories index">
		<table cellpadding="0" cellspacing="0" width="100%" class="table table-striped">
	<tr>
		<td style="width:70%;border:0px solid red;">
			<form action="<?php echo Router::url(array('controller' => "countries", 'action' => "index"),true);?>" method="post" style="margin: 0px;">
				<input type="text" name="data[Country][name]" id="en" placeholder="Search By Country Name" value="<?php if(isset($keyword)&&($keyword!='')){echo $keyword;} ?>" style="float: left;">
				<input type="submit" value="Search" style="float: left;margin: 0px 0px 0px 12px;">
					<div class="submit" style="margin: 0px 0px 0px 12px;">
						<a class="cancel" href="<?php echo Router::url(array('controller' => "countries", 'action' => "index"), true);?>">Cancel</a>
					</div>
			</form>
		</td>
		<!--<td style="width:30%;border:1px dashed #ccc;text-align:center"><?php //echo $this->Html->link(__('Add A New Word/Sentence'), array('controller' => 'languages', 'action' => 'add')); ?></td>-->
	</tr>
	</table>
	<table cellpadding="0" cellspacing="0" width="100%" class="table table-striped">
	<tr>
			<th><?php echo $this->Paginator->sort('name_en'); ?></th>
			<th><?php echo $this->Paginator->sort('name_pr'); ?></th>
			<th><?php echo $this->Paginator->sort('name_fr'); ?></th>
			<th><?php echo $this->Paginator->sort('name_ar'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($countries as $country): ?>
	<tr>
		<td><?php echo h($country['Country']['name_en']); ?>&nbsp;</td>
		<td><?php echo h($country['Country']['name_pr']); ?>&nbsp;</td>
		<td><?php echo h($country['Country']['name_fr']); ?>&nbsp;</td>
		<td><?php echo h($country['Country']['name_ar']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $country['Country']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
		</div>
	</div>
	
	<!-- /block -->
</div>









