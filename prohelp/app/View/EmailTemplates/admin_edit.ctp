<?php echo $this->Html->script('ckeditor/ckeditor');?>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
   <div class="span12">
      <!-- BEGIN PAGE TITLE & BREADCRUMB-->
       <h3 class="page-title">
         Email Templates
       </h3>
       <ul class="breadcrumb">
           <li>
               <a href="#">Home</a>
               <span class="divider">/</span>
           </li>
           <li>
               <a href="<?php echo $this->webroot.'admin/email_templates/'; ?>">Email Templates</a>
               <span class="divider">/</span>
           </li>
           <li class="active">
              Edit Template
           </li>
           <!--<li class="pull-right search-wrap">
               <form action="search_result.html" class="hidden-phone">
                   <div class="input-append search-input-area">
                       <input class="" id="appendedInputButton" type="text">
                       <button class="btn" type="button"><i class="icon-search"></i> </button>
                   </div>
               </form>
           </li>-->
       </ul>
       <!-- END PAGE TITLE & BREADCRUMB-->
   </div>
</div>
<!-- END PAGE HEADER-->

 <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN SAMPLE FORMPORTLET-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> <?php echo __('Edit Email Template'); ?></h4>
                <span class="tools">
                <a href="javascript:;" class="icon-chevron-down"></a>
                <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <?php 
                    echo $this->Form->create('EmailTemplate',array('class' => 'form-horizontal'));
                    echo $this->Form->input('id',array('type'=>'hidden'));
                    echo $this->Form->input('template_id',array('type'=>'hidden'));
                ?>
                    <div class="control-group">
                        <div class="control-group">
                            <label class="control-label">Mail For</label>
                            <div class="controls">
                                <?php echo $this->Form->input('mail_for',array('label' => FALSE)); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">Mail From</label>
                            <div class="controls">
                                <?php echo $this->Form->input('mail_from',array('label' => FALSE)); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">Subject</label>
                            <div class="controls">
                                <?php echo $this->Form->input('subject',array('label' => FALSE)); ?>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label">Content</label>
                            <div class="controls">
                                <?php echo $this->Form->textarea('content',array('class'=>'ckeditor','label'=>FALSE)); ?>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <?php echo $this->Form->submit('Save',array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                 <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>