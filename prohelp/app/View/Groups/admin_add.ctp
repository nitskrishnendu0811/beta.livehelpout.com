<?php ?>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
   <div class="span12">
      <!-- BEGIN PAGE TITLE & BREADCRUMB-->
       <h3 class="page-title">
         Add Group
       </h3>
       <ul class="breadcrumb">
           <li>
               <a href="#">Home</a>
               <span class="divider">/</span>
           </li>
           <li>
               <a href="<?php echo $this->webroot.'admin/groups/'; ?>">Groups</a>
               <span class="divider">/</span>
           </li>
           <li class="active">
               Add Group           </li>
           <!--<li class="pull-right search-wrap">
               <form action="search_result.html" class="hidden-phone">
                   <div class="input-append search-input-area">
                       <input class="" id="appendedInputButton" type="text">
                       <button class="btn" type="button"><i class="icon-search"></i> </button>
                   </div>
               </form>
           </li>-->
       </ul>
       <!-- END PAGE TITLE & BREADCRUMB-->
   </div>
</div>
<!-- END PAGE HEADER-->

 <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN SAMPLE FORMPORTLET-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> Add Group</h4>
                <span class="tools">
                <a href="javascript:;" class="icon-chevron-down"></a>
                <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
            <div class="widget-body">
                <?php echo $this->Form->create('Group', array('class' => 'form-horizontal')); ?>
                    <div class="control-group">
                        <div class="control-group">
                            <label class="control-label">Title</label>
                            <div class="controls">
                            <?php echo $this->Form->input('title',array('required'=>'required','label' => FALSE)); ?>
                            </div>
                        </div>
						<div class="control-group">
                            <label class="control-label">Users Group</label>
                        </div>
<?php foreach($user as $use):
if($use['User']['name']!=''){
?>

                        <div class="control-group">
                            <label class="control-label"><?php echo $use['User']['name'] ;?></label>
                            <div class="controls">
								<input type="checkbox" name="data[Group][is_group][]" value="<?php echo $use['User']['id'];?>">
                                <?php //echo $this->Form->input('is_group', array('type'=>'checkbox','label'=>FALSE)); ?>
                            </div>
                        </div>
						<?php }?>
<?php endforeach;?>
<div class="control-group">
                            <label class="control-label">Is Active</label>
                            <div class="controls">
                                <?php echo $this->Form->input('is_active', array('type'=>'checkbox','label'=>FALSE)); ?>
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <?php echo $this->Form->submit('Save',array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                 <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>