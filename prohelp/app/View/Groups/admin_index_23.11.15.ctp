<!-- BEGIN PAGE HEADER-->   
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
          Groups
        </h3>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
                <span class="divider">/</span>
            </li>
            <!--<li>
                <a href="#">Metro Lab</a>
                <span class="divider">/</span>
            </li>-->
            <li class="active">
                Groups
            </li>
            <!--<li class="pull-right search-wrap">
                <form action="search_result.html" class="hidden-phone">
                    <div class="input-append search-input-area">
                        <input class="" id="appendedInputButton" type="text">
                        <button class="btn" type="button"><i class="icon-search"></i> </button>
                    </div>
                </form>
            </li>-->
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
    <!-- BEGIN EXAMPLE TABLE widget-->
    <div class="widget red">
        <div class="widget-title">
            <h4><i class="icon-reorder"></i> Groups</h4>
                <span class="tools">
                    <?php echo $this->Html->link(__('Add New group'), array('controller' => 'groups', 'action' => 'add')); ?>
                </span>
        </div>
        <div class="widget-body">
            <table class="table table-striped table-bordered" id="sample_1">
                <thead>
                <tr>
                    <th><?php echo $this->Paginator->sort('title'); ?></th>
                    <th><?php echo $this->Paginator->sort('is_active'); ?></th>
                    <th class="actions"><?php echo __('Actions'); ?></th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($groups as $group):
					 ?>
                        <tr>
                            <td>
                            <?php echo ($this->Html->link(__($group['Group']['title']), array('action' => 'add', $group['Group']['id'])));?>
                            </td>
                            <td><?php echo h($group['Group']['is_active']==1?'Yes':'No'); ?>&nbsp;</td>
                            <td class="actions actionbtn">
                             <a href="<?php echo $this->webroot;?>admin/groups/add/<?php echo $group['Group']['id'];?>"><img src="<?php echo $this->webroot;?>img/subcat_add.png" title="Add Group" width="22" height="21"></a>

                             <a href="<?php echo $this->webroot;?>admin/groups/edit/<?php echo $group['Group']['id'];?>"><img src="<?php echo $this->webroot;?>img/edit.png" title="Edit Group" width="22" height="21"></a>

                             <a href="<?php echo $this->webroot;?>admin/groups/delete/<?php echo $group['Group']['id'];?>" onclick="return confirm('Are you sure to delete this group?')"><img src="<?php echo $this->webroot;?>img/delete.png" title="Delete Group" width="24" height="24"></a>
                            </td>
                        </tr>
                   
                    <?php endforeach; ?>
                </tbody>
            </table>
            <ul>
<?php
		echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>	</p>
		<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
		</div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE widget-->
    </div>
</div>
