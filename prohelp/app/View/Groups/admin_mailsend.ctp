<?php echo $this->Html->script('ckeditor/ckeditor');?>
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">
   <div class="span12">
      <!-- BEGIN PAGE TITLE & BREADCRUMB-->
       <h3 class="page-title">
        Mail Send
       </h3>
       <ul class="breadcrumb">
           <li>
               <a href="#">Home</a>
               <span class="divider">/</span>
           </li>
           
           <li class="active">
              Mail Send
           </li>
           <!--<li class="pull-right search-wrap">
               <form action="search_result.html" class="hidden-phone">
                   <div class="input-append search-input-area">
                       <input class="" id="appendedInputButton" type="text">
                       <button class="btn" type="button"><i class="icon-search"></i> </button>
                   </div>
               </form>
           </li>-->
       </ul>
       <!-- END PAGE TITLE & BREADCRUMB-->
   </div>
</div>
<!-- END PAGE HEADER-->

 <!-- BEGIN PAGE CONTENT-->
<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN SAMPLE FORMPORTLET-->
        <div class="widget green">
            <div class="widget-title">
                <h4><i class="icon-reorder"></i> <?php echo __('Mail Send'); ?></h4>
                <span class="tools">
                <a href="javascript:;" class="icon-chevron-down"></a>
                <a href="javascript:;" class="icon-remove"></a>
                </span>
            </div>
             
            <div class="widget-body">
                <?php 
                    echo $this->Form->create('Group'); 
                    echo $this->Form->input('id',array('type'=>'hidden'));
                ?>
                    
                        
                        <div class="control-group">
                           
                            <div class="controls">
                            	<?php echo $this->request->data['Group']['title']; ?>
                            </div>
                        </div>
			
                        <div class="control-group">
                            <label class="control-label">User</label>
                        </div>
                        <?php 
                        $group = explode(',',$this->request->data['Group']['is_group']);
                        //print_r($group);
                        foreach($group as $gr_id)
                        {
                        //$gr_id;
                        $group_id=$this->requestAction('/admin/groups/user_find/'.$gr_id);
                        
                        echo $group_id['User']['name'].'('.$group_id['User']['email'].')'.'<br>';
                        //echo $this->Form->input('group_id',array('type'=>'text','value'=>$group_id['User']['name']));
                        }
                        
                        

                        //echo '</pre>';
                        //print_r($templete);
                        //if(isset($templete['subject']))
                        //{
                        	
                        //}
                        ?>
                        
                        <div class="control-group">
                        
                         <div class="control-group">
                            <label class="control-label">Subject</label>
                        </div>
                           
                            <div class="controls">
                               <input type="text" name="data[subject]" id="" value="<?php echo $templete['EmailTemplate']['subject'];?>">
                            </div>
                        </div>
                        
                        <div class="control-group">
                        
                         <div class="control-group">
                            <label class="control-label">Description</label>
                        </div>
                           
                            <div class="controls">
                            <!--<textarea class="ckeditor" style="height:100px;width:220px;"><?php echo $templete['EmailTemplate']['content'];?></textarea>-->
                            <?php //echo $templete['EmailTemplate']['content'];?>
                                <?php //echo $this->Form->textarea($templete['EmailTemplate']['content'],array('class'=>'ckeditor','label'=>FALSE)); ?>
                                	<?php echo $this->Form->textarea('content',array('class'=>'ckeditor','label'=>FALSE, 'value'=>$templete['EmailTemplate']['content'])); ?>
                            </div>
                        </div>
                       
                        
                        <div class="form-actions">
                            <?php echo $this->Form->submit('Submit',array('class' => 'btn btn-success')); ?>
                        </div>
                    </div>
                 <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>