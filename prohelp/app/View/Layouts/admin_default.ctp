<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'LiveHelpout Admin Panel');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
            <?php echo $cakeDescription ?>:
            <?php echo $this->fetch('title'); ?>
    </title>
    <?php
        echo $this->Html->meta('icon');
        #echo $this->Html->css('cake.generic');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
    <link href="<?php echo $this->webroot; ?>admin_theme/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo $this->webroot; ?>admin_theme/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="<?php echo $this->webroot; ?>admin_theme/assets/bootstrap/css/bootstrap-fileupload.css" rel="stylesheet" />
    <link href="<?php echo $this->webroot; ?>admin_theme/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo $this->webroot; ?>admin_theme/css/style.css" rel="stylesheet" />
    <link href="<?php echo $this->webroot; ?>admin_theme/css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo $this->webroot; ?>admin_theme/css/style-default.css" rel="stylesheet" id="style_color" />
    <link href="<?php echo $this->webroot; ?>admin_theme/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
    <link href="<?php echo $this->webroot; ?>admin_theme/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    
    <!---------- Jquery ------------------>
    <script src="<?php echo $this->webroot; ?>admin_theme/js/jquery-1.8.3.min.js"></script>
    <!---------- Jquery ------------------>
</head>
<body class="fixed-top">
    <?php 
        if($this->params['controller']=='users' && $this->params['action']=='admin_index')
        {
            echo $this->fetch('content');
        }
        else { ?>  
        
    <?php echo $this->element('admin_header'); ?>
   <div id="container" class="row-fluid">
                  
         <?php echo $this->element('admin_sidebar'); ?>  
        
        <div id="main-content">
            <!-- BEGIN PAGE CONTAINER-->
            <div class="container-fluid">
                <?php echo $this->Session->flash(); ?>

                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
   </div>
        <?php }
        ?>
    
	
    
    <!-- BEGIN FOOTER -->
    <div id="footer">
        2013 &copy; LiveHelpout
    </div>
    <!-- END FOOTER -->
    
    
	<?php echo $this->element('sql_dump'); ?>
    
    <!-- END FOOTER -->

   <!-- BEGIN JAVASCRIPTS -->
   <!-- Load javascripts at bottom, this will reduce page load time -->   
   <script src="<?php echo $this->webroot; ?>admin_theme/js/jquery.nicescroll.js" type="text/javascript"></script>
   <script type="text/javascript" src="<?php echo $this->webroot; ?>admin_theme/assets/jquery-slimscroll/jquery-ui-1.9.2.custom.min.js"></script>
   <script type="text/javascript" src="<?php echo $this->webroot; ?>admin_theme/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
   <script src="<?php echo $this->webroot; ?>admin_theme/assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
   <script src="<?php echo $this->webroot; ?>admin_theme/assets/bootstrap/js/bootstrap.min.js"></script>

   <!-- ie8 fixes -->
   <!--[if lt IE 9]>
   <script src="js/excanvas.js"></script>
   <script src="js/respond.js"></script>
   <![endif]-->

   <script src="<?php echo $this->webroot; ?>admin_theme/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
   <script src="<?php echo $this->webroot; ?>admin_theme/js/jquery.sparkline.js" type="text/javascript"></script>
   <script src="<?php echo $this->webroot; ?>admin_theme/assets/chart-master/Chart.js"></script>
   <script src="<?php echo $this->webroot; ?>admin_theme/js/jquery.scrollTo.min.js"></script>


   <!--common script for all pages-->
   <script src="<?php echo $this->webroot; ?>admin_theme/js/common-scripts.js"></script>

   <!--script for this page only-->

   <script src="<?php echo $this->webroot; ?>admin_theme/js/easy-pie-chart.js"></script>
   <script src="<?php echo $this->webroot; ?>admin_theme/js/sparkline-chart.js"></script>
   <script src="<?php echo $this->webroot; ?>admin_theme/js/home-page-calender.js"></script>
   <script src="<?php echo $this->webroot; ?>admin_theme/js/home-chartjs.js"></script>

   <!-- END JAVASCRIPTS -->   
   
</body>
</html>
