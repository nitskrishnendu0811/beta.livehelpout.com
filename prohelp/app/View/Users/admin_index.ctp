<section class="container" >
<div class="login">
  <h1>Login to Admin Panel</h1>
  <form id="UserAdminLoginForm" name="UserAdminLoginForm" action="<?php echo $this->webroot; ?>admin/users/login" class="contact_form" method="post">
	<p><input type="text" name="data[User][email]" maxlength="50" id="Useremail" class="contact_text_box" placeholder="Enter your username" required="required" value=""/></p>
	<p><input type="password" name="data[User][password]" maxlength="50" id="UserPassword" class="contact_text_box" placeholder="Enter your password" required="required" value=""/></p>
	<!-- <p class="remember_me">
	  <label>
		<input type="checkbox" name="remember_me" id="remember_me">Remember me
	  </label>
	</p> -->
	<p class="submit"><input type="submit" name="commit" value="Login"></p>
  </form>
</div>


</section>
<style>
.login:before{
background: none repeat scroll 0 0 rgba(0, 0, 0, 0.08);
    border-radius: 4px;
    bottom: -8px;
    content: "";
    left: -8px;
    position: absolute;
    right: -8px;
    top: -8px;
    z-index: -1;

}
.login{
 background: none repeat scroll 0 0 white;
    border-radius: 3px;
    box-shadow: 0 0 200px rgba(255, 255, 255, 0.5), 0 1px 2px rgba(0, 0, 0, 0.3);
    margin: 0 auto;
    padding: 20px;
    position: relative;
    width: 310px;

}
#content h1{background: none repeat scroll 0 0 #f3f3f3;
    border-bottom: 1px solid #cfcfcf;
    border-radius: 3px 3px 0 0;
    box-shadow: 0 1px whitesmoke;
    color: #555;
    font-size: 15px;
    font-weight: bold;
    line-height: 40px;
    margin: -20px -20px 21px;
    text-align: center;
    text-shadow: 0 1px white;}
.container{width:640px; margin:80px auto;}
body{background-color:#0ca3d2;color: #404040;
    font: 13px/20px 'Lucida Grande',Tahoma,Verdana,sans-serif;}
	#header{display:none;}
	#footer{display:none;}
	#content{background-color:transparent;}
	input[type="text"], input[type="password"] {-moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    -moz-outline-radius: 3px;
    background: none repeat scroll 0 0 white;
    border-color: #c4c4c4 #d1d1d1 #d4d4d4;
    border-image: none;
    border-radius: 2px;
    border-style: solid;
    border-width: 1px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12) inset;
    color: #404040;
    height: 34px;
    margin: 5px;
    outline: 5px solid #eff4f7;
    padding: 0 10px;}
	
	input[type="submit"]{-moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: linear-gradient(to bottom, #edf5f8, #cde5ef) repeat scroll 0 0 #cde5ef!important;
    border-color: #b4ccce #b3c0c8 #9eb9c2!important;
    border-image: none;
    border-radius: 16px;
    border-style: solid;
    border-width: 1px;
    box-shadow: 0 1px white inset, 0 1px 2px rgba(0, 0, 0, 0.15);
    box-sizing: content-box;
    color: #527881!important;
    font-size: 12px;
    font-weight: bold!important;
    height: 29px!important;
    outline: 0 none!important;
    padding: 0 18px!important;
    text-shadow: 0 1px #e3f1f1!important;}
	.login p.submit {
    text-align: right;
}
</style>