angular
.module("authFront", ["ngCookies"])
.factory("myAuth", function ($http,$cookies, $cookieStore) {
    
  var factobj = {};
  
  factobj.alerts = [
    { type: 'danger', msg: 'Invalid Email/Password!', altclass: 'text-center alert alert-danger' },
    { type: 'success', msg: 'You have successfully logged in.', altclass: 'text-center alert alert-success' },
{ type: 'warning', msg: 'You have successfully logged in.', altclass: 'text-center alert alert-warning' }
  ];

  factobj.addAlert = function(type,message) {
   var msg={};
    angular.forEach(factobj.alerts,function(alert,key){
       if(alert.type==type)
       {
         alert.msg=message;
         msg=alert;
       }
    });
    return msg;
  };
  
  
    factobj.baseurl = "http://108.179.225.244/~nationalit/team2/prohelp/";
    var years = [];
    for (var i = 2014; i <= 2050; i++) {
        years.push(i);
    }
    factobj.years = years;
    
    var days = [];
    for (var i = 1; i <= 31; i++) {
        days.push(i);
    }
    factobj.days = days;
    
    /*********************************User Authorisation ***********************************/
    factobj.userinfo = { loginstatus: false, id: "", email: "",name:"",image:"",username:"" };
    factobj.updateUserinfo = function (obj) {
    if(obj)
	{
           factobj.userinfo = { loginstatus: obj.isloggedin, id: obj.id, email: obj.email,name:obj.name,username:obj.username,image:obj.image };
           return true;
	}
    };
    factobj.resetUserinfo = function () {
        factobj.userinfo = { loginstatus: false, id: "", email: "",name:"",image:"",username:"" };
    };
    
    factobj.getUserAuthorisation = function () {      
       var obj=$cookieStore.get('users');
       if(obj)
       return obj;
       else
       return null;
    };
    
    factobj.getUserNavlinks = function () {
        var userlogin = factobj.userinfo.loginstatus,
            useremail = (typeof factobj.userinfo.id == "undefined" || factobj.userinfo.id == "") ? "Unknown" : factobj.userinfo.id;
        if (!userlogin) {
           return false;
        } else {
	    return factobj.userinfo;
        }
    };
    
    factobj.isUserLoggedIn = function () {
        var userlogin = factobj.userinfo.loginstatus;
        if (!userlogin) {
            return false;
        } else {
	    return true;
        }
    };
    
    /*********************************User Authorisation ***********************************/

    
    return factobj;
	
})
