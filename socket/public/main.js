/**
 * Created by NITS on 6/10/2015.
 */
var app=angular.module("chatApp",[]);
app.factory("socket",function(){
    var socket=io.connect("http://104.238.100.98:3001");
return socket;
});
app.controller("ChatController",function($scope,socket){
    $scope.msgs=[];
    $scope.sendMessage=function(){
        socket.emit("send message",$scope.msg.text);
        $scope.msg.text='';
        $scope.typingmsg='';
    }
    $scope.keypressed=function(){
      console.log('hi');
      socket.emit("typing");
    }
    socket.on("sendtyping",function(data){
        console.log(data)
        $scope.typingmsg=data;
        $scope.$digest();
    });
    socket.on("get message",function(data){
        $scope.msgs.push(data);
        $scope.$digest();
    });
});
